<?php
    class Finanzas extends Controlador{

        public function __construct(){
            date_default_timezone_set('America/El_Salvador'); 
            $this->finanzasModelo = $this->modelo('Finanza');
            Sesion::start();
        }
        
        public function index(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $tipoCompra = $this->finanzasModelo->gettipocompra();
                $datos = [
                    'tipocomp'=>$tipoCompra
                ];
                $this->vista('/finanzas/index',$datos);
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }

        public function compras(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $fechainit = $_POST['fechainit'];
                    $fechafin = $_POST['fechafin'];
                    $tipoCompra = $_POST['tipocompra'];

                    $datos = [
                        'fechainit'=>$fechainit,
                        'fechafin'=>$fechafin,
                        'tipocompra'=>$tipoCompra
                    ];
                    $getcompras = $this->finanzasModelo->getcompras($datos);
                    echo json_encode($getcompras);
                }else{
                    echo '0';// problemas al enviar los datos
                }
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }
    }