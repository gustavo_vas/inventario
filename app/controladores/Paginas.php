<?php

    class Paginas extends Controlador{
        
        public function __construct(){            
            date_default_timezone_set('America/El_Salvador');   
            $this->usuarioModelo = $this->modelo('Usuario');
            Sesion::start();
        }

        public function index(){            
            $this->vista('/paginas/inicio');
        }
        
        public function login(){
            //obtener el usuario
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                $user=ltrim(rtrim(strip_tags($_POST['usuario'])));
                $clave=ltrim(rtrim(strip_tags($_POST['clave'])));
                if (isset($user) && isset($clave)) {
                    $usuario = $this->usuarioModelo->obtenerUsuario($user,$clave);
                    if($usuario){
                        if($usuario->estado == 1){
                            $this->createSesion('codusuario',$usuario->codusuario);
                            $this->createSesion('usuario',$usuario->usuario);
                            $this->createSesion('cargo',$usuario->cargo);
                            $this->createSesion('estado',$usuario->estado);
                            $this->createSesion('nombreUser',$usuario->nombre);
                            $this->createSesion('apellidoUser',$usuario->apellido);
                            $this->createSesion('idsucursal',$usuario->idsuc);
                            $this->createSesion('idemp',$usuario->idemp);
                            
                            switch ($usuario->cargo) {
                                case 'ADMINISTRADOR':
                                    redireccionar('/admins1/inicio');
                                break;

                                case 'GERENTE':
                                    redireccionar('/gerente/index');
                                break;
                                
                                default:
                                    redireccionar('/paginas/destroySesion');
                                break;
                            }
                        }else{
                            redireccionar('/paginas/destroySesion');
                        }   
                    }else{
                        redireccionar('/paginas/destroySesion');
                    }
                }else{                
                    redireccionar('/paginas/destroySesion');
                }        
                            
            }else{                
                redireccionar('/paginas/destroySesion');
            }            
        }

        //metodo para crear las sesiones
        public function createSesion($nombreSesion,$valor){
            Sesion::setSesion($nombreSesion,$valor);
        }

        //metodo para eliminar las sesiones
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }
    