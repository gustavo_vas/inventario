<?php
    class Ventas extends Controlador{
        public function __construct(){
            date_default_timezone_set('America/El_Salvador');
            $this->ventasModelo = $this->modelo('venta');
            $this->clienteModelo = $this->modelo('cliente');
            $this->empleadoModelo =  $this->modelo('empleado');
            // $this->productoModelo =  $this->modelo('producto');
            $this->inventarioModelo = $this->modelo('inventario');
            Sesion::start();
        }

        public function index(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                try {
                    $tipo = conta;                    
                    $empleados =  $this->empleadoModelo->obtenerEmpleados();
                    $clientes = $this->clienteModelo->obtenerClientes();

                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {                        
                        $dateinit = $_POST['inicio'];
                        $datefin = $_POST['fin'];
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $ventas = $this->ventasModelo->obtenerVentas($datos1);
                        $datos = [
                            'tipo'=>$tipo,
                            'ventas'=>$ventas,
                            'vendedores' =>$empleados,
                            'clientes' =>$clientes,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];
                        $this->vista('/ventas/controlventas',$datos);
                    }else{
                        $dateinit = date('Y-m-d');
                        $datefin = date('Y-m-d');
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $ventas = $this->ventasModelo->obtenerVentas($datos1);
                        $datos = [
                            'tipo'=>$tipo,
                            'ventas'=>$ventas,
                            'vendedores' =>$empleados,
                            'clientes' =>$clientes,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];
                        $this->vista('/ventas/controlventas',$datos);
                    }
                } catch (Exeception $e) {
                    //throw $th;
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function credito(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                try {
                    $tipo = credi;
                    $empleados =  $this->empleadoModelo->obtenerEmpleados();
                    $clientes = $this->clienteModelo->obtenerClientes();
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $dateinit = $_POST['inicio'];
                        $datefin = $_POST['fin'];
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $ventas = $this->ventasModelo->obtenerVentas($datos1);
                        
                        $datos = [
                            'tipo'=>$tipo,
                            'ventas'=>$ventas,
                            'vendedores' =>$empleados,
                            'clientes' =>$clientes,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];
                        $this->vista('/ventas/controlventascredito',$datos);
                    }else{
                        $dateinit = date('Y-m-d');
                        $datefin = date('Y-m-d');
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $ventas = $this->ventasModelo->obtenerVentas($datos1);
                        
                        $datos = [
                            'tipo'=>$tipo,
                            'ventas'=>$ventas,
                            'vendedores' =>$empleados,
                            'clientes' =>$clientes,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];
                        $this->vista('/ventas/controlventascredito',$datos);
                    }
                } catch (Exeception $e) {
                    //throw $th;
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function agregar(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $tipo = conta;
                $tipoV = $this->ventasModelo->getTipoventa($tipo); 
                $clientes = $this->clienteModelo->obtenerClientes();
                $empleados =  $this->empleadoModelo->obtenerEmpleados();
                // $productos = $this->productoModelo->obtenerProductos();
                $productosinvent = $this->inventarioModelo->obtenerInventario();
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $numf = trim($_POST['numfact']);
                    $vendedor = trim($_POST['Vendedor']);
                    $fecha = trim($_POST['fechaventa']);
                    $cliente = trim($_POST['Cliente']);
                    $tipoventa = trim($_POST['tipoventa']);
                    $comision = trim($_POST['comision']);
                    $totalv = 0;
                    $totaliva = 0;
                    $totasiniva = 0;
                    $estadov = 1;
                    $suc = Sesion::getSesion('idsucursal');
                    $detalle = json_decode($_POST['detalle']);

                    foreach ($detalle as $det) {
                        $totalv += $det->total;
                    }
                    $totaliva = ($totalv*0.13);
                    $totasiniva = ($totalv-$totaliva);
                        $datos = [
                            'numf' => $numf,
                            'vendedor' => $vendedor,
                            'fecha' => $fecha,
                            'cliente' => $cliente,
                            'tipoventa' => $tipoventa,
                            'comision' => $comision,
                            'totalv' => $totalv,
                            'totaliva' => $totaliva,
                            'totasiniva' => $totasiniva,
                            'estadov' => $estadov,
                            'suc' => $suc,
                            'detalle' => $detalle
                        ];
                        $r = $this->ventasModelo->agregarVentas($datos);
                        if($r){
                            // echo '1';
                            //  echo $r;
                            foreach ($detalle as $det) {
                                $cantidad = $det->cant;
                                $preciou = $det->costounitario;
                                $totaldet = ($cantidad * $preciou);
                                $iva = ($totaldet * 0.13);
                                $detalle = [
                                    'idprod' => $det->codeproducto,
                                    'cantidad' => $cantidad,
                                    'costounit' => $preciou,
                                    'iva' => $iva,
                                    'totaldetlle' => $totaldet,
                                    'idventa' => $r
                                ];

                                if($this->ventasModelo->agregarDetalleVenta($detalle)){
                                    // redireccionar('/empleados');
                                    echo '1';
                                }else{
                                    die('Ocurrio un Problema al Insertar los datos');
                                }
                            }
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }
                }else{
                    $datos = [
                        'tipovent' => $tipoV,
                        'clientes' => $clientes,
                        'empleados' => $empleados,
                        // 'productos' => $productos
                        'inventario' => $productosinvent
                    ];
                    $this->vista('/ventas/addventa',$datos);
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para agregar venta al credito
        public function agregarcredito(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $tipo = credi;
                $tipoV = $this->ventasModelo->getTipoventa($tipo); 
                $clientes = $this->clienteModelo->obtenerClientes();
                $empleados =  $this->empleadoModelo->obtenerEmpleados();
                // $productos = $this->productoModelo->obtenerProductos();
                $productosinvent = $this->inventarioModelo->obtenerInventario();
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $numf = trim($_POST['numfact']);
                    $vendedor = trim($_POST['Vendedor']);
                    $fecha = trim($_POST['fechaventa']);
                    $cliente = trim($_POST['Cliente']);
                    $tipoventa = trim($_POST['tipoventa']);
                    $comision = trim($_POST['comision']);
                    $totalv = 0;
                    $totaliva = 0;
                    $totasiniva = 0;
                    $estadov = 1;
                    $suc = Sesion::getSesion('idsucursal');
                    $detalle = json_decode($_POST['detalle']);

                    foreach ($detalle as $det) {
                        $totalv += $det->total;
                    }
                    $totaliva = ($totalv*0.13);
                    $totasiniva = ($totalv-$totaliva);
                        $datos = [
                            'numf' => $numf,
                            'vendedor' => $vendedor,
                            'fecha' => $fecha,
                            'cliente' => $cliente,
                            'tipoventa' => $tipoventa,
                            'comision' => $comision,
                            'totalv' => $totalv,
                            'totaliva' => $totaliva,
                            'totasiniva' => $totasiniva,
                            'estadov' => $estadov,
                            'suc' => $suc,
                            'detalle' => $detalle
                        ];
                        $r = $this->ventasModelo->agregarVentas($datos);
                        if($r){
                            // echo '2';
                            //  echo $r;
                            foreach ($detalle as $det) {
                                $cantidad = $det->cant;
                                $preciou = $det->costounitario;
                                $totaldet = ($cantidad * $preciou);
                                $iva = ($totaldet * 0.13);
                                $detalle = [
                                    'idprod' => $det->codeproducto,
                                    'cantidad' => $cantidad,
                                    'costounit' => $preciou,
                                    'iva' => $iva,
                                    'totaldetlle' => $totaldet,
                                    'idventa' => $r
                                ];

                                if($this->ventasModelo->agregarDetalleVenta($detalle)){
                                    // redireccionar('/empleados');
                                    echo '2';
                                }else{
                                    die('Ocurrio un Problema al Insertar los datos');
                                }
                            }
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }
                }else{
                    $datos = [
                        'tipovent' => $tipoV,
                        'clientes' => $clientes,
                        'empleados' => $empleados,
                        // 'productos' => $productos
                        'inventario' => $productosinvent
                    ];
                    $this->vista('/ventas/addventacredito',$datos);
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para mostrar detalle de ventas
        public function verdetalle($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $detalleId = $this->ventasModelo->obtenerDetalleId($id);
                $ventaId = $this->ventasModelo->obtenerVentaId($id);
                // $abono = $this->ventasModelo->obtenerSumaAbono($id);

                // if($abono->monto > 0){
                //     $abono = $abono->monto;
                //     $totalc = $compraId->totalc;
                //     $deuda = (number_format((float)round(($totalc - $abono),2),2));
                // }else{
                //     $abono = 0.00;
                //     $deuda = $compraId->totalc;
                // }
                $datos = [
                    'nfac' => $ventaId->nfact,
                    'nomclie' => $ventaId->nomclie,
                    'apellclie' => $ventaId->apellclie,
                    'nomemp' => $ventaId->nomemp,
                    'apellemp' => $ventaId->apellemp,
                    'fecha' => $ventaId->fecha,
                    'sucursal' => $ventaId->sucursal,
                    'estadov' => $ventaId->estadov,
                    'tipov' => $ventaId->tipov,
                    'comision' => $ventaId->comision,
                    'dui' => $ventaId->dui,
                    'direccion' => $ventaId->direccion,
                    'nit' => $ventaId->nit,
                    'detalle' => $detalleId,
                    'totv' => $ventaId->totv,
                    'totiva' => $ventaId->totiva,
                    'totsiniva' => $ventaId->totsiniva,
                    // 'tipoc' => $ventaId->tipoc,
                    // 'sucursal' => $ventaId->sucursal,
                    // 'detalle' => $detalleId
                    // 'abono' => $abono,
                    // 'deuda' => $deuda
                ];
                $this->vista('/ventas/verdetallesv',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para obtener los datos de la venta que se va a editar
        public function obtenerVentasmaestro(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $ventaId = $this->ventasModelo->obtenerVentaId($id);
                        if (!$ventaId) {
                            die('Fallo al obtener los datos');
                            $ventaId = [
                                'nomemp' => '',
                                'apellemp' => '',
                                'idclie' => '',
                                'nfact' => '',
                                'nomclie' => '',
                                'apellclie' => '',
                                'idemple' => '',
                                'idtpv' => '',
                                'tipov' => '',
                                // 'totalc' => '',
                                // 'totiva' => '',
                                // 'totsiniva' => '',
                            ];
                            echo json_encode($ventaId);
                        }
                        echo json_encode($ventaId);
                    }                    
                }else{
                    redireccionar('/ventas/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar el maestro de venta
        public function editarmaestroventas(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $nfact = $_POST['nfact'];
                        $vende = $_POST['vende'];
                        $fecha = $_POST['fecha'];
                        $clie = $_POST['clie'];
                        $estad = $_POST['estad'];

                        $datos = [
                            'id' => $id,
                            'nfact' => $nfact,
                            'vende' => $vende,
                            'fecha' => $fecha,
                            'clie' => $clie,
                            'estad' => $estad
                        ];

                        if($this->ventasModelo->editarmestroventa($datos)){
                            echo '1';
                        }else{
                            die('Fallo al actualizar los datos');
                            echo '0';
                        }
                    }                    
                }else{
                    // redireccionar('/ventas/');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar el detalle de una venta al contado
        public function editardetallecontado($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $prod = trim($_POST['prod']);
                    $cantidad = trim($_POST['cantidad']);
                    $costunit = trim($_POST['costunit']);
                    $idvent = trim($_POST['idvent']);
                    $comision = trim($_POST['comision']);
                    $totaldet = ($cantidad * $costunit);
                    $iva = ($totaldet * 0.13);

                    $d = [
                       'idvent'=>$idvent,
                       'comision' =>$comision
                    ];

                    $detalle = [
                        'idprod' => $prod,
                        'cantidad' => $cantidad,
                        'costounit' => $costunit,
                        'iva' => $iva,
                        'totaldetlle' => $totaldet,
                        'idventa' => $idvent
                    ];

                    if($this->ventasModelo->agregarDetalleVenta($detalle)){
                        $this->ventasModelo->actuaVenta($idvent,$comision);
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                            
                }else{
                    $detalleId = $this->ventasModelo->obtenerDetalleId($id);
                    $ventaId = $this->ventasModelo->obtenerVentaId($id);
                    $empleados =  $this->empleadoModelo->obtenerEmpleados();
                    $productosinvent = $this->inventarioModelo->obtenerInventario();
                    // $detalleId = $this->ventasModelo->obtenerDetalleId($id);
                    // $tipoV = $this->ventasModelo->getTipoventa($tipo); 
                    $clientes = $this->clienteModelo->obtenerClientes();
                    // $abono = $this->ventasModelo->obtenerSumaAbono($id);

                    // if($abono->monto > 0){
                    //     $abono = $abono->monto;
                    //     $totalc = $compraId->totalc;
                    //     $deuda = (number_format((float)round(($totalc - $abono),2),2));
                    // }else{
                    //     $abono = 0.00;
                    //     $deuda = $compraId->totalc;
                    // }
                    $datos = [
                        'id' => $id,
                        'nfac' => $ventaId->nfact,
                        'idclie' => $ventaId->idclie,
                        'nomclie' => $ventaId->nomclie,
                        'apellclie' => $ventaId->apellclie,
                        'idemple' => $ventaId->idemple,
                        'nomemp' => $ventaId->nomemp,
                        'apellemp' => $ventaId->apellemp,
                        'fecha' => $ventaId->fecha,
                        'sucursal' => $ventaId->sucursal,
                        'estadov' => $ventaId->estadov,
                        'tipov' => $ventaId->tipov,
                        'comision' => $ventaId->comision,
                        'dui' => $ventaId->dui,
                        'direccion' => $ventaId->direccion,
                        'nit' => $ventaId->nit,
                        'detalle' => $detalleId,
                        'totv' => $ventaId->totv,
                        'totiva' => $ventaId->totiva,
                        'totsiniva' => $ventaId->totsiniva,
                        'empleados' => $empleados,
                        'idtpv' => $ventaId->idtpv,
                        'clientes' => $clientes,
                        'inventario' => $productosinvent
                        // 'detalle' => $detalleId
                        // 'abono' => $abono,
                        // 'deuda' => $deuda
                    ];
                    $this->vista('/ventas/editardetalles',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para eliminar linea de detalle de detalle de ventas
        public function eliminardetalleLineaventa(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $id = $_POST['id'];
                $idvent = trim($_POST['idvent']);
                    if($this->ventasModelo->eliminardetalleVentaid($id)){
                        $this->ventasModelo->actuaVentadel($idvent);
                    }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para eliminar una venta al contado
        public function eliminarventa($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $ventaId = $this->ventasModelo->obtenerVentaId($id);
                $estadov = $ventaId->estadov;
                if ($estadov == 1) {
                    if($this->ventasModelo->eliminarVentaid($id)){
                        redireccionar('/ventas/index');
                    }
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para eliminar una venta al contado
        public function eliminarventacredito($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $ventaId = $this->ventasModelo->obtenerVentaId($id);
                $estadov = $ventaId->estadov;
                if ($estadov == 1) {
                    if($this->ventasModelo->eliminarVentaid($id)){
                        redireccionar('/ventas/credito');
                    }
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar detalle credito
        public function editardetallecredito($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $prod = trim($_POST['prod']);
                    $cantidad = trim($_POST['cantidad']);
                    $costunit = trim($_POST['costunit']);
                    $idvent = trim($_POST['idvent']);
                    $comision = trim($_POST['comision']);
                    $totaldet = ($cantidad * $costunit);
                    $iva = ($totaldet * 0.13);

                    $d = [
                       'idvent'=>$idvent,
                       'comision' =>$comision
                    ];

                    $detalle = [
                        'idprod' => $prod,
                        'cantidad' => $cantidad,
                        'costounit' => $costunit,
                        'iva' => $iva,
                        'totaldetlle' => $totaldet,
                        'idventa' => $idvent
                    ];

                    if($this->ventasModelo->agregarDetalleVenta($detalle)){
                        $this->ventasModelo->actuaVenta($idvent,$comision);
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                            
                }else{
                    $detalleId = $this->ventasModelo->obtenerDetalleId($id);
                    $ventaId = $this->ventasModelo->obtenerVentaId($id);
                    $empleados =  $this->empleadoModelo->obtenerEmpleados();
                    $productosinvent = $this->inventarioModelo->obtenerInventario();
                    // $detalleId = $this->ventasModelo->obtenerDetalleId($id);
                    // $tipoV = $this->ventasModelo->getTipoventa($tipo); 
                    $clientes = $this->clienteModelo->obtenerClientes();
                    // $abono = $this->ventasModelo->obtenerSumaAbono($id);

                    // if($abono->monto > 0){
                    //     $abono = $abono->monto;
                    //     $totalc = $compraId->totalc;
                    //     $deuda = (number_format((float)round(($totalc - $abono),2),2));
                    // }else{
                    //     $abono = 0.00;
                    //     $deuda = $compraId->totalc;
                    // }
                    $datos = [
                        'id' => $id,
                        'nfac' => $ventaId->nfact,
                        'idclie' => $ventaId->idclie,
                        'nomclie' => $ventaId->nomclie,
                        'apellclie' => $ventaId->apellclie,
                        'idemple' => $ventaId->idemple,
                        'nomemp' => $ventaId->nomemp,
                        'apellemp' => $ventaId->apellemp,
                        'fecha' => $ventaId->fecha,
                        'sucursal' => $ventaId->sucursal,
                        'estadov' => $ventaId->estadov,
                        'tipov' => $ventaId->tipov,
                        'comision' => $ventaId->comision,
                        'dui' => $ventaId->dui,
                        'direccion' => $ventaId->direccion,
                        'nit' => $ventaId->nit,
                        'detalle' => $detalleId,
                        'totv' => $ventaId->totv,
                        'totiva' => $ventaId->totiva,
                        'totsiniva' => $ventaId->totsiniva,
                        'empleados' => $empleados,
                        'idtpv' => $ventaId->idtpv,
                        'clientes' => $clientes,
                        'inventario' => $productosinvent
                        // 'detalle' => $detalleId
                        // 'abono' => $abono,
                        // 'deuda' => $deuda
                    ];
                    $this->vista('/ventas/editardetallescredito',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para obtener los abonos por cada venta al credito
        public function obtenerabonosxventa(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $abonos = $this->ventasModelo->obtenerabonosxventa($id);
                        if (!$abonos) {
                            die('Fallo al obtener los datos');
                            $abonos = [
                                'monto' => 0,
                                'fecha' => ''
                            ];
                            echo json_encode($abonos);
                        }
                        echo json_encode($abonos);
                    }                    
                }else{
                    redireccionar('/ventas/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para abonar a una venta
        public function agregarabonosxventa(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id']) && isset($_POST['abono'])) {
                        $id = $_POST['id'];
                        $abono = $_POST['abono'];
                        $fechacreada = date("Y/m/d");
                        $fechabono = $_POST['fecha'];

                        $sumabono = $this->ventasModelo->obtenerSumaAbono($id);
                        $ventaId = $this->ventasModelo->obtenerVentaId($id);

                        $totventa = $ventaId->totv;
                        $sumbono = $sumabono->monto;
                        $sdeudor = ($totventa - $sumbono);
                        if ($sdeudor > 0) {
                            if($abono > 0){
                                if ($abono <= $sdeudor && $abono > 0) {
                                    $datos = [
                                        'idventa' => $id,
                                        'monto' => $abono,
                                        'fecha' => $fechabono
                                    ];
                                    // var_dump($sdeudor);
                                    if($this->ventasModelo->abonarventa($datos)){
                                        $smabono = $this->ventasModelo->obtenerSumaAbono($id);
                                        $smbono = $smabono->monto;
                                        $sdeuda = ($totventa - $smbono);                                        
                                        if ($smbono == $totventa && $sdeuda == 0) {
                                            $estado = 3;
                                            $datos2 = [
                                                'id'=>$id,
                                                'estad'=>$estado
                                            ];
                                            $this->ventasModelo->updateEstado($datos2);
                                        }
                                        echo 'abono ingresado';
                                    }else{
                                        die('Fallo al ingresar un abono');
                                    }
                                }else{
                                    echo '0';
                                }
                            }else{
                                echo '1';
                            }
                        }else{
                            echo '2';
                        }
                    }
                }else{
                    redireccionar('/ventas/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
   //metodo para agregar contrato de usuario
   public function agregarcontrato(){
    if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
       // $detalleId = $this->ventasModelo->obtenerDetalleId($id);
        
        if($_SERVER['REQUEST_METHOD'] == 'POST') {
            $idventa = trim($_POST['id']);
            $fechac = date("Y-m-d");

            $datos = [
                'idventa' => $idventa,
                'fechac' => $fechac,
            ];
            
             $contrato = $this->ventasModelo->agregarContrato($datos);
            //$this->vista('ventas/detallecontrato',$datos);
        }else{
           
        }
    }else{
        redireccionar('/errores/destroySesion');
    }
}
  //metodo para mostrar detalle de contrato
  public function verdetallecontrato($id){
    if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
        $detalleId = $this->ventasModelo->obtenerDetalleId($id);
        $ventaId = $this->ventasModelo->obtenerVentaContratoId($id);
        $cli1 = 'El cliente no puede ser menor de 18 años, ni mayor a 65 años';
        $cli2 = 'El cliente queda sujeto a mantener el articulo en la ubicacion pactada, caso contrario estaremos recuperando el articulo';
        $cli3= 'En caso de fallecimiento del contratante, el articulo queda cancelado en su totalidad';
        $cli4= 'El articulo se entrega en calidad de dominio al cliente durante el tiempo que se encuentre cancelando el articulo seguira siendo propiedad de la Sra. Kathetine Maritza Guerrero de Guerrero propietaria de Comercial Nohelia';
        $cli5 ='EL producto puede ser retirado de la propiedad del cliente si presenta una mora de 45 dias';
        $cli6 = 'No se le cobrara interes moratorio ni multas de ningun tipo durante el tiempo pactado que este cancelado';
        $cli7 = 'Se le permitira al cliente un atraso de 5 dias sin realizarce llamadas ni una visita de territorio';
        $cli8 = 'El cliente no podra retirar el articulo para otro domicilio sin autorizacion del propietario del articulo ';
     
        $datos = [
            'nomclie' => $ventaId->nomclie,
            'apellclie' => $ventaId->apellclie,
            'fecha' => $ventaId->fecha,
            'sucursal' => $ventaId->sucursal,
            'dui' => $ventaId->dui,
            'direccion' => $ventaId->direccion,
            'nit' => $ventaId->nit,
            'totv' => $ventaId->totv,
            'descpro' => $ventaId->descpro,
            'marcapro' => $ventaId->marcapro,
            'codigopro' => $ventaId->codigopro,
            'cli1'=> $cli1,
            'cli2'=> $cli2,
            'cli3'=> $cli3,
            'cli4'=> $cli4,
            'cli5'=> $cli5,
            'cli6'=> $cli6,
            'cli7'=> $cli7,
            'cli8'=> $cli8
        ];
        $this->vista('/ventas/detallecontrato',$datos);
    }else{
        redireccionar('/errores/destroySesion');
    }
}

    }