<?php
    class Compras extends Controlador{

        //constructor inicializa las instancias al modelo
        public function __construct(){
            date_default_timezone_set('America/El_Salvador');
            $this->comprasModelo = $this->modelo('compra');
            $this->proveedorModelo = $this->modelo('Proveedor');
            $this->productoModelo = $this->modelo('Producto');
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }

        //metodo index muestra la lista de todas las compras al contado descendente
        public function index(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                try{
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $tipo = conta;
                        $dateinit = $_POST['inicio'];
                        $datefin = $_POST['fin'];
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $compras = $this->comprasModelo->obtenerCompras($datos1);
                        $proveedores = $this->proveedorModelo->obtenerProveedores();
                        $sucursales = $this->adminModelo->obtenerSucursales();
                        rsort($compras);
                        $datos = [
                            'tipo' => $tipo,
                            'compras' => $compras,
                            'proveedores' => $proveedores,
                            'sucursal'=>$sucursales,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];

                        $this->vista('/compras/controlcompras',$datos);           
                    }else{
                        $tipo = conta;
                        $dateinit = date('Y-m-d');
                        $datefin = date('Y-m-d');
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $compras = $this->comprasModelo->obtenerCompras($datos1);
                        $proveedores = $this->proveedorModelo->obtenerProveedores();
                        $sucursales = $this->adminModelo->obtenerSucursales();
                        rsort($compras);
                        $datos = [
                            'tipo' => $tipo,
                            'compras' => $compras,
                            'proveedores' => $proveedores,
                            'sucursal'=>$sucursales,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];

                        $this->vista('/compras/controlcompras',$datos);
                    }
                }catch(Exception $e){
                    $datos = [
                        'error'=>$e
                    ];
                    $this->vista('/errores/errorConsultaBd',$datos);
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para obteenr el listado de compras al credito
        public function credito(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                try{
                    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                        $tipo = credi;
                        $dateinit = $_POST['inicio'];
                        $datefin = $_POST['fin'];
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $compras = $this->comprasModelo->obtenerCompras($datos1);
                        $proveedores = $this->proveedorModelo->obtenerProveedores();
                        $tipoC = $this->comprasModelo->getTipoCompra($tipo);
                        rsort($compras);
                        $datos = [
                            'tipo' => $tipo,
                            'compras' => $compras,
                            'proveedores' => $proveedores,
                            'tipoc' => $tipoC,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];

                        $this->vista('/compras/controlcomprascredito',$datos);
                    }else{
                        $tipo = credi;
                        $dateinit = date('Y-m-d');
                        $datefin = date('Y-m-d');
                        $datos1 = [
                            'tipo'=>$tipo,
                            'dateinit'=>$dateinit,
                            'datefin'=>$datefin
                        ];
                        $compras = $this->comprasModelo->obtenerCompras($datos1);
                        $proveedores = $this->proveedorModelo->obtenerProveedores();
                        $tipoC = $this->comprasModelo->getTipoCompra($tipo);
                        rsort($compras);
                        $datos = [
                            'tipo' => $tipo,
                            'compras' => $compras,
                            'proveedores' => $proveedores,
                            'tipoc' => $tipoC,
                            'inicio'=>$dateinit,
                            'fin'=>$datefin
                        ];

                        $this->vista('/compras/controlcomprascredito',$datos);
                    }
                }catch(Exception $e){
                    $datos = [
                        'error'=>$e
                    ];
                    $this->vista('/errores/errorConsultaBd',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para obtener los abonos por cada compra al credito
        public function obtenerabonosxcompra(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $abonos = $this->comprasModelo->obtenerabonosxcompra($id);
                        if (!$abonos) {
                            die('Fallo al obtener los datos');
                            $abonos = [
                                'monto' => 0,
                                'fecha' => ''
                            ];
                            echo json_encode($abonos);
                        }
                        echo json_encode($abonos);
                    }                    
                }else{
                    redireccionar('/compras/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
        
        //metodo para agregar una compra al contado
        public function agregar(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $tipo = conta;
                $tipoC = $this->comprasModelo->getTipoCompra($tipo); 
                $proveedores = $this->proveedorModelo->obtenerProveedores();
                $productos = $this->productoModelo->obtenerProductos();
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $numf = trim($_POST['numf']);
                    $pref = trim($_POST['pref']);
                    $fecha = trim($_POST['fecha']);
                    $provee = trim($_POST['provee']);
                    $tipoco = trim($_POST['tipoco']);
                    $totalc = 0;
                    $totaliva = 0;
                    $totasiniva = 0;
                    $estadoc = 1;//emitida
                    $suc = Sesion::getSesion('idsucursal');
                    $detalles = json_decode($_POST['detalles']);

                    foreach ($detalles as $det) {
                        $totalc += $det->total;
                    }
                    $totaliva = ($totalc*0.13);
                    $totasiniva = ($totalc-$totaliva);
                        $datos = [
                            'numf' => $numf,
                            'pref' => $pref,
                            'fecha' => $fecha,
                            'provee' => $provee,
                            'tipoco' => $tipoco,
                            'totalc' => $totalc,
                            'totaliva' => $totaliva,
                            'totasiniva' => $totasiniva,
                            'estadoc' => $estadoc,
                            'suc' => $suc,
                            'deta' => $detalles
                        ];
                        $r = $this->comprasModelo->agregarCompra($datos);
                        if($r){
                            echo 'maestro ingresado';
                            // echo $r;
                            foreach ($detalles as $det) {
                                $cantidad = $det->cant;
                                $preciou = $det->costou;
                                $totaldet = ($cantidad * $preciou);
                                $iva = ($totaldet * 0.13);
                                $detalle = [
                                    'idprod' => $det->producto,
                                    'cantidad' => $cantidad,
                                    'costounit' => $preciou,
                                    'iva' => $iva,
                                    'totaldetlle' => $totaldet,
                                    'idcompra' => $r
                                ];

                                if($this->comprasModelo->agregarDetalleCompra($detalle)){
                                    // redireccionar('/empleados');
                                    echo 'detalle ingresado';
                                }else{
                                    die('Ocurrio un Problema al Insertar los datos');
                                }
                            }
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }
                }else{
                    $datos = [
                        'tipocomp' => $tipoC,
                        'proveedores' => $proveedores,
                        'productos' => $productos
                    ];
                    $this->vista('/compras/addcompra',$datos);
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para agregar una compra al credito
        public function agregarCredito(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $tipo = credi;
                $tipoC = $this->comprasModelo->getTipoCompra($tipo); 
                $proveedores = $this->proveedorModelo->obtenerProveedores();
                $productos = $this->productoModelo->obtenerProductos();
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $numf = trim($_POST['numf']);
                    $pref = trim($_POST['pref']);
                    $fecha = trim($_POST['fecha']);
                    $provee = trim($_POST['provee']);
                    $tipoco = trim($_POST['tipoco']);
                    $totalc = 0;
                    $totaliva = 0;
                    $totasiniva = 0;
                    $estadoc = 1;//emitida
                    $suc = Sesion::getSesion('idsucursal');
                    $detalles = json_decode($_POST['detalles']);

                    foreach ($detalles as $det) {
                        $totalc += $det->total;
                    }
                    $totaliva = ($totalc*0.13);
                    $totasiniva = ($totalc-$totaliva);
                        $datos = [
                            'numf' => $numf,
                            'pref' => $pref,
                            'fecha' => $fecha,
                            'provee' => $provee,
                            'tipoco' => $tipoco,
                            'totalc' => $totalc,
                            'totaliva' => $totaliva,
                            'totasiniva' => $totasiniva,
                            'estadoc' => $estadoc,
                            'suc' => $suc,
                            'deta' => $detalles
                        ];
                        $r = $this->comprasModelo->agregarCompra($datos);
                        if($r){
                            // echo $r;
                            foreach ($detalles as $det) {
                                $cantidad = $det->cant;
                                $preciou = $det->costou;
                                $totaldet = ($cantidad * $preciou);
                                $iva = ($totaldet * 0.13);
                                $detalle = [
                                    'idprod' => $det->producto,
                                    'cantidad' => $cantidad,
                                    'costounit' => $preciou,
                                    'iva' => $iva,
                                    'totaldetlle' => $totaldet,
                                    'idcompra' => $r
                                ];

                                if($this->comprasModelo->agregarDetalleCompra($detalle)){
                                    $credito = 'credito';
                                    echo $credito;
                                }else{
                                    die('Ocurrio un Problema al Insertar los datos');
                                }
                            }
                        }else{
                            die('Ocurrio un Problema al Insertar los datos');
                        }
                }else{
                    $datos = [
                        'tipocomp' => $tipoC,
                        'proveedores' => $proveedores,
                        'productos' => $productos
                    ];
                    $this->vista('/compras/addcompracredito',$datos);
                }
                
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para mostrar la informacion completa de una factura de compra
        public function verdetalle($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $detalleId = $this->comprasModelo->obtenerDetalleId($id);
                $compraId = $this->comprasModelo->obtenerCompraId($id);
                $abono = $this->comprasModelo->obtenerSumaAbono($id);

                if($abono->monto > 0){
                    $abono = $abono->monto;
                    $totalc = $compraId->totalc;
                    $deuda = (number_format((float)round(($totalc - $abono),2),2));
                }else{
                    $abono = 0.00;
                    $deuda = $compraId->totalc;
                }
                $datos = [
                    'nfac' => $compraId->nfact,
                    'pref' => $compraId->prefijo,
                    'fecha' => $compraId->fecha,
                    'prov' => $compraId->proveedor,
                    'totalc' => $compraId->totalc,
                    'totsiniva' => $compraId->totsiniva,
                    'totiva' => $compraId->totiva,
                    'estado' => $compraId->estado,
                    'tipoc' => $compraId->tipoc,
                    'sucursal' => $compraId->sucursal,
                    'detalle' => $detalleId,
                    'abono' => $abono,
                    'deuda' => $deuda
                ];
                $this->vista('/compras/verdetalles',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para abonar a una compra
        public function agregarabonosxcompra(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id']) && isset($_POST['abono'])) {
                        $id = $_POST['id'];
                        $abono = $_POST['abono'];
                        $fechacreada = date("Y/m/d");
                        $fechabono = $_POST['fecha'];
                        $sumabono = $this->comprasModelo->obtenerSumaAbono($id);
                        $compraId = $this->comprasModelo->obtenerCompraId($id);

                        $totcompra = $compraId->totalc;
                        $sumbono = $sumabono->monto;
                        $sdeudor = ($totcompra - $sumbono);
                        if ($sdeudor > 0) {
                            if($abono > 0){
                                if ($abono <= $sdeudor && $abono > 0) {
                                    $datos = [
                                        'idcompra' => $id,
                                        'monto' => $abono,
                                        'fecha' => $fechabono
                                    ];
                                    // var_dump($sdeudor);
                                    if($this->comprasModelo->abonarcompra($datos)){
                                        $smabono = $this->comprasModelo->obtenerSumaAbono($id);
                                        $smbono = $smabono->monto;
                                        $sdeuda = ($totcompra - $smbono);                                        
                                        if ($smbono == $totcompra && $sdeuda == 0) {
                                            $estado = 3;
                                            $datos2 = [
                                                'id'=>$id,
                                                'estad'=>$estado
                                            ];
                                            $this->comprasModelo->updateEstado($datos2);
                                        }
                                        echo 'abono ingresado';
                                    }else{
                                        die('Fallo al ingresar un abono');
                                    }
                                }else{
                                    echo '0';
                                }
                            }else{
                                echo '1';
                            }
                        }else{
                            echo '2';
                        }
                    }
                }else{
                    redireccionar('/compras/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para eliminar una compra al contado
        public function eliminarcompra($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $compraId = $this->comprasModelo->obtenerCompraId($id);
                $estadoc = $compraId->estado;
                if ($estadoc == 1) {//emitida
                    if($this->comprasModelo->eliminarCompraid($id)){
                        redireccionar('/compras/index');
                    }
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para eliminar una compra al credito
        public function eliminarcompracredito($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $compraId = $this->comprasModelo->obtenerCompraId($id);
                $estadoc = $compraId->estado;
                if ($estadoc == 1) {//emitida
                    if($this->comprasModelo->eliminarCompraid($id)){
                        redireccionar('/compras/credito');
                    }
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para obtener los datos de la compra que se va a editar
        public function obtenercomprasmaestro(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $compraId = $this->comprasModelo->obtenerCompraId($id);
                        if (!$compraId) {
                            die('Fallo al obtener los datos');
                            $compraId = [
                                'estado' => '',
                                'fecha' => '',
                                'idcompra' => '',
                                'nfact' => '',
                                'prefijo' => '',
                                'proveedor' => '',
                                'sucursal' => '',
                                'tipoc' => '',
                                'totalc' => '',
                                'totiva' => '',
                                'totsiniva' => '',
                            ];
                            echo json_encode($compraId);
                        }
                        echo json_encode($compraId);
                    }                    
                }else{
                    redireccionar('/compras/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar el maestro de compra
        public function editarmaestrocompra(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    if (isset($_POST['id'])) {
                        $id = $_POST['id'];
                        $nfact = $_POST['nfact'];
                        $prefijo = $_POST['prefijo'];
                        $fecha = $_POST['fecha'];
                        $provee = $_POST['provee'];
                        $estad = $_POST['estad'];

                        $datos = [
                            'id' => $id,
                            'nfact' => $nfact,
                            'prefijo' => $prefijo,
                            'fecha' => $fecha,
                            'provee' => $provee,
                            'estad' => $estad
                        ];

                        if($this->comprasModelo->editarmestrocompra($datos)){
                            echo '1';
                        }else{
                            die('Fallo al actualizar los datos');
                            echo '0';
                        }
                    }                    
                }else{
                    redireccionar('/compras/credito');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar el detalle de una compra al credito
        public function editardetalle($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $prod = trim($_POST['prod']);
                    $cantidad = trim($_POST['cantidad']);
                    $costunit = trim($_POST['costunit']);
                    $idcomp = trim($_POST['idcomp']);
                    $totaldet = ($cantidad * $costunit);
                    $iva = ($totaldet * 0.13);
                    $detalle = [
                        'idprod' => $prod,
                        'cantidad' => $cantidad,
                        'costounit' => $costunit,
                        'iva' => $iva,
                        'totaldetlle' => $totaldet,
                        'idcompra' => $idcomp
                    ];

                    if($this->comprasModelo->agregarDetalleCompra($detalle)){
                        $this->comprasModelo->actuaCompra($idcomp);
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                            
                }else{
                    $tipo = credi;
                    $tipoC = $this->comprasModelo->getTipoCompra($tipo);
                    $detalleId = $this->comprasModelo->obtenerDetalleId($id);
                    $compraId = $this->comprasModelo->obtenerCompraId($id);
                    $proveedores = $this->proveedorModelo->obtenerProveedores();
                    $productos = $this->productoModelo->obtenerProductos();
                    $datos = [
                        'id' => $id,
                        'nfac' => $compraId->nfact,
                        'pref' => $compraId->prefijo,
                        'fecha' => $compraId->fecha,
                        'prov' => $compraId->proveedor,
                        'idprov' => $compraId->idproveedor,
                        'estado' => $compraId->estado,
                        'tipoc' => $compraId->tipoc,
                        'detalle' => $detalleId,
                        'proveedores' => $proveedores,
                        'tipocomp' => $tipoC,
                        'productos' => $productos
                    ];
                    $this->vista('/compras/editardetalles',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function eliminardetallecompracredito(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $id = $_POST['id'];
                $idcomp = trim($_POST['idcomp']);
                    if($this->comprasModelo->eliminardetalleCompraid($id)){
                        $this->comprasModelo->actuaCompra($idcomp);
                    }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar el detalle de una compra al contado
        public function editardetallecontado($id){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $prod = trim($_POST['prod']);
                    $cantidad = trim($_POST['cantidad']);
                    $costunit = trim($_POST['costunit']);
                    $idcomp = trim($_POST['idcomp']);
                    $totaldet = ($cantidad * $costunit);
                    $iva = ($totaldet * 0.13);
                    $detalle = [
                        'idprod' => $prod,
                        'cantidad' => $cantidad,
                        'costounit' => $costunit,
                        'iva' => $iva,
                        'totaldetlle' => $totaldet,
                        'idcompra' => $idcomp
                    ];

                    if($this->comprasModelo->agregarDetalleCompra($detalle)){
                        $this->comprasModelo->actuaCompra($idcomp);
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                            
                }else{
                    $tipo = conta;
                    $tipoC = $this->comprasModelo->getTipoCompra($tipo);
                    $detalleId = $this->comprasModelo->obtenerDetalleId($id);
                    $compraId = $this->comprasModelo->obtenerCompraId($id);
                    $proveedores = $this->proveedorModelo->obtenerProveedores();
                    $productos = $this->productoModelo->obtenerProductos();
                    $datos = [
                        'id' => $id,
                        'nfac' => $compraId->nfact,
                        'pref' => $compraId->prefijo,
                        'fecha' => $compraId->fecha,
                        'prov' => $compraId->proveedor,
                        'idprov' => $compraId->idproveedor,
                        'estado' => $compraId->estado,
                        'tipoc' => $compraId->tipoc,
                        'detalle' => $detalleId,
                        'proveedores' => $proveedores,
                        'tipocomp' => $tipoC,
                        'productos' => $productos
                    ];
                    $this->vista('/compras/editardetalles',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
    }