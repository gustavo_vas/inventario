<?php
    class Clientes extends Controlador {
        public function __construct(){
            //instancia del modelo clientes
            $this->clienteModelo = $this->modelo('Cliente');
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }

        public function index(){            
         
            if(Sesion::getSesion('estado') == 1) {
                //obtener los clientes
                $clientes = $this->clienteModelo->obtenerClientes();                
                $datos = [
                    'Clientes' => $clientes
                ];
                $this->vista('/Clientes/controlclientes',$datos);
            }else{
                redireccionar('/admin/destroySesion');
            }
        }

        //metodo para insertar un nuevo cliente
        public function agregar(){
            if(Sesion::getSesion('estado') == 1) {
                $departamentos = $this->adminModelo->obtenerDepartamentos();
                $sucursales = $this->adminModelo->obtenerSucursales();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = trim($_POST['nombre']);
                    $apellido = trim($_POST['apellido']);
                    $dui = trim($_POST['dui']);
                    $nit = trim($_POST['nit']);
                    $tel = trim($_POST['tel']);
                    $cel = trim($_POST['cel']);
                    $direccion = trim($_POST['direccion']);
                    $departamento = trim($_POST['departamento']);
                    $sucursal = trim($_POST['sucursal']);
                    $datos = [
                        'nombre' => $nombre,
                        'apellido' => $apellido,
                        'dui' => $dui,
                        'nit' => $nit,
                        'tel' => $tel,
                        'cel' => $cel,
                        'direccion' => $direccion,
                        'departamento' => $departamento,
                        'sucursal' => $sucursal
                        
                    ];

                    if($this->clienteModelo->agregarCliente($datos)){
                        redireccionar('/Clientes');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    $datos = [
                        'dui' => '',
                        'nit' => '',
                        'tel' => '',
                        'cel' => '',
                        'direccion' => '',
                        'departamento' => $departamentos,
                        'sucursal' => $sucursales  
                    ];

                    $this->vista('clientes/agregar',$datos);
                }
            }else{
                redireccionar('/clientes/destroySesion');
            }        
        }

        //metodo para editar cliente
        public function editar($id){
            if(Sesion::getSesion('estado') == 1) {
                $departamentos = $this->adminModelo->obtenerDepartamentos();
                $sucursales = $this->adminModelo->obtenerSucursales();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = trim($_POST['nombre']);
                    $apellido = trim($_POST['apellido']);
                    $dui = trim($_POST['dui']);
                    $nit = trim($_POST['nit']);
                    $tel = trim($_POST['tel']);
                    $cel = trim($_POST['cel']);
                    $direccion = trim($_POST['direccion']);
                    $sucursal = trim($_POST['sucursal']);
                    $departamento = trim($_POST['departamento']);
                    
                    
                    $datos = [
                        'codigo' => $id,
                        'nombre' => $nombre,
                        'apellido' => $apellido,
                        'dui' => $dui,
                        'nit' => $nit,
                        'tel' => $tel,
                        'cel' => $cel,
                        'direccion' => $direccion,
                        'sucursal' => $sucursal,
                        'departamento' => $departamento,
                        
                    ];

                    if($this->clienteModelo->actualizarCliente($datos)){
                        redireccionar('/Clientes');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    //obtener informacion del usuario desde el modelo
                    $clientes = $this->clienteModelo->obtenerClienteID($id);
                    $datos = [
                        'codigo' => $id,
                        'nombre' => $clientes->nombre,
                        'apellido' => $clientes->apellido,
                        'dui' => $clientes->dui,
                        'nit' => $clientes->nit,
                        'tel' => $clientes->tel,
                        'cel' => $clientes->cel,
                        'direccion' => $clientes->direccion,
                        'sucursal' => $clientes->sucursal,
                        'id_suc' => $clientes->id_suc,
                        'sucursales' => $sucursales,
                        'departamento' => $clientes->departamento,
                        'id_dep' => $clientes->id_dep,
                        'departamentos' => $departamentos

                    ];

                    $this->vista('Clientes/editar',$datos);
                }
            }else{
                redireccionar('/Clientes/destroySesion');
            }        
        }

        public function eliminar(){          
            if(Sesion::getSesion('estado') == 1) {
                $idc = $_POST['idc'];
                $estado = 'S';
                $update = $this->clienteModelo->eliminarcli($idc,$estado);
                if ($update) {
                    echo '1';//correcto
                }else{
                    echo '0';//error
                }
            }else{
                redireccionar('/admin/destroySesion');
            }
        }
    }