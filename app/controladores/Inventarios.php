<?php
    class Inventarios extends Controlador {
        public function __construct(){
            //instancia del modelo inventario
            $this->inventarioModelo = $this->modelo('Inventario');
            // $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }
        public function index(){            
            if(Sesion::getSesion('estado') == 1) {
                //obtener el inventario
                $inventario = $this->inventarioModelo->obtenerInventario();                
                $datos = [
                    'Inventario' => $inventario
                ];
                $this->vista('/inventario/controlinventario',$datos);
            }else{
                redireccionar('/admin/destroySesion');
            }
        }
       
    }
