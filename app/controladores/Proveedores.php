<?php
    class Proveedores extends Controlador{

        public function __construct(){
            //instancia del modelo empleado
            $this->proveedorModelo = $this->modelo('Proveedor');
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
            
        }

        public function index(){            
            if(Sesion::getSesion('estado') == 1) {
                //obtener los empleados
                $proveedores = $this->proveedorModelo->obtenerProveedores();                
                $datos = [
                    'proveedores' => $proveedores
                ];
                $this->vista('/proveedores/controlproveedores',$datos);
            }else{
                redireccionar('/admin/destroySesion');
            }
        }

        //metodo para insertar un nuevo proveedor
        public function agregar(){
            if(Sesion::getSesion('estado') == 1) {
                $tipop = $this->adminModelo->obtenerTipoP();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = trim($_POST['nombre']);
                    $direccion = trim($_POST['direccion']);
                    $dui = trim($_POST['dui']);
                    $nit = trim($_POST['nit']);
                    $tel = trim($_POST['tel']);
                    $nrc = trim($_POST['nrc']);
                    $actividad = trim($_POST['actividad']);
                    $tp = trim($_POST['tp']);
                    $datos = [
                        'nombres' => $nombre,
                        'direccion' => $direccion,
                        'dui' => $dui,
                        'nit' => $nit,
                        'tel' => $tel,
                        'nrc' => $nrc,
                        'actividad' => $actividad,
                        'tp' => $tp
                    ];

                    if($this->proveedorModelo->agregarProveedor($datos)){
                        redireccionar('/proveedores');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    $datos = [
                        'nombres' => '',
                        'direccion' => '',
                        'dui' => '',
                        'nit' => '',
                        'tel' => '',
                        'nrc' =>  '',
                        'actividad' => '',
                        'tp' => $tipop  
                    ];

                    $this->vista('proveedores/agregar',$datos);
                }
            }else{
                redireccionar('/proveedores/destroySesion');
            }        
        }

        //metodo para editar proveedor
        public function editar($id){
            if(Sesion::getSesion('estado') == 1) {
                $tipop = $this->adminModelo->obtenerTipoP();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = trim($_POST['nombre']);
                    $direccion = trim($_POST['direccion']);
                    $dui = trim($_POST['dui']);
                    $nit = trim($_POST['nit']);
                    $tel = trim($_POST['tel']);
                    $nrc = trim($_POST['nrc']);
                    $actividad = trim($_POST['actividad']);
                    $tp = trim($_POST['tp']);
                    $datos = [
                        'codigo' => $id,
                        'nombres' => $nombre,
                        'direccion' => $direccion,
                        'dui' => $dui,
                        'nit' => $nit,
                        'tel' => $tel,
                        'nrc' => $nrc,
                        'actividad' => $actividad,
                        'tp' => $tp
                    ];;

                    if($this->proveedorModelo->actualizarProveedor($datos)){
                        redireccionar('/proveedores');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    //obtener informacion del usuario desde el modelo
                    $proveedor = $this->proveedorModelo->obtenerProveedorId($id);
                    $datos = [
                        'codigo' => $id,
                        'nombres' => $proveedor->nombre,
                        'direccion' => $proveedor->direccion,
                        'dui' => $proveedor->dui,
                        'nit' => $proveedor->nit,
                        'tel' => $proveedor->tel,
                        'nrc' =>  $proveedor->nrc,
                        'actividad' => $proveedor->actividad,
                        'descripcion' => $proveedor->descripcion,
                        'tipo' => $proveedor->tipo,
                        'tipop' => $tipop 
                    ];

                    $this->vista('proveedores/editar',$datos);
                }
            }else{
                redireccionar('/proveedores/destroySesion');
            }        
        }

        //metodo para destruir la sesion
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }