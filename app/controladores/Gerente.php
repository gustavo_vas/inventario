<?php
    class Gerente extends Controlador{
        public function index(){            
            if(Sesion::getSesion('cargo') == 'GERENTE' && Sesion::getSesion('estado') == 1) {
                $this->vista('/gerente/index');
            }else{
                redireccionar('/paginas/destroySesion');
            }    
        }
    }    
    