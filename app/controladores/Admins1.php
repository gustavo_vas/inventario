<?php
    class Admins1 extends Controlador{

        public function __construct(){
            date_default_timezone_set('America/El_Salvador'); 
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }

        public function inicio(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $this->vista('/administrador/index');
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }
        
        public function index(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $this->vista('/administrador/index');
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }
        
        //metodo para eliminar las sesiones
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }

        //metodo para mostrar panel de sucursales y cargos
        public function local(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $cargos = $this->adminModelo->obtenerCargos();
                $sucursales = $this->adminModelo->obtenerSucursales();
                $datos = [
                    'cargos'=>$cargos,
                    'sucursal'=>$sucursales
                ];
                $this->vista('/administrador/local',$datos);
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }

        //metodo para agregar cargos
        public function addcargo(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $cargo = $_POST['scargo'];
                $resulinsert = $this->adminModelo->addcargo($cargo);
                if ($resulinsert) {
                    echo '1';
                }else{
                    echo '0';
                }
                
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }

        //metodo para editar cargos
        public function editcargo(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $cargo = $_POST['scargo'];
                $idcarg = $_POST['idcarg'];
                $resulinsert = $this->adminModelo->edicargo($cargo,$idcarg);
                if ($resulinsert) {
                    echo '1';
                }else{
                    echo '0';
                }
                
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }

        //metodo para agregar sucursales
        public function addsucursal(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $suc = $_POST['suc'];
                $dire = $_POST['dire'];
                $tel = $_POST['tel'];

                $datos = [
                    'suc'=>$suc,
                    'dire'=>$dire,
                    'tel'=>$tel
                ];
                $resulinsert = $this->adminModelo->addsucur($datos);
                if ($resulinsert) {
                    echo '1';
                }else{
                    echo '0';
                }
                
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }

        //metodo para editar sucursales
        public function editsucursal(){            
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                $sucu = $_POST['sucu'];
                $dire = $_POST['dire'];
                $tele = $_POST['tele'];
                $idsucu = $_POST['idsucu'];

                $datos = [
                    'sucu'=>$sucu,
                    'dire'=>$dire,
                    'tele'=>$tele,
                    'idsucu'=>$idsucu
                ];
                $resulinsert = $this->adminModelo->edisucursal($datos);
                if ($resulinsert) {
                    echo '1';
                }else{
                    echo '0';
                }
                
            }else{
                redireccionar('/paginas/destroySesion');
            }
        }
    }