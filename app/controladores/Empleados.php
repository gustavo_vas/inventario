<?php
    class Empleados extends Controlador{

        public function __construct(){
            //instancia del modelo empleado
            $this->empleadoModelo = $this->modelo('Empleado');
            $this->adminModelo = $this->modelo('Admin');
            Sesion::start();
        }

        public function index(){            
            if(Sesion::getSesion('estado') == 1) {
                //obtener los empleados
                $empleados = $this->empleadoModelo->obtenerEmpleados();                
                $datos = [
                    'empleados' => $empleados
                ];
                $this->vista('/empleados/controlempleados',$datos);
            }else{
                redireccionar('/admin/destroySesion');
            }
        }

        //metodo para insertar un nuevo empleado
        public function agregar(){
            if(Sesion::getSesion('estado') == 1) {
                $cargos = $this->adminModelo->obtenerCargos();
                $sucursales = $this->adminModelo->obtenerSucursales();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = trim($_POST['nombre']);
                    $apellido = trim($_POST['apellido']);
                    $direccion = trim($_POST['direccion']);
                    $dui = trim($_POST['dui']);
                    $nit = trim($_POST['nit']);
                    $afp = trim($_POST['afp']);
                    $isss = trim($_POST['isss']);
                    $suel = trim($_POST['salario']);
                    if(is_numeric($suel)){
                        $sueldo = $suel;
                    }else{
                        $sueldo = 000.00;
                    }
                    $cargo = trim($_POST['cargo']);
                    $estat = trim($_POST['estado']);
                    if(is_numeric($estat)){
                        if($estat == 1){
                            $estado = 1;
                        }else{
                            $estado = 0;
                        }
                    }else{
                        $estado = 0;
                    }
                    $sucursal = trim($_POST['sucursal']);
                    $datos = [
                        'nombres' => $nombre,
                        'apellidos' => $apellido,
                        'direccion' => $direccion,
                        'dui' => $dui,
                        'nit' => $nit,
                        'afp' => $afp,
                        'isss' => $isss,
                        'sueldo' => $sueldo,
                        'cargo' => $cargo,
                        'estado' => $estado,
                        'sucursal' => $sucursal
                    ];

                    if($this->empleadoModelo->agregarEmpleado($datos)){
                        redireccionar('/empleados');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    $datos = [
                        'nombres' => '',
                        'apellidos' => '',
                        'direccion' => '',
                        'dui' => '',
                        'nit' => '',
                        'afp' => '',
                        'isss' =>  '',
                        'sueldo' => '',
                        'cargo' => $cargos,
                        'estado' => '',
                        'sucursal' => $sucursales  
                    ];

                    $this->vista('empleados/agregar',$datos);
                }
            }else{
                redireccionar('/empleados/destroySesion');
            }        
        }

        //metodo para editar empleado
        public function editar($id){
            if(Sesion::getSesion('estado') == 1) {
                $cargos = $this->adminModelo->obtenerCargos();
                $sucursales = $this->adminModelo->obtenerSucursales();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $nombre = trim($_POST['nombre']);
                    $apellido = trim($_POST['apellido']);
                    $direccion = trim($_POST['direccion']);
                    $dui = trim($_POST['dui']);
                    $nit = trim($_POST['nit']);
                    $afp = trim($_POST['afp']);
                    $isss = trim($_POST['isss']);
                    $suel = trim($_POST['salario']);
                    if(is_numeric($suel)){
                        $sueldo = $suel;
                    }else{
                        $sueldo = 000.00;
                    }
                    $cargo = trim($_POST['cargo']);
                    $estat = trim($_POST['estado']);
                    if(is_numeric($estat)){
                        if($estat == 1){
                            $estado = 1;
                        }else{
                            $estado = 0;
                        }
                    }else{
                        $estado = 0;
                    }
                    $sucursal = trim($_POST['sucursal']);
                    $datos = [
                        'codigo' => $id,
                        'nombres' => $nombre,
                        'apellidos' => $apellido,
                        'direccion' => $direccion,
                        'dui' => $dui,
                        'nit' => $nit,
                        'afp' => $afp,
                        'isss' => $isss,
                        'sueldo' => $sueldo,
                        'cargo' => $cargo,
                        'estado' => $estado,
                        'sucursal' => $sucursal
                    ];

                    if($this->empleadoModelo->actualizarEmpleado($datos)){
                        redireccionar('/empleados');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    //obtener informacion del empleado desde el modelo
                    $empleado = $this->empleadoModelo->obtenerEmpleadoId($id);
                    $datos = [
                        'codigo' => $id,
                        'nombres' => $empleado->nombre,
                        'apellidos' => $empleado->apellido,
                        'direccion' => $empleado->direccion,
                        'dui' => $empleado->dui,
                        'nit' => $empleado->nit,
                        'afp' => $empleado->afp,
                        'isss' =>  $empleado->isss,
                        'sueldo' => $empleado->sueldo,
                        'cargo' => $empleado->cargo,
                        'idcargo' => $empleado->idcargo,
                        'cargos' => $cargos,
                        'estado' => $empleado->estado,
                        'sucursal' => $empleado->sucursal,
                        'idsucursal' => $empleado->idsucursal,
                        'sucursales' => $sucursales  
                    ];

                    $this->vista('empleados/editar',$datos);
                }
            }else{
                redireccionar('/empleados/destroySesion');
            }        
        }

        //metodo para desactivar empleado
        public function desactivar($id){
            if(Sesion::getSesion('estado') == 1) {
                $cargos = $this->adminModelo->obtenerCargos();
                $sucursales = $this->adminModelo->obtenerSucursales();

                //obtener informacion del empleado desde el modelo
                $empleado = $this->empleadoModelo->obtenerEmpleadoId($id);
                $datos = [
                    'codigo' => $id,
                    'nombres' => $empleado->nombre,
                    'apellidos' => $empleado->apellido,
                    'direccion' => $empleado->direccion,
                    'dui' => $empleado->dui,
                    'nit' => $empleado->nit,
                    'afp' => $empleado->afp,
                    'isss' =>  $empleado->isss,
                    'sueldo' => $empleado->sueldo,
                    'cargo' => $empleado->cargo,
                    'idcargo' => $empleado->idcargo,
                    'cargos' => $cargos,
                    'estado' => $empleado->estado,
                    'sucursal' => $empleado->sucursal,
                    'idsucursal' => $empleado->idsucursal,
                    'sucursales' => $sucursales  
                ];

                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    
                    $datos = [
                        'codigo' => $id
                    ];

                    if($this->empleadoModelo->desactivarEmpleado($datos)){
                        redireccionar('/empleados');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }
                $this->vista('empleados/editar',$datos);
            }else{
                redireccionar('/empleados/destroySesion');
            }        
        }

        //metodo para destruir la sesion
        public function destroySesion(){
            Sesion::destroy();
            header('Location: '.RUTA_URL);
        }
    }