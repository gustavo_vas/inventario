<?php
class Devoluciones extends Controlador{
//constructor inicializa las instancias al modelo
public function __construct(){
    date_default_timezone_set('America/El_Salvador');
    $this->devolucionModelo = $this->modelo('Devolucion');
 
    Sesion::start();

    }

     //metodo index muestra la lista de todas las compras al contado descendente
     public function index(){
        if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
            try{
                // $tipo = conta;
                $devolucion = $this->devolucionModelo->obtenerVentasDUI();
                $datos = [
                    // 'tipo' => $tipo,
                    'Devolucion' => $devolucion
                ];
                $this->vista('/devoluciones/controldevoluciones',$datos);
            }catch(Exception $e){
                $datos = [
                    'error'=>$e
                ];
                $this->vista('/errores/errorConsultaBd',$datos);
            }

        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    // //metodo para mostrar facturas vendidas
    public function agregar(){
        
        if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {           
            try{
                // $tipo = conta;
                $devolucion = $this->devolucionModelo->obtenerVentasDUI();
                $datos = [
                    // 'tipo' => $tipo,
                    'Devolucion' => $devolucion
                ];
                $this->vista('/devoluciones/agregardevolucion',$datos);
            }catch(Exception $e){
                $datos = [
                    'error'=>$e
                ];
                $this->vista('/errores/errorConsultaBd',$datos);
            }
                }else{
                redireccionar('/errores/destroySesion');
        }
    }

            //metodo para obtener los datos de la devolucion que se va a editar
    public function obtenerdevoluciones(){
        if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                if (isset($_POST['id'])) {
                    $id = $_POST['id'];
                    $devolucionId = $this->devolucionModelo->obtenerDetalleId($id);
                    if (!$devolucionId) {
                        die('Fallo al obtener los datos');
                        $devolucionId = [
                            'descripcion_producto' => '',
                            'cantidad_producto' => '',
                            'costo_producto' => '',
                            'total ' => ''                                
                        ];
                        echo json_encode($devolucionId);
                    }
                    echo json_encode($devolucionId);
                }                    
            }else{
                redireccionar('/devoluciones/');
            }
        }else{
            redireccionar('/errores/destroySesion');
        }
    }

    //metodo para realizar una devolucion
    public function devolverarticulo(){
        if(Sesion::getSesion('cargo') == "ADMINISTRADOR" && Sesion::getSesion('estado') == 1){
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                if(isset($_POST['id'])){
                    $id = $_POST['id'];
                    $cant = $_POST['cant'];
                    $cantdv = $_POST['cantdv'];

                    $idv = $_POST['idv'];
                    $fecha = $_POST['fecha'];
                    $sucid = Sesion::getSesion('idsucursal');
                    $userid = Sesion::getSesion('codusuario');

                    $detlineaventa = $this->devolucionModelo->getlineadetventa($id);
                    $venta = $this->devolucionModelo->getventa($idv);
                    $totventa = $venta->total_venta;

                    if ($detlineaventa) {
                        $cantdb = $detlineaventa->cantidad;
                        $idventa = $detlineaventa->id_venta;
                        $idprod = $detlineaventa->id_producto;
                        $excandev = $detlineaventa->cant_dev;
                        $n = ($excandev+$cantdv);
                        if ($cantdb == $cant) {
                            if ($cantdv<=$cantdb) {
                                if ($cantdv>0) {
                                    $newcant = ($cantdb-$cantdv);
                                    $pu = $detlineaventa->costo_unitario;
                                    $total = ($newcant*$pu);

                                    $t=($cantdv*$pu);
                                    $totalv = ($totventa-$t);
                                    $ivanewvent = ($totalv*0.13);
                                    $newsiniva = ($totalv-$ivanewvent);

                                    $iva = ($total*0.13);
                                    $datosdetalle = [
                                        'newcant'=>$newcant,
                                        'id'=>$id,
                                        'cantdv'=>$n,
                                        'iva'=>$iva,
                                        'total'=>$total,

                                        'totalv'=>$totalv,
                                        'ivanewvent'=>$ivanewvent,
                                        'newsiniva'=>$newsiniva,

                                        'fecha'=>$fecha,
                                        'idsuc'=>$sucid,
                                        'userid'=>$userid,
                                        'idv'=>$idv,

                                        'cantdv2'=>$cantdv,
                                        'idprod'=>$idprod,
                                        'pu'=>$pu,
                                        'totdev'=>$t
                                    ];
                                    if ($idv==$idventa) {
                                        $actudetvent = $this->devolucionModelo->devolucion($datosdetalle);
                                        if ($actudetvent) {
                                            echo '4';
                                        }else{
                                            echo '3';// ocurrio un problema al realizar el proceso de devolucion
                                        }
                                        
                                    }else{
                                        echo '0';
                                    }
                                    
                                }else{
                                    echo '2';//la cantidad a devolver no puede ser menor o igual a cero
                                }
                            }else{
                                echo '1';//la cantidad a devolver no puede ser mayor a la cantidad de venta
                            }
                        }else{
                            echo '0';
                        }
                    }else{
                        echo '0';//error al intentar consultar los datos de linea detalle de venta
                    }
                }
            }else{
                redireccionar('/devoluciones/');
            }
        }else{
            redireccionar('/errores/destroySesion');
        }
    }
}
