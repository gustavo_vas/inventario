<?php

    class Productos extends Controlador{

        public function __construct(){
            $this->productoModelo = $this->modelo('Producto');
            Sesion::start();
        }

        public function index(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                $productos = $this->productoModelo->obtenerProductos();

                $datos = [
                    'productos' => $productos
                ];

                $this->vista('/productos/controlproductos',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para mostrar marcas y categorias
        public function mostrarCM(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                //obtener categorias
                $categorias = $this->productoModelo->obtenerCategorias();
                $marcas = $this->productoModelo->obtenerMarcas();

                $datos = [
                    'categorias' => $categorias,
                    'marcas' => $marcas
                ];
                $this->vista('/productos/categoriaMarca',$datos);
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para agregar un nuevo producto
        public function agregar(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                $categorias = $this->productoModelo->obtenerCategorias();
                $marcas = $this->productoModelo->obtenerMarcas();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $codigop = trim($_POST['codep']);
                    $marca = trim($_POST['marca']);
                    $categoria = trim($_POST['categ']);
                    $descr = trim($_POST['descrip']);
                    // $pv = trim($_POST['pv']);
                    // if(is_numeric($pv)){
                    //     $precven = $pv;
                    // }else{
                    //     redireccionar('/productos/agregar');
                    // }

                    $datos = [
                        'codp' => $codigop,
                        'marca' => $marca,
                        'categoria' => $categoria,
                        'descri' => $descr
                        // 'precventa' => $precven
                    ];

                    if($this->productoModelo->agregarProducto($datos)){
                        redireccionar('/productos');
                    }else{
                        die('Ocurrio un Problema al Insertar los datos');
                    }
                }else{
                    $datos = [
                        'codp' => '',
                        'marcas' => $marcas,
                        'categorias' => $categorias,
                        'descri' => ''
                        // 'precventa' => ''
                    ];

                    $this->vista('productos/agregar',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        //metodo para editar un producto
        public function editar($id){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                $categorias = $this->productoModelo->obtenerCategorias();
                $marcas = $this->productoModelo->obtenerMarcas();
                if($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $codigop = trim($_POST['codep']);
                    $marca = trim($_POST['marca']);
                    $categoria = trim($_POST['categ']);
                    $descr = trim($_POST['descrip']);
                    // $pv = trim($_POST['pv']);
                    // if(is_numeric($pv)){
                    //     $precven = $pv;
                    // }else{
                    //     redireccionar('/productos/agregar');
                    // }

                    $datos = [
                        'code' => $id,
                        'codp' => $codigop,
                        'marca' => $marca,
                        'categoria' => $categoria,
                        'descri' => $descr
                        // 'precventa' => $precven
                    ];

                    if($this->productoModelo->actualizarProducto($datos)){
                        redireccionar('/productos');
                    }else{
                        die('Ocurrio un Problema al editar los datos');
                    }
                }else{
                    $producto = $this->productoModelo->obtenerProductoId($id);
                    $datos = [
                        'code' => $id,
                        'codp' => $producto->codigo,
                        'idmarca' => $producto->idmarca,
                        'marca' => $producto->marca,
                        'marcas' => $marcas,
                        'idcate' => $producto->idcate,
                        'cate' => $producto->cate,
                        'categorias' => $categorias,
                        'descri' => $producto->producto
                        // 'precventa' => $producto->precio
                    ];

                    $this->vista('productos/editar',$datos);
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function addCategoria(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
               if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $descripcion = trim($_POST['cate']);

                    $datos = [
                        'descripcion' => $descripcion
                    ];

                    if ($this->productoModelo->agregarCategoria($datos)) {
                        redireccionar('/productos/mostrarCM');
                    }else{
                        //aqui se puede controlar para que redireccione a una paina de error
                        die('ocurrio un problema al insertar la categoria');
                    }
               }else{
                   $datos = [
                       'descripcion' => ''
                   ];
                   $this->vista('productos/categoriaMarca',$datos);
               }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function addMarca(){
            if (Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1) {
                if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                    $descripcion = trim($_POST['cate']);

                    $datos = [
                        'descripcion' => $descripcion
                    ];

                    if ($this->productoModelo->agregarMarca($datos)) {
                        redireccionar('/productos/mostrarCM');
                    }else{
                        die('ocurrio un problema al insertar la marca');
                    }
                }else{
                    $datos = [
                        'descripcion' => ''
                    ];
                    $this->vista('productos/categoriaMarca');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function editarC(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $descripcion = trim($_POST['descr']);
                    $code = trim($_POST['cod']);

                    if(is_numeric($code)){
                        $datos = [
                            'code' => $code,
                            'descripcion' => $descripcion
                        ];

                        if ($this->productoModelo->editarCategoria($datos)) {
                            redireccionar('/productos/mostrarCM');
                        }else{
                            die('ocurrio un problema al insertar la marca');
                        }
                    }else{
                        redireccionar('/productos/mostrarCM');
                    }
                    
                }else{
                    $categoria = $this->productoModelo->obtenerCategoriaId($id);
                    $datos = [
                        'descripcion' => $categoria->descripcion
                    ];
                    $this->vista('productos/categoriaMarca');
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }

        public function editarM(){
            if(Sesion::getSesion('cargo') == 'ADMINISTRADOR' && Sesion::getSesion('estado') == 1){
                if($_SERVER['REQUEST_METHOD'] == 'POST'){
                    $descripcion = trim($_POST['descri']);
                    $code = trim($_POST['codi']);

                    if(is_numeric($code)){
                        $datos = [
                            'code' => $code,
                            'descripcion' => $descripcion
                        ];

                        if ($this->productoModelo->editarMarca($datos)) {
                            redireccionar('/productos/mostrarCM');
                        }else{
                            die('ocurrio un problema al insertar la marca');
                        }
                    }else{
                        redireccionar('/productos/mostrarCM');
                    }
                }
            }else{
                redireccionar('/errores/destroySesion');
            }
        }
    }