<?php
    class Usuarios extends Controlador
    {
        public function __construct(){
            $this->empleadoModelo = $this->modelo('Empleado');
            $this->usuarioModelo = $this->modelo('Usuario');
            Sesion::start();
        }
        
        public function index(){
            if (Sesion::getSesion('cargo')=='ADMINISTRADOR' && Sesion::getSesion('estado')==1) {
                $usuarios = $this->usuarioModelo->getusers();
                $datos = [
                    'users' => $usuarios
                ];
                $this->vista('/usuarios/controlusuarios',$datos);
            }else{
                redireccionar('/admin/destroySesion');
            }
        }

        public function miperfil(){
            if (Sesion::getSesion('cargo')=='ADMINISTRADOR' && Sesion::getSesion('estado')==1) {
                $idemp = Sesion::getSesion('idemp');
                $empleado = $this->empleadoModelo->obtenerEmpleadoId($idemp);
                if ($empleado->estado==1) {
                    $estado='ACTIVO';
                }else{
                    $estado='INACTIVO';
                }
                $datos = [
                    'codigo' => $empleado->code,
                    'nombres' => $empleado->nombre,
                    'apellidos' => $empleado->apellido,
                    'direccion' => $empleado->direccion,
                    'dui' => $empleado->dui,
                    'nit' => $empleado->nit,
                    'afp' => $empleado->afp,
                    'isss' =>  $empleado->isss,
                    'sueldo' => $empleado->sueldo,
                    'cargo' => $empleado->cargo,
                    'idcargo' => $empleado->idcargo,
                    'estado' => $estado,
                    'sucursal' => $empleado->sucursal,
                    'idsucursal' => $empleado->idsucursal
                ];
                $this->vista('/usuarios/miperfil',$datos);
            }else{
                redireccionar('/admin/destroySesion');
            }
            
        }
        
    }
    