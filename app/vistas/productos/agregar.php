<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nuevo Producto</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Nuevo Producto</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <!-- <div class="row"> -->
                <!-- <div class="col-12"> -->
                    <!-- <div class="card"> -->                        
                        <!-- /.card-header -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Datos de Productos</h3>
                                    </div>
                                        <form role="form" action="<?php echo RUTA_URL; ?>/productos/agregar" method="POST">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-2">
                                                        <label for="codep" class="control-label">Código</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="codep" name="codep" placeholder="Código" required="true">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="tp" class="control-label">Categoría</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="categ" name="categ">
                                                            <?php foreach ($datos['categorias'] as $categoria) { ?>
                                                                <option value="<?php echo $categoria->id_categoria; ?>">
                                                                    <?php echo $categoria->descripcion; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>    
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-2">
                                                        <label for="descrip" class="control-label">Descripción</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="descrip" name="descrip" placeholder="Descripción" required>
                                                    </div>                                                    

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="tp" class="control-label">Marca</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="marca" name="marca">
                                                            <?php foreach ($datos['marcas'] as $marca) { ?>
                                                                <option value="<?php echo $marca->id_marca; ?>">
                                                                    <?php echo $marca->descripcion; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                                           
                                                </div>

                                                <!-- <div class="form-group row">
                                                    <div class="col-sm-2">
                                                        <label for="pv" class="control-label">Precio Venta</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="number" class="form-control" id="pv" name="pv" placeholder="Precio Venta" required>
                                                    </div>
                                                                                                   
                                                </div>                                                      -->
                                            </div>
                                        <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="nav-icon fas fa-save">
                                                        <b>Guardar Datos</b>
                                                    </i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                    <!-- /.card -->
                <!-- </div> -->
            <!-- </div> -->
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>        
    </body>
</html>