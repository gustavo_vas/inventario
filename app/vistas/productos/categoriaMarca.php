<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>

        <script>
            function modal(tm){
                if(tm == 1){
                    document.getElementById('lblcate').innerHTML = 'Categoria';
                    document.getElementById('cate').placeholder = 'Categoria';
                    document.getElementById('encab').innerHTML = 'Nueva Categoria';
                    document.getElementById('mod').action = '<?php echo RUTA_URL; ?>/productos/addCategoria';
                }else if(tm == 2){
                    document.getElementById('lblcate').innerHTML = 'Marca';
                    document.getElementById('cate').placeholder = 'Marca';
                    document.getElementById('encab').innerHTML = 'Nueva Marca';
                    document.getElementById('mod').action = '<?php echo RUTA_URL; ?>/productos/addMarca';
                }else{
                    document.getElementById('mod').action = '#';
                }
            }            
        </script>
        <script type="text/javascript">
            function enviar(id, descripcion,tm){
                if(tm == 1){
                    document.getElementById('cod').value = id;
                    document.getElementById('descr').value = descripcion;
                }else if(tm == 2){
                    document.getElementById('codi').value = id;
                    document.getElementById('descri').value = descripcion;
                }else{
                    document.getElementById('modeditM').action = '#';
                }                
            }
        </script>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Categorías y Marcas</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Categorías y Marcas</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Datos Generales</h3>  
                            </div>                         
                        </div>                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- Main content -->
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                    <!-- left column -->
                                    <div class="col-md-6">
                                        <!-- general form elements -->
                                        <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Categorías de Productos</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <!-- <form role="form"> -->
                                            <div class="card-footer">
                                                <button type="button" class="float-sm-right btn btn-success" data-toggle="modal" data-target="#addcate" onclick="modal(1)">
                                                    <i class="nav-icon fas fa-plus-square"></i> 
                                                    <b>Categoría</b>
                                                </button>
                                            </div>

                                            <div class="card-body">
                                                <table id="example3" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Categorías</th>  
                                                            <th>Acciones</th>                                                       
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbcategoria">
                                                        <?php foreach ($datos['categorias'] as $categoria) { ?>
                                                            <?php $ddd = 'kkkk'; ?>
                                                            <tr>
                                                                <td><?php echo $categoria->descripcion; ?> </td>
                                                                <td>
                                                                    <button type="button" onclick="enviar(<?php echo $categoria->id_categoria; ?>,'<?php echo $categoria->descripcion; ?>',1)" data-toggle="modal" data-target="#edit" title="Editar Categoria" class="btn btn-primary">
                                                                    <i class='nav-icon fas fa-edit'></i>
                                                                    </button>
                                                                    <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                                    <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>                            
                                                </table>
                                            </div>
                                            <!-- /.card-body -->                
                                        <!-- </form> -->
                                        </div>
                                        <!-- /.card -->

                                    </div>
                                    <!--/.col (left) -->
                                    <!-- right column -->
                                    <div class="col-md-6">
                                        <!-- Horizontal Form -->
                                        <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">Marcas de Productos</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <!-- <form class="form-horizontal"> -->
                                            <div class="card-footer">
                                            <button type="button" class="float-sm-right btn btn-success" data-toggle="modal" data-target="#addcate" onclick="modal(2)">
                                                    <i class="nav-icon fas fa-plus-square"></i> 
                                                    <b>Marca</b>
                                                </button>
                                            </div>
                                            <div class="card-body">
                                                <table id="example2" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Marcas</th> 
                                                            <th>Acciones</th>                                                        
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($datos['marcas'] as $marca) { ?>
                                                            <tr>
                                                                <td><?php echo $marca->descripcion; ?> </td>
                                                                <td>
                                                                    <button type="button" onclick="enviar(<?php echo $marca->id_marca; ?>,'<?php echo $marca->descripcion; ?>',2)" data-toggle="modal" data-target="#editM" title="Editar Categoria" class="btn btn-primary">
                                                                        <i class='nav-icon fas fa-edit'></i>
                                                                    </button>
                                                                    <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                                    <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>                            
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                        <!-- </form> -->
                                        </div>
                                        <!-- /.card -->            
                                    </div>
                                    <!--/.col (right) -->
                                    </div>
                                    <!-- /.row -->
                                </div><!-- /.container-fluid -->
                                </section>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>   
        <!-- modal para agregar  -->
        <div class="modal fade" id="addcate">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab"></h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="" method="POST" id="mod">
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="cate" class="control-label" id="lblcate"></label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cate" name="cate" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Guardar</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- modal para editar categorias-->
        <div class="modal fade" id="edit">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encabe">Actualizar Categoria</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?php echo RUTA_URL; ?>/productos/editarC" method="POST" id="modedit">
                        <div class="modal-body">
                        <div class="form-group row">
                                <div class="col-sm-10">
                                    <input type="hidden" readonly class="form-control" id="cod" name="cod" placeholder="Codigo" required="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="descr" class="control-label" id="lbldesc">Categoria</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="descr" name="descr" placeholder="Categoria" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
        <!-- /.modal-dialog -->
        </div>

        <!-- modal para editar -->
        <div class="modal fade" id="editM">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encabe">Actualizar Marca</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="<?php echo RUTA_URL; ?>/productos/editarM" method="POST" id="modeditM">
                        <div class="modal-body">
                        <div class="form-group row">
                                <div class="col-sm-10">
                                    <input type="hidden" readonly class="form-control" id="codi" name="codi" placeholder="Codigo" required="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="descr" class="control-label" id="lbldesc">Marca</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="descri" name="descri" placeholder="Marca" required="true">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Actualizar</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
        <!-- /.modal-dialog -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            });

            $('#example3').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            });
        });
        </script>
    </body>
</html>