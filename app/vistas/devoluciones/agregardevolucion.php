<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/validcampos.js"></script> 
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Panel de Devoluciones</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Panel de Devoluciones</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                        <form id="compras-form"> 
                                <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <div class="form-group row">
                                <div class="col-sm-3">
                                                        <select class="form-control" id="sucursal" name="sucursal">
                                                        <option value=""> Seleccione Filtro</option>
                                                            <option> Número de Dui </option>
                                                            <option> Número de Factura </option>
                                                            </select>
                                    </div> 
                                 
                                    <div class="col-sm-3">
                                    <input type="text" class="form-control" id="NombreCliente" name="nit" placeholder="Número de DUI">                                   
                                     </div>
                                    
                                    <div class="col-sm-3">
                                    <input type="text" class="form-control" id="NFactura" name="nit" placeholder="Número de Factura">                                   
                                     </div>
                                   
                                    
                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success" id="btndetalle">
                                            <i class="nav-icon fas fa-forward">
                                                <b>Siguiente</b>
                                            </i>
                                        </button>
                                    </div>
                                </div>                       
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                        <table class="table table-bordered table-striped table-sm" id="example1">
                                <thead>
                                    <tr>
                                        <th>No. Factura</th>
                                        <th>Fecha </th>
                                        <th>DUI Cliente</th>
                                        <th>Nombre Cliente</th>
                                        <th>Tipo Venta</th>
                                        <th>Estado</th>
                                        <th>Total</th>                                      
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="table-detalle">
                                <?php foreach ($datos['Devolucion'] as $devolucion) {
                                        if ($devolucion->estado == 3) {
                                            $estado = '<span class="span label-success"><b>CANCELADA</b></span>';
                                        }else if ($devolucion->estado == 2){
                                            $estado = '<span class="span label-warning"><b>ADEUDO</b></span>';
                                        }else if ($devolucion->estado == 1){
                                            $estado = '<span class="span label-warning"><b>EN PROCESO</b></span>';
                                        }
                                    ?>
                                    <tr inde="<?php echo $devolucion->IdVenta; ?>">
                                            <td><?php echo $devolucion->Nfactura; ?> </td>
                                            <td><?php echo $devolucion->f_venta; ?></td>
                                            <td><?php echo $devolucion->dui; ?></td>
                                            <td><?php echo $devolucion->n_clientes.' '.$devolucion->n_apellidos; ?> </td>
                                            <td><?php echo $devolucion->tipoventa; ?></td>
                                            <td><?php echo $estado; ?></td>
                                            <td><b>$ <?php echo $devolucion->totalvent; ?></b></td>
                                        
                                            <td>
                                            <?php ?>
                                                    <!-- <a href="<?php //echo RUTA_URL; ?>/devolucion/agregardevolucion/<?php //echo $compra->idcompra; ?>" title="Agregar Devolución" class="btn btn-success"><i class='nav-icon fas fa-file-alt'></i></a> -->
                                                    <button onClick="val('<?php echo $devolucion->IdVenta; ?>','<?php echo $devolucion->Nfactura; ?>','<td><?php echo $devolucion->n_clientes.' '.$devolucion->n_apellidos; ?>')" title="Devolución de Producto" class="btn btn-primary abc"><i class='nav-icon fas fa-edit'></i></button>
                                                    <!-- <a href="<?php //echo RUTA_URL; ?>/compras/editardetallecontado/<?php //echo $compra->idcompra; ?>" title="editar detalle" class="btn btn-primary"><i class='nav-icon fas fa-pen-square'></i></a>
                                                    <a href="<?php //echo RUTA_URL; ?>/compras/eliminarcompra/<?php //echo $compra->idcompra; ?>" title="Eliminar Compra" class="btn btn-danger"><i class='nav-icon fas fa-trash-alt'></i></a> --> 
                                            <?php?>
                                            </td>
                                        </tr>
                                    <?php } ?>                 
                                </tbody>
                                <tfoot>
                                <tfoot>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <!-- modal para editar maestro  -->
        <div class="modal fade" id="adddevoluciones">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Devoluciones</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- <form id="compras-editar"> -->
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-4">                                
                                    <label for="fecha" class="control-label" id="lblfecha">Fecha de Devolución</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="date" class="form-control" id="fecha" name="fecha" required="true" placeholder="Fecha" value="<?php echo date("Y-m-d"); ?>">
                                    <input type="hidden" id="idv">
                                </div>
                            </div>
                            
                            <small>Seleccione el articulo a devolver de la factura No. <label id="s"></label> </small><br/>
                            <small>Nombre de Cliente <label id="sd"></label> </small>
                            <div class="form-group row">
                                <!-- style="border-style: solid double #068D91; color: #068D91;" -->
                                <div class="col-sm-8">
                                    <table class="table table-bordered table-striped table-sm" id="t">
                                        <thead>
                                            <tr>
                                                
                                                <th>Producto</th>
                                                <th>Cant.</th>
                                                <th>P/U</th>
                                                <th>Total</th>
                                                <th>Acciones</th>
                                            </tr>
                                        </thead>
                                        <tbody id="table-detalle1">
                                            
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td style="text-align: right;" colspan="3"><b>TOTAL</b></td>
                                                <td id="etotal"></td>
                                                <td></td>
                                            </tr>
                                        <tfoot>
                                    </table>
                                </div>
                                <!-- <div class="col-sm-1"></div> -->
                                <div class="col-sm-4" style="BORDER:GROOVE 10PX #068D91;">
                                    <hr>
                                    <input type="hidden" id="det">
                                    <label for="prd">Producto</label>
                                    <textarea class="form-control" name="prd" id="prd" cols="30" rows="1"></textarea>

                                    <label for="cant">Cantidad de venta</label>
                                    <input onkeypress="return soloenteros(event)" class="form-control" type="number" name="cant" id="cant">

                                    <label for="cantdv">Cantidad a devolver</label>
                                    <input onkeypress="return soloenteros(event)" class="form-control" type="number" name="cantdv" id="cantdv">
                                    <hr>
                                </div>
                                
                            </div>
                            <hr>
                            </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="button" class="btn btn-primary" id="btndev">Realizar Devolución</button>
                        </div>
                    <!-- </form> -->
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>























        <!-- modal para confirmar devolucion  -->
        <div class="modal fade" id="okis">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 id="encab">Devolución Correcta</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <a href="<?php echo RUTA_URL; ?>/devoluciones/agregar" class="btn btn-success form-control" id="ok">OK</a>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <!-- <button class="btn btn-danger" data-dismiss="modal">CERRAR</button> -->
                            <!-- <button class="btn btn-primary" id="ok">Aceptar</button> -->
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>


























        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/devoluciones.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>