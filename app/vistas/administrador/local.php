<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Sucursales y Cargos de empleados</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Sucursales y Cargos de empleados</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Datos Generales</h3>  
                            </div>                         
                        </div>                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <!-- Main content -->
                            <section class="content">
                                <div class="container-fluid">
                                    <div class="row">
                                    <!-- left column -->
                                    <div class="col-md-4">
                                        <!-- general form elements -->
                                        <div class="card card-primary">
                                        <div class="card-header">
                                            <h3 class="card-title">Cargos de empleado</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <!-- <form role="form"> -->
                                            <div class="card-footer">
                                                <button id="cargo" class="float-sm-right btn btn-success">
                                                    <i class="nav-icon fas fa-plus-square"></i> 
                                                    <b>Cargo</b>
                                                </button>
                                            </div>

                                            <div class="card-body table-responsive">
                                                <table id="example1" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Cargo</th>  
                                                            <th>Acciones</th>                                                       
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbcategoria">
                                                        <?php foreach ($datos['cargos'] as $cargos) { ?>                                                            
                                                            <tr inde="<?php echo $cargos->id_cargo; ?>">
                                                                <td><?php echo $cargos->cargo; ?> </td>
                                                                <td>
                                                                    <button title="Editar Cargo" class="btn btn-primary edca">
                                                                    <i class='nav-icon fas fa-edit'></i>
                                                                    </button>
                                                                    <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                                    <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>                            
                                                </table>
                                            </div>
                                            <!-- /.card-body -->                
                                        <!-- </form> -->
                                        </div>
                                        <!-- /.card -->

                                    </div>
                                    <!-- <div class="col-md-1"></div> -->
                                    <!--/.col (left) -->
                                    <!-- right column -->
                                    <div class="col-md-8">
                                        <!-- Horizontal Form -->
                                        <div class="card card-info">
                                        <div class="card-header">
                                            <h3 class="card-title">Sucursales</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <!-- form start -->
                                        <!-- <form class="form-horizontal"> -->
                                            <div class="card-footer">
                                            <button id="btnaddsuc" class="float-sm-right btn btn-success">
                                                    <i class="nav-icon fas fa-plus-square"></i> 
                                                    <b>Sucursal</b>
                                                </button>
                                            </div>
                                            <div class="card-body table-responsive">
                                                <table id="example2" class="table table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>Sucursal</th>
                                                            <th>direccion</th> 
                                                            <th>Telefono</th>
                                                            <th>Acciones</th>                                                        
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach ($datos['sucursal'] as $sucursa) { ?>
                                                            <tr inde="<?php echo $sucursa->id_sucursal; ?>">
                                                                <td><?php echo $sucursa->nombre; ?> </td>
                                                                <td><?php echo $sucursa->direccion; ?> </td>
                                                                <td><?php echo $sucursa->tel; ?> </td>
                                                                <td>
                                                                    <button title="Editar Sucursal" class="btn btn-primary edsuc">
                                                                        <i class='nav-icon fas fa-edit'></i>
                                                                    </button>
                                                                    <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                                    <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                                                </td>
                                                            </tr>
                                                        <?php } ?>
                                                    </tbody>                            
                                                </table>
                                            </div>
                                            <!-- /.card-body -->
                                        <!-- </form> -->
                                        </div>
                                        <!-- /.card -->            
                                    </div>
                                    <!--/.col (right) -->
                                    </div>
                                    <!-- /.row -->
                                </div><!-- /.container-fluid -->
                                </section>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>   
        <!-- modal para agregar cargo  -->
        <div class="modal fade" id="addcargo">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Cargo de empleado</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                    <label for="cate" class="control-label" id="lblcate">Cargo</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cargoemp" required="true" placeholder="Cargo">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button id="btnadcargo" class="btn btn-primary">Guardar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

<!-- modal para editar cargo  -->
<div class="modal fade" id="editcargo">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Cargo de empleado</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <!-- <input type="text" id="ruta" value="<?php //echo RUTA_URL;?>" readonly> -->
                                    <input type="hidden" id="idcargo">
                                    <label for="cate" class="control-label" id="lblcate">Cargo</label>
                                </div>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" id="cargoempedit" required="true" placeholder="Cargo">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button id="btneditcargo" class="btn btn-primary">Actualizar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- modal para agregar sucursal -->
        <div class="modal fade" id="addsucsl">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encabe">Sucursales</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="suc" class="control-label" id="lbldesc">Sucursal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="suc" placeholder="Ingresar Sucursal" required="true" title="Ingresar Sucursal">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="dire" class="control-label" id="lbldesc">Dirección </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="dire" title="Ingresar Dirección" placeholder="Ingresar Dirección" required="true">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="tel" class="control-label" id="lbldesc">Teléfono</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="tel" title="Ingresar Teléfono" placeholder="Ingresar Teléfono" required="true">
                                </div>
                            </div>
                        </div><hr>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button id="btnadsucursal" class="btn btn-primary">Guardar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
        <!-- /.modal-dialog -->
        </div>

        <!-- modal para editar sucursal -->
        <div class="modal fade" id="editsucsl">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encabe">Sucursales</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <div class="modal-body">
                        <div class="form-group row">
                                <div class="col-sm-10">
                                    <input type="hidden" readonly class="form-control" id="codi" name="codi" placeholder="Codigo" required="true">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="sucedit" class="control-label" id="lbldesc">Sucursal</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="sucedit" placeholder="Ingresar Sucursal" required="true" title="Ingresar Sucursal">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="direedit" class="control-label" id="lbldesc">Dirección </label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="direedit" title="Ingresar Dirección" placeholder="Ingresar Dirección" required="true">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-2">
                                    <label for="teledit" class="control-label" id="lbldesc">Teléfono</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="text" class="form-control" id="teledit" title="Ingresar Teléfono" placeholder="Ingresar Teléfono" required="true">
                                </div>
                            </div>
                        </div><hr>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button id="btneditsucursal" class="btn btn-primary">Actualizar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
        <!-- /.modal-dialog -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/admin.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            });

            $('#example3').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true,
            });
        });
        </script>
    </body>
</html>