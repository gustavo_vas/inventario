<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Control de usuarios</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Control de usuarios</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Datos de usuario</h3>  
                            </div>
                            <div class="col-sm-12">
                                <a href="<?php echo RUTA_URL; ?>/empleados/agregar" class="float-sm-right btn btn-success">
                                    <i class="nav-icon fas fa-user-plus"> 
                                        <b>Agregar Empleado</b>
                                    </i>
                                </a>
                            </div><!-- /.col -->                         
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Cargo</th>
                                        <th>Estado</th>
                                        <th>Sucursal</th>
                                        <th>Usuario</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['users'] as $users) { ?>
                                        <tr>
                                            <td><?php echo $users->nombre; ?> </td>
                                            <td><?php echo $users->apellido; ?></td>
                                            <td><?php echo $users->cargo; ?></td>
                                            <td>
                                                <?php 
                                                    if($users->estado == 1){
                                                        echo "<span class='span label-success'><b>Activo</b></span>";
                                                    }else{
                                                        echo "<span class='span label-danger'><b>Inactivo</b></span>";
                                                    }                                               
                                                ?></span>
                                            </td>
                                            <td><?php echo $users->sucursal; ?></td>
                                            <td><?php echo $users->usuario; ?></td>
                                            <td>
                                                <a href="<?php echo RUTA_URL; ?>/empleados/editar/<?php echo $users->code; ?>" title="Editar Empleado" class="btn btn-primary"><i class='nav-icon fas fa-edit'></i></a>
                                                <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>