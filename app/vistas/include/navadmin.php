    <div class="wrapper">
        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                <a href="<?php echo RUTA_URL;?>/admins1/inicio" class="nav-link">Inicio</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                <a href="#" class="nav-link">Ayuda</a>
                </li>
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-3">
                <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" placeholder="Buscar" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-navbar" type="submit">
                    <i class="fas fa-search"></i>
                    </button>
                </div>
                </div>
            </form>

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                <a href="<?php echo RUTA_URL; ?>/admins1/destroySesion" class="nav-link">
                    <i class="nav-icon fas fa-sign-out-alt"></i>
                    Salir
                </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
        <!-- Brand Logo -->
        <a href="#" class="brand-link">
            <img src="<?php echo RUTA_URL;?>/img/logo.png" alt="Logo" class="brand-image img-circle elevation-3"
                style="opacity: .8">
            <span class="brand-text font-weight-light">Administrador</span>
        </a>

        <!-- Sidebar -->
        <div class="sidebar">
            <!-- Sidebar user panel (optional) -->
            <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block"><?php echo Sesion::getSesion('nombreUser'). ' '. Sesion::getSesion('apellidoUser'); ?></a>
            </div>
            </div>

            <!-- Sidebar Menu -->
            <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                    with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview menu-open">
                <a href="<?php echo RUTA_URL;?>/admins1/inicio" class="nav-link active">
                    <i class="nav-icon fas fa-tachometer-alt"></i>
                    <p>
                        Inicio
                    </p>
                </a>
                </li>

                <!-- <li class="nav-item">
                    <a href="<?php //echo RUTA_URL; ?>/empleados/" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>Control de Empleados</p>
                    </a>
                </li> -->
                
                <!-- <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-user"></i>
                        <p>
                            Mi Perfil
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">1</span>
                        </p>
                    </a> 
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php //echo RUTA_URL; ?>/Usuarios/miperfil" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>Perfil</p>
                            </a>
                        </li>
                    </ul>                   
                </li> -->

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            Administración
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">7</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/Clientes/agregar" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Agregar Cliente</p>
                            </a>
                        </li>
                        
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/empleados/agregar" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Agregar Empleado</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/proveedores/agregar" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Agregar Proveedor</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/Clientes/" class="nav-link">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p>Control de Clientes</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/empleados/" class="nav-link">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p>Control de Empleados</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/proveedores/" class="nav-link">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p>Control de Proveedores</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/Admins1/local" class="nav-link">
                                <i class="nav-icon fas fa-user-cog"></i>
                                <p>Cargos y Sucursales</p>
                            </a>
                        </li>

                        <!-- <li class="nav-item">
                            <a href="<?php //echo RUTA_URL; ?>/usuarios/" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>Control de Usuarios</p>
                            </a>
                        </li> -->
                        
                    </ul>
                </li>

                <!-- <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-money-check-alt"></i>
                        <p>
                            Abonos
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">2</span>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php //echo RUTA_URL; ?>/errores/mantenimiento" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Proveedor</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php //echo RUTA_URL; ?>/errores/mantenimiento" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Clientes</p>
                            </a>
                        </li>                       
                    </ul>
                </li> -->

                <!-- menu de productos -->
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Catálogo Productos
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">3</span>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/productos/mostrarCM" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Categorías y Marcas</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/productos/agregar" class="nav-link">
                                <i class="nav-icon fas fa-user-plus"></i>
                                <p>Agregar Productos</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/productos/" class="nav-link">
                                <i class="nav-icon fas fa-user"></i>
                                <p>Catálogo Productos</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-shopping-cart"></i>
                        <p>
                            Compras
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">4</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/compras/agregar" class="nav-link">
                                <i class="nav-icon fas fa-cart-plus"></i>
                                <p>Realizar Compra Contado</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/compras/agregarCredito" class="nav-link">
                                <i class="nav-icon fas fa-cart-plus"></i>
                                <p>Realizar Compra Crédito</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/compras/" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Compras al Contado</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/compras/credito" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Compras al Crédito</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <!-- menu Inventario -->
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-clipboard-list"></i>
                        <p>
                            Inventario
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">1</span>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/inventarios/" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                <p>Productos</p>
                            </a>
                        </li>                       
                    </ul>
                </li>

                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>
                            Ventas
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">4</span>
                        </p>
                    </a>

                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/ventas/agregar" class="nav-link">
                                <i class="nav-icon fas fa-cart-plus"></i>
                                <p>Realizar Venta Contado</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/ventas/agregarcredito" class="nav-link">
                                <i class="nav-icon fas fa-cart-plus"></i>
                                <p>Realizar Venta Crédito</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/ventas/" class="nav-link">
                            <!-- <a href="<?php //echo RUTA_URL; ?>/errores/mantenimiento" class="nav-link"> -->
                                <i class="nav-icon fas fa-money-check-alt"></i>
                                    <p>Ventas Contado</p>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/ventas/credito" class="nav-link">
                            <!-- <a href="<?php //echo RUTA_URL; ?>/errores/mantenimiento" class="nav-link"> -->
                                <i class="nav-icon fas fa-credit-card"></i>
                                <p>Ventas Crédito</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>
                            Devoluciones
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">2</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/devoluciones/agregar" class="nav-link">
                                <i class="nav-icon fas fa-cart-arrow-down"> </i>
                                    <p>Realizar Devolución</p>                                
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/devoluciones/" class="nav-link">
                                <i class="nav-icon fas fa-clipboard-list"></i>
                                    <p>Devoluciones de Artículos</p>
                            </a>
                        </li>                        
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>
                            Control Usuarios
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">1</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/usuarios/" class="nav-link">
                                <i class="nav-icon fas fa-cart-arrow-down"> </i>
                                    <p>Control Usuarios</p>                                
                            </a>
                        </li>                        
                    </ul>
                </li>
                <li class="nav-item has-treeview">
                    <a href="#" class="nav-link" style="background:#086A87; color:#fff">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>
                            Movimientos
                            <i class="fas fa-angle-left right"></i>
                            <span class="badge badge-info right">1</span>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="<?php echo RUTA_URL; ?>/Finanzas/" class="nav-link">
                                <i class="nav-icon fas fa-cart-arrow-down"> </i>
                                    <p>$ Compras</p>                                
                            </a>
                        </li>                        
                    </ul>
                </li>
            </ul>
            </nav>
        </div>
        </aside>