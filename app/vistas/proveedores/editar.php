<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Proveedor</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Editar Proveedor</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <!-- <div class="row"> -->
                <!-- <div class="col-12"> -->
                    <!-- <div class="card"> -->                        
                        <!-- /.card-header -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Datos del Proveedor</h3>
                                    </div>
                                        <form role="form" action="<?php echo RUTA_URL; ?>/proveedores/editar/<?php echo $datos['codigo']; ?>" method="POST">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="nombre" class="control-label">Nombre</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" required="true" value="<?php echo $datos['nombres']; ?>">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="tel" class="control-label">Telefono</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="tel" name="tel" placeholder="Telefono" value="<?php echo $datos['tel']; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="direccion" class="control-label">Direccion</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direcccion" value="<?php echo $datos['direccion']; ?>">
                                                    </div>                                                    

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="nrc" class="control-label">NRC</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="nrc" name="nrc" placeholder="NRC" value="<?php echo $datos['nrc']; ?>">
                                                    </div>                                                                         
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="dui" class="control-label">DUI</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="dui" name="dui" placeholder="DUI" required value="<?php echo $datos['dui']; ?>">
                                                    </div>

                                                    <div class="col-sm-1"></div>
                                                    <div class="col-sm-1">
                                                        <label for="actividad" class="control-label">Actividad</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="actividad" name="actividad" placeholder="Actividad" required value="<?php echo $datos['actividad']; ?>">
                                                    </div>
                                                                                                   
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="nit" class="control-label">NIT</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="nit" name="nit" placeholder="NIT" required value="<?php echo $datos['nit']; ?>">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="tp" class="control-label">Tipo</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="tp" name="tp">
                                                            <option value="<?php echo $datos['tipo']; ?>"><?php echo $datos['descripcion']; ?></option>
                                                            <?php foreach ($datos['tipop'] as $tps) { ?>
                                                                <option value="<?php echo $tps->id_tipo_proveedor; ?>">
                                                                    <?php echo $tps->descripcion; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>    
                                                </div>

                                                     
                                            </div>
                                        <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="nav-icon fas fa-save">
                                                        <b>Guardar Datos</b>
                                                    </i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                    <!-- /.card -->
                <!-- </div> -->
            <!-- </div> -->
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>        
    </body>
</html>