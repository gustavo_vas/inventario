<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Control de Proveedores</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Control de Proveedores</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Datos de los Proveedores</h3>  
                            </div>
                            <div class="col-sm-12">
                                <a href="<?php echo RUTA_URL; ?>/proveedores/agregar" class="float-sm-right btn btn-success">
                                    <i class="nav-icon fas fa-user-plus"> 
                                        <b>Agregar Proveedor</b>
                                    </i>
                                </a>
                            </div><!-- /.col -->                         
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Dirección</th>
                                        <th>DUI</th>
                                        <th>NIT</th>
                                        <th>Teléfono</th>
                                        <th>NRC</th>
                                        <th>Actividad</th>
                                        <th>Tipo</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['proveedores'] as $proveedor) { ?>
                                        <tr>
                                            <td><?php echo $proveedor->nombre; ?> </td>
                                            <td><?php echo $proveedor->direccion; ?></td>
                                            <td><?php echo $proveedor->dui; ?></td>
                                            <td><?php echo $proveedor->nit; ?></td>
                                            <td><?php echo $proveedor->tel; ?></td>
                                            <td><?php echo $proveedor->nrc; ?></td>
                                            <td><?php echo $proveedor->actividad; ?></td>
                                            <td><?php echo $proveedor->descripcion; ?></td>
                                            <td>
                                                <a href="<?php echo RUTA_URL; ?>/proveedores/editar/<?php echo $proveedor->code; ?>" title="Editar Proveedor" class="btn btn-primary"><i class='nav-icon fas fa-edit'></i></a>
                                                <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>