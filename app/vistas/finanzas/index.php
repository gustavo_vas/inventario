<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Panel de Compras</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Panel de Compras</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header form-group row">
                            <input type="hidden" id="ruta1" value="<?php echo RUTA_URL;?>" readonly>
                            <div class="col-sm-3 form-group row">
                                <label class="col-sm-3" for="fechainit">Inicio</label>
                                <input title="Fecha de Inicio" type="date" class="form-control col-sm-9" id="fechainit" name="fechainit" placeholder="Fecha" value="<?php echo date("Y-m-d"); ?>">
                            </div>

                            <!-- <div class="col-sm-1"></div> -->

                            <div class="col-sm-3 form-group row">
                                <label class="col-sm-2" for="fechafin">Fin</label>
                                <input title="Fecha Fin" type="date" class="form-control col-sm-9" id="fechafin" name="fechafin" placeholder="Fecha" value="<?php echo date("Y-m-d"); ?>">
                            </div>
                            <div class="col-sm-2">
                                <select title="Tipo de compra" name="tipoco" id="tipoco" class="form-control">
                                    <?php foreach ($datos['tipocomp'] as $tpc) { ?>
                                        <option value="<?php echo $tpc->id_tipo_compra; ?>">
                                            <?php echo $tpc->descripcion; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <select title="Estado de compra" name="estad" id="estad" class="form-control">
                                    <option value="1">EN PROCESO</option>
                                    <option value="2">ADEUDO</option>
                                    <option value="3">CANCELADA</option>
                                    <option value="4">TODAS</option>
                                </select>
                            </div>

                            <div class="col-sm-2">
                                <button title="Mostrar Registros" class="btn btn-success" id="btnbuscar">
                                    <i class="nav-icon fas fa-search">
                                        <b>Mostrar</b>
                                    </i>
                                </button>
                            </div>
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="col-12 table-responsive">
                                        <table id="example1" class="table table-bordered table-striped">
                                            <thead>
                                            <input type="text" name="" id="buscador">
                                                <tr>
                                                    <th># Factura</th>
                                                    <!-- <th>Usuario</th> -->
                                                    <!-- <th>Proveedor</th> -->
                                                    <!-- <th>Tipo</th> -->
                                                    <th>Estado</th>
                                                    <th>Sucursal</th>
                                                    <th>Fecha</th>                                        
                                                    <th>Total</th>
                                                    <th>IVA 13%</th>
                                                    <th>Subtotal</th>
                                                    <!-- <th>Acciones</th> -->
                                                </tr>
                                            </thead>
                                            <tbody id="finanzas1">
                                                
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <td style="text-align: right;" colspan="4"><b>TOTAL</b></td>
                                                    <td id="total"></td>
                                                    <td id="iva"></td>
                                                    <td id="subtotal"></td>
                                                </tr>
                                            </tfoot>                            
                                        </table>
                                    </div>
                                </div>
                                <div class="col-md-3" style="BORDER:GROOVE 10PX #068D91;">
                                    <p class="lead">Montos de Facturación</p>

                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped">
                                        <tr>
                                            <th style="width:50%">Subtotal:</th>
                                            <td id="subtot"></td>
                                        </tr>
                                        <tr>
                                            <th>IVA (13%)</th>
                                            <td id="iva2"></td>
                                        </tr>
                                        <tr>
                                            <th>Total:</th>
                                            <td id="tot2"></td>
                                        </tr>
                                        </table><hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <!-- modal para editar maestro  -->
        <div class="modal fade" id="editmaster">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Factura de Compra</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="compras-editar">
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <div class="form-group row">
                                <input type="hidden" id="idcomedit">
                                <div class="col-sm-3">                                
                                    <label for="nfact" class="control-label" id="lblnfact">No. Factura</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nfact" name="nfact" required="true" placeholder="No. Factura">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="prefijo" class="control-label" id="lblpref">Prefijo</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="prefijo" name="prefijo" placeholder="Prefijo">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="fecha" class="control-label" id="lblfecha">Fecha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="fecha" name="fecha" required="true" placeholder="Fecha">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="prov" class="control-label" id="lblprov">Proveedor</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="provee" id="provee" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($datos['proveedores'] as $prov) { ?>
                                            <option value="<?php echo $prov->code; ?>">
                                                <?php echo $prov->nombre; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="tipo" class="control-label" id="lbltipo">Tipo</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="tipocom" id="tipocom" class="form-control">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="estad" class="control-label" id="lblestad">Estado</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control" id="estad" name="estad" required="true">
                                        <option value="2">EN PROCESO</option>
                                        <option value="1">CANCELAR</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="btnactcompra">Actualizar Compra</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/finanzas.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": true,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>