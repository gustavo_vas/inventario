<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Control de Empleados</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Control de Empleados</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Datos de los Empleados</h3>  
                            </div>
                            <div class="col-sm-12">
                                <a href="<?php echo RUTA_URL; ?>/empleados/agregar" class="float-sm-right btn btn-success">
                                    <i class="nav-icon fas fa-user-plus"> 
                                        <b>Agregar Empleado</b>
                                    </i>
                                </a>
                            </div><!-- /.col -->                         
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>Dirección</th>
                                        <th>DUI</th>
                                        <th>Cargo</th>
                                        <th>Sueldo</th>
                                        <th>Estado</th>
                                        <th>Sucursal</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['empleados'] as $empleado) { ?>
                                        <tr inde="<?php echo $empleado->code; ?>">
                                            <td><?php echo $empleado->nombre; ?> </td>
                                            <td><?php echo $empleado->apellido; ?></td>
                                            <td><?php echo $empleado->direccion; ?></td>
                                            <td><?php echo $empleado->dui; ?></td>
                                            <td><?php echo $empleado->cargo; ?></td>
                                            <td><?php echo '$ '.$empleado->sueldo; ?></td>
                                            <td>
                                                <?php 
                                                    if($empleado->estado == 1){
                                                        echo "<span class='span label-success'><b>Activo</b></span>";
                                                    }else{
                                                        echo "<span class='span label-danger'><b>Inactivo</b></span>";
                                                    }                                               
                                                ?></span>
                                            </td>
                                            <td><?php echo $empleado->sucursal; ?></td>
                                            <td>
                                                <a href="<?php echo RUTA_URL; ?>/empleados/editar/<?php echo $empleado->code; ?>" title="Editar Empleado" class="btn btn-primary"><i class='nav-icon fas fa-edit'></i></a>
                                                <button title="Asignar Usuario" class="btn btn-info asus"><i class="nav-icon fas fa-user"></i></button>
                                                <button title="Eliminar Empleado" class="btn btn-danger"><i class="nav-icon fas fa-trash-alt"></i></button>
                                                <!-- <button type="button" title="Editar Empleado" class="btn btn-info"><i class='nav-icon fas fa-edit'></i></button> -->
                                                <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        







        <!-- modal para editar maestro  -->
        <div class="modal fade" id="adduser">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Asignar Usuario</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- <form id="ventas-editar"> -->
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <div class="form-group row">
                                <input type="hidden" id="userid">
                                <div class="col-sm-5">                                
                                    <label for="user" class="control-label" id="lbluser">Nombre de usuario</label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="text" class="form-control" id="user" name="user" required="true" placeholder="Usuario">
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="vende" class="control-label" id="lblvende">Contraseña</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="vende" id="vende" class="form-control">
                                        <option value=""></option>
                                        <?php //foreach ($datos['vendedores'] as $vende) { ?>
                                            <option value="<?php// echo $vende->code; ?>">
                                                <?php //echo $vende->nombre.' '.$vende->apellido; ?>
                                            </option>
                                        <?php// } ?>
                                    </select>
                                </div>
                            </div> -->

                            <div class="form-group row">
                                <div class="col-sm-5">                                
                                <label for="contra" class="control-label" id="lblcontra">Contraseña</label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control" id="contra" name="contra" required="true" placeholder="Contraseña">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-5">                                
                                <label for="confcontra" class="control-label" id="lblconcontra">Confirmar Contraseña</label>
                                </div>
                                <div class="col-sm-7">
                                    <input type="password" class="form-control" id="confcontra" name="confcontra" required="true" placeholder="Confirmar Contraseña">
                                </div>
                            </div>

                            <!-- <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="clie" class="control-label" id="lblclie">Cliente</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="clie" id="clie" class="form-control">
                                        <option value=""></option>
                                        <?php //foreach ($datos['clientes'] as $cliente) { ?>
                                            <option value="<?php// echo $cliente->code; ?>">
                                                <?php //echo $cliente->nombre.' '.$cliente->apellido; ?>
                                            </option>
                                        <?php// } ?>
                                    </select>
                                </div>
                            </div> -->

                            <!-- <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="tipo" class="control-label" id="lbltipo">Tipo</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="tipoven" id="tipoven" class="form-control">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div> -->

                            <!-- <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="estad" class="control-label" id="lblestad">Estado</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control" id="estad" name="estad" required="true">
                                        <option value="2">EN PROCESO</option>
                                        <option value="1">CANCELAR</option>
                                    </select>
                                </div>
                            </div> -->
                            <!-- <hr> -->
                            </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button class="btn btn-primary" id="btnadduser">Asignar</button>
                        </div>
                    <!-- </form> -->
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>







        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/usuarios.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>