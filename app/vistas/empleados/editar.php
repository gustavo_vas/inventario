<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Empleado</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Editar Empleado</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <!-- <div class="row"> -->
                <!-- <div class="col-12"> -->
                    <!-- <div class="card"> -->                        
                        <!-- /.card-header -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Datos del Empleado</h3>
                                    </div>
                                        <form role="form" action="<?php echo RUTA_URL; ?>/empleados/editar/<?php echo $datos['codigo']; ?>" method="POST">
                                            <div class="card-body">
                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="nombre" class="control-label">Nombres</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombres" required="true" value="<?php echo $datos['nombres']; ?>">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="afp" class="control-label">AFP</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="afp" name="afp" placeholder="AFP" value="<?php echo $datos['afp']; ?>">
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="apellido" class="control-label">Apellidos</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellidos" required="true" value="<?php echo $datos['apellidos']; ?>">
                                                    </div>                                                    

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="isss" class="control-label">ISSS</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="isss" name="isss" placeholder="ISSS" value="<?php echo $datos['isss']; ?>">
                                                    </div>                                                    
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="direccion" class="control-label">Direccion</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Direcccion" value="<?php echo $datos['direccion']; ?>">
                                                    </div>                                                    

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="cargo" class="control-label">Cargo</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="cargo" name="cargo">
                                                            <option value="<?php echo $datos['idcargo']; ?>"><?php echo $datos['cargo']; ?></option>
                                                            <?php foreach ($datos['cargos'] as $cargos) { ?>
                                                                <option value="<?php echo $cargos->id_cargo; ?>">
                                                                    <?php echo $cargos->cargo; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="dui" class="control-label">DUI</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="dui" name="dui" placeholder="DUI" required value="<?php echo $datos['dui']; ?>">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="sucursal" class="control-label">Sucursal</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="sucursal" name="sucursal">
                                                            <option value="<?php echo $datos['idsucursal']; ?>"><?php echo $datos['sucursal']; ?></option>
                                                            <?php foreach ($datos['sucursales'] as $sucursales) { ?>
                                                                <option value="<?php echo $sucursales->id_sucursal; ?>">
                                                                    <?php echo $sucursales->nombre; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                    
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="nit" class="control-label">NIT</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="nit" name="nit" placeholder="NIT" required value="<?php echo $datos['nit']; ?>">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="estado" class="control-label">Estado</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="estado" name="estado">
                                                            <option value="<?php echo $datos['estado']; ?>">
                                                                <?php 
                                                                    if($datos['estado'] == 1){
                                                                        echo 'ACTIVO';
                                                                    } else {
                                                                        echo 'INACTIVO';
                                                                    }                                                                    
                                                                ?>
                                                            </option>
                                                            <option value="1">ACTIVO</option>
                                                            <option value="0">INACTIVO</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="salario" class="control-label">Salario</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="number" class="form-control" id="salario" name="salario" placeholder="000.00" required value="<?php echo $datos['sueldo']; ?>">
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="nav-icon fas fa-save">
                                                        <b>Guardar Datos</b>
                                                    </i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                    <!-- /.card -->
                <!-- </div> -->
            <!-- </div> -->
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>        
    </body>
</html>