<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php //echo RUTA_URL;?>/css/select2.css"> -->
        <!-- <script src="<?php //echo RUTA_URL;?>/js/select2.js"></script> -->
        <!-- <script>
            $(document).ready(function() {
                 $("#provee").select2(); 
                });
        </script> -->
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Factura de Venta</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Factura de Venta</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row" style="justify-content: center; align-items: center;">
                <div class="col-11">
                    <div class="card">
                        <div class="card-header">

                            <form id="editventas-form"> 
                                <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <input type="hidden" id="idvent" name="idvent" value="<?php echo $datos['id']; ?>" readonly>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <input title="No. Factura" disabled="true" type="text" class="form-control" id="numf" name="numf" placeholder="# Factura" required="true" value="<?php echo $datos['nfac']; ?>">
                                    </div>
                                    <!-- <div class="col-sm-3">
                                        <input type="text" class="form-control" id="pref" name="pref" placeholder="Prefijo">
                                    </div> -->
                                    <div class="col-sm-3">
                                        <!-- <input type="text" name="prove" list="prove" placeholder="Seleccionar Proveedor" class="form-control"/> -->
                                        <!-- <datalist id="prove"> -->
                                                <select disabled="true" title="Vendedor" name="emple" id="emple" class="form-control">
                                                    <option value="<?php echo $datos['idemple']; ?>"><?php echo $datos['nomemp'].' '.$datos['apellemp']; ?></option>
                                                </select>
                                        <!-- </datalist> -->
                                    </div>

                                    <div class="col-sm-3">
                                        <input disabled="true" title="Fecha de Facturacion" value="<?php echo $datos['fecha']; ?>" type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha" >
                                    </div>

                                    <!-- <div class="col-sm-3">
                                        <input title="Comision de Empleado por Venta" type="number" class="form-control" id="comision" name="comision" placeholder="Comision Empleado" >
                                    </div> -->
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <!-- <input type="text" name="prove" list="prove" placeholder="Seleccionar Proveedor" class="form-control"/> -->
                                        <!-- <datalist id="prove"> -->
                                                <select disabled="true" title="Cliente" name="clie" id="clie" class="form-control">
                                                    <option value="<?php echo $datos['idclie']; ?>"><?php echo $datos['nomclie'].' '.$datos['apellclie']; ?></option>                                                    
                                                </select>
                                        <!-- </datalist> -->
                                    </div>
                                    <div class="col-sm-3">
                                        <select title="Tipo de Venta" name="tipoven" id="tipoven" class="form-control">
                                                <option value="<?php echo $datos['idtpv']; ?>">
                                                    <?php echo $datos['tipov']; ?>
                                                </option>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <input title="Estado" readonly required type="text" class="form-control" id="estad" name="estad" placeholder="Estado Compra" value="EN PROCESO">
                                    </div>

                                    <!-- <div class="col-sm-3">
                                        <button type="button" class="btn btn-success" id="btndetalle">
                                            <i class="nav-icon fas fa-forward">
                                                <b>Siguiente</b>
                                            </i>
                                        </button>
                                    </div> -->
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-2">
                                        <label for="editcomision" id="lbleditcomi">Comision de Vendedor</label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input title="Comision de Empleado por Venta" value="<?php echo $datos['comision']; ?>" type="number" class="form-control" id="editcomision" name="editcomision" placeholder="Comision Empleado" >
                                    </div>
                                </div>
                            <!-- </form>    -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" id="editdetalle">
                            <!-- <form id="form-detalle"> -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                        <select name="eprod" id="eprod" class="form-control">
                                            <option value="">Seleccionar Producto</option>
                                            <?php foreach ($datos['inventario'] as $invent) { ?>
                                                <option value="<?php echo $invent->codigo_producto; ?>">
                                                    <?php echo $invent->loteproducto; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="ecantidad" placeholder="Cantidad" required>
                                </div>

                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="ecostunit" placeholder="Precio Unitario" required>
                                </div>

                                <div class="col-sm-3">
                                        <button type="button" class="btn btn-success swalDefaultError" id="emas">
                                            <i class="nav-icon fas fa-plus">
                                                <b></b>
                                            </i>
                                        </button>
                                    </div>
                            </div>
                            <table class="table table-bordered table-striped table-sm" id="t">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <!-- <th>IVA</th> -->
                                        <th>Total</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="table-detalle">
                                    <?php $total = 0;
                                     foreach ($datos['detalle'] as $detalle) {
                                        $total += $detalle->total; ?>
                                        <tr ides="<?php echo $detalle->iddetalle; ?>">
                                            <td><?php echo $detalle->codpd; ?> </td>
                                            <td><?php echo $detalle->producto; ?> </td>
                                            <td><?php echo $detalle->cantidad; ?> </td>
                                            <td><?php echo $detalle->precio; ?> </td>
                                            <td><?php echo $detalle->total; ?> </td>
                                            <td><button type="button" class="btn btn-danger deldet"><i class="nav-icon fas fa-trash-alt"></i></button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="text-align: right;" colspan="4"><b>TOTAL</b></td>
                                        <td id="total">$ <?php echo $datos['totv']; ?></td>
                                        <td></td>
                                    </tr>
                                <tfoot>
                            </table><hr>
                           
                            <div class="col-sm-7">
                                <a href="<?php echo RUTA_URL; ?>/ventas/" class="btn btn-success" >
                                    <i class="nav-icon fas fa-cart-plus">
                                        <b>Actualizar Venta</b>
                                    </i>
                                </a>
                                <a href="<?php echo RUTA_URL; ?>/ventas/" class="btn btn-danger" >
                                    <i class="nav-icon fas fa-window-close">
                                        <b>Cancelar Venta</b>
                                    </i>
                                </a>
                            </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/ventas.js"></script>
        <script src="<?php echo RUTA_URL;?>/js/jquery.validate.js"></script>

        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>

        <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" -->
  <!-- crossorigin="anonymous"></script> -->
    </body>
</html>