<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>
    <body class="hold-transition sidebar-mini">
        <?php //require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <div class="wrapper">
            <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Factura de Venta</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                <li class="breadcrumb-item active">Ventas</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-12">
                    <!-- <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Nota:</h5>
                        Esta sección solo permite lectura, el Usuario no puede realizar ninguna acción sobre este documento.
                    </div> -->


                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> Detalle de Factura No. <?php echo $datos['nfac']; ?>
                            <small class="float-right">Fecha de Facturación: <?php echo $datos['fecha']; ?></small>
                        </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        <b>Cliente: </b> <?php echo $datos['nomclie'].' '.$datos['apellclie']; ?>
                        <address>
                            DUI: <?php echo $datos['dui']; ?><br>
                            NIT: <?php echo $datos['nit']; ?><br>
                            Residencia: <?php echo $datos['direccion']; ?>
                        </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Factura No. </b> <?php echo $datos['nfac']; ?><br>
                            <b>Vendedor: </b><?php echo $datos['nomemp'].' '.$datos['apellemp']; ?> <br>
                            <b>Fecha: </b><?php echo $datos['fecha']; ?> <br>
                            <b>Sucursal: </b> <?php echo $datos['sucursal']; ?>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>Estado:</b>
                            <?php
                                if ($datos['estadov'] == 1) {
                                    echo '<span class="span label-warning"><b>EN PROCESO</b></span>';
                                }else if ($datos['estadov'] == 2){
                                    echo '<span class="span label-warning"><b>ADEUDO</b></span>';
                                }else if ($datos['estadov'] == 3){
                                    echo '<span class="span label-success"><b>CANCELADA</b></span>';
                                }
                            ?>
                            <br>
                            <b>Tipo de Venta: </b><?php echo $datos['tipov']; ?><br>
                            <b>Comision: $ </b><?php echo $datos['comision']; ?><br>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Reporte Detalle de Venta</h5>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-9">
                            <p class="text-center">
                            <strong>Detalle</strong>
                            </p>
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                    <th>Cant.</th>
                                    <th>Codigo</th>
                                    <th>Descripcion</th>
                                    <th>P/U</th>
                                    <th>Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($datos['detalle'] as $detalle) { ?>
                                            <tr>
                                                <td><?php echo $detalle->cantidad; ?> </td>
                                                <td><?php echo $detalle->codpd; ?> </td>
                                                <td><?php echo $detalle->producto; ?> </td>
                                                <td><?php echo $detalle->precio; ?> </td>
                                                <td><?php echo $detalle->total; ?> </td>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                            <p class="lead">Montos de Facturación</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tr>
                                        <th style="width:50%">Subtotal:</th>
                                        <td>$ <?php echo $datos['totsiniva']; ?></td>
                                    </tr>
                                    <tr>
                                        <th>IVA (13%)</th>
                                        <td>$ <?php echo $datos['totiva']; ?></td>
                                    </tr>
                                    <!-- <tr>
                                        <th>Shipping:</th>
                                        <td>$5.80</td>
                                    </tr> -->
                                    <tr>
                                        <th>Total:</th>
                                        <td>$ <?php echo $datos['totv']; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./card-body -->
                
                    <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                </div>
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>
