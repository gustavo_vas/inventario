<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>
    <body class="hold-transition sidebar-mini">
        <?php //require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <div class="wrapper">
            <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1>Detalle de Contrato</h1>
                        </div>
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="#">Inicio</a></li>
                                <li class="breadcrumb-item active">Ventas</li>
                            </ol>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </section>

            <section class="content">
            <div class="container-fluid">
                <div class="row">
                <div class="col-12">
                    <!-- <div class="callout callout-info">
                        <h5><i class="fas fa-info"></i> Nota:</h5>
                        Esta sección solo permite lectura, el Usuario no puede realizar ninguna acción sobre este documento.
                    </div> -->


                    <!-- Main content -->
                    <div class="invoice p-3 mb-3">
                    <!-- title row -->
                    <div class="row">
                        <div class="col-12">
                        <h4>
                            <i class="fas fa-globe"></i> Detalle de Contrato No.
                            <small class="float-right">Fecha de Contrato: <?php echo date("d/m/Y");?></small>
                        </h4>
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- info row -->
                    <div class="row invoice-info">
                        <div class="col-sm-4 invoice-col">
                        <b>Cliente: </b> <?php echo $datos['nomclie'].' '.$datos['apellclie']; ?>
                        <address>
                        <b>   DUI:</b> <?php echo $datos['dui']; ?><br>
                        <b>   NIT: </b><?php echo $datos['nit']; ?><br>
                        <b>    Fecha de Compra:</b> <?php echo $datos['fecha']; ?>
                           
                        </address>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                        <b>Dirección:</b> <?php echo $datos['direccion']; ?><br>
                        <b>Articulo* </b> <?php echo $datos['descpro']; ?><br>
                        <b>Marca*</b><?php echo $datos['marcapro']; ?> <br>    
                        <b>Modelo* </b> <?php echo $datos['sucursal']; ?>
                        </div>
                        <!-- /.col -->
                        <div class="col-sm-4 invoice-col">
                            <b>No. de Serie *: </b><?php echo $datos['codigopro']; ?><br>
                            <b>Precio Total *</b> <?php echo $datos['totv']; ?> <br>
                            
                        </div>
                        <!-- /.col -->
                    </div>
                    <!-- /.row -->

                    <!-- Table row -->
                    <div class="row">
                <div class="col-md-12">
                    <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Clausulas de Contrato</h5>

                        <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-widget="collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-widget="remove">
                            <i class="fas fa-times"></i>
                        </button>
                        </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <div class="row">
                        <div class="col-md-9">
                            <p class="text-center">
                            <strong>Detalle</strong>
                            </p>
                            <div class="col-12 table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                  
                                    <th>Descripcion</th>
                                    
                                    </tr>
                                    </thead>
                                    <tbody>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli1']; ?></textarea> </td>
                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli2']; ?></textarea> </td>
                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli3']; ?></textarea> </td>                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli4']; ?></textarea> </td>                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli5']; ?></textarea> </td>                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli6']; ?></textarea> </td>                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli7']; ?></textarea> </td>                                            </tr>
                                            <tr>
                                            <td> <textarea rows="2" cols="95" > <?php echo $datos['cli8']; ?></textarea> </td>                                            </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-md-3">
                        <p class="lead">Acciones</p>

                        </div>
                        <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- ./card-body -->
                
                    <!-- /.card-footer -->
                    </div>
                    <!-- /.card -->
                </div>
                <!-- /.col -->
                </div>
                    </div>
                    <!-- /.invoice -->
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
    </body>
</html>
