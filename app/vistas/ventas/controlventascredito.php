<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
        <script>
            function total(tot){
                document.getElementById("lbltotalc").innerHTML = tot.toFixed(2);
            }
        </script>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Panel de Ventas</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Panel de Ventas</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Historial de Ventas al <?php echo $datos['tipo']; ?></h3>  
                            </div>
                            <div class="col-sm-12">
                                <a href="<?php echo RUTA_URL; ?>/ventas/agregarcredito" class="float-sm-right btn btn-success">
                                    <i class="nav-icon fas fa-cart-arrow-down"> 
                                        <b>Realizar Venta</b>
                                    </i>
                                </a>
                            </div><!-- /.col -->                         
                        </div>

                        <div class="card-header">
                            <form action="<?php echo RUTA_URL; ?>/ventas/credito" method="post">
                                <div class="row col-sm-12">                               
                                    <div>
                                        <label for="filtro">Inicio: </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input value="<?php echo $datos['inicio']; ?>" title="Seleccione fecha de inicio" type="date" id="inicio" name="inicio" class="form-control" required="true">                                        
                                    </div>

                                    <div>
                                        <label for="filtro">Fin: </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input value="<?php echo $datos['fin']; ?>" title="Seleccione fecha de fin" type="date" id="fin" name="fin" class="form-control" required="true">                                        
                                    </div>

                                    <div>
                                        <button class="form-control btn btn-info" id="mostrarfilt">
                                            <i class="nav-icon fas fa-search"> 
                                                <b>Mostrar</b>
                                            </i>
                                        </button>
                                    </div>                                                             
                                </div>  
                            </form>
                        </div>
                        
                        <!-- /.card-header table-responsive -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th># Factura</th>
                                        <th>Vendedor</th>
                                        <th>Cliente</th>
                                        <!-- <th>Tipo</th> -->
                                        <!-- <th>Sucursal</th> -->
                                        <th>Total</th>
                                        <th>Saldo</th>
                                        <th>Abonado</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['ventas'] as $venta) {
                                            if ($venta->estadov == 3) {
                                                $estado = '<span class="span label-success"><b>CANCELADA</b></span>';
                                            }else if ($venta->estadov == 2){
                                                $estado = '<span class="span label-warning"><b>ADEUDO</b></span>';
                                            }else if ($venta->estadov == 1){
                                                $estado = '<span class="span label-warning"><b>EN PROCESO</b></span>';
                                            }
                                            $deudor = ($venta->totv - $venta->abono);
                                        ?>
                                        <tr inde="<?php echo $venta->idventa; ?>">
                                            <td><?php echo $venta->nfact; ?> </td>
                                            <td><?php echo $venta->nomemp.' '.$venta->apellemp; ?> </td>
                                            <td><?php echo $venta->nomclie.' '.$venta->apellclie; ?> </td>
                                            <!-- <td><?php //echo $venta->tipov; ?> </td> -->
                                            <!-- <td><?php //echo $venta->sucursal; ?> </td> -->
                                            <td><b>$ <?php echo $venta->totv; ?> </b></td>
                                            <td><b>$ <?php echo number_format((Double)$deudor,2, '.', ''); ?> </b></td>
                                            <td><b>$ <?php echo $venta->abono; ?> </b></td>
                                            <td><?php echo $estado; ?> </td>
                                            <td><b><?php echo $venta->fecha; ?> </b></td>
                                            <td>
                                                <?php if ($venta->estadov == 3 || $venta->estadov == 2) {?>
                                                    <a href="<?php echo RUTA_URL; ?>/ventas/verdetalle/<?php echo $venta->idventa; ?>" title="Ver detalle" class="btn btn-primary"><i class='nav-icon fas fa-file-alt'></i></a>
                                                    <button title="Abonar" onclick="total(<?php echo $venta->totv; ?>)" class="btn btn-warning ab" data-toggle="modal" data-target="#abon"><i class='nav-icon fas fa-money-check-alt'></i></button>
                                                    <button title="Contrato" class="btn btn-success contrato"><i class='nav-icon fas fa-file-alt'></i></button>
                                                    <a href="<?php echo RUTA_URL; ?>/ventas/verdetallecontrato/<?php echo $venta->idventa; ?>" title="Ver detalle" class="btn btn-success"><i class='nav-icon fas fa-file-alt'></i></a>
                                                <?php }else{?>
                                                    <a href="<?php echo RUTA_URL; ?>/ventas/verdetalle/<?php echo $venta->idventa; ?>" title="Ver detalle" class="btn btn-success"><i class='nav-icon fas fa-file-alt'></i></a>
                                                    <button title="Editar Venta" class="btn btn-primary abc"><i class='nav-icon fas fa-edit'></i></button>
                                                    <a href="<?php echo RUTA_URL; ?>/ventas/editardetallecredito/<?php echo $venta->idventa; ?>" title="editar detalle" class="btn btn-primary"><i class='nav-icon fas fa-pen-square'></i></a>
                                                    <a href="<?php echo RUTA_URL; ?>/ventas/eliminarventacredito/<?php echo $venta->idventa; ?>" title="Eliminar Venta" class="btn btn-danger"><i class='nav-icon fas fa-trash-alt'></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <!-- modal para editar maestro  -->
        <div class="modal fade" id="editmaster">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Factura de venta</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="ventas-editar">
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <div class="form-group row">
                                <input type="hidden" id="idcomedit">
                                <div class="col-sm-3">                                
                                    <label for="nfact" class="control-label" id="lblnfact">No. Factura</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nfact" name="nfact" required="true" placeholder="No. Factura">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="vende" class="control-label" id="lblvende">Vendedor</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="vende" id="vende" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($datos['vendedores'] as $vende) { ?>
                                            <option value="<?php echo $vende->code; ?>">
                                                <?php echo $vende->nombre.' '.$vende->apellido; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="fecha" class="control-label" id="lblfecha">Fecha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="fecha" name="fecha" required="true" placeholder="Fecha">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="clie" class="control-label" id="lblclie">Cliente</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="clie" id="clie" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($datos['clientes'] as $cliente) { ?>
                                            <option value="<?php echo $cliente->code; ?>">
                                                <?php echo $cliente->nombre.' '.$cliente->apellido; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="tipo" class="control-label" id="lbltipo">Tipo</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="tipoven" id="tipoven" class="form-control">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="estad" class="control-label" id="lblestad">Estado</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control" id="estad" name="estad" required="true">
                                        <option value=""></option>
                                        <option value="1">EN PROCESO</option>
                                        <option value="2">ADEUDO</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="btnactventa">Actualizar Venta</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <!-- modal para agregar  abono-->
        <div class="modal fade" id="abon">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Abono de Cliente</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="compras-abono">
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6 ">
                                    <label id="lblcuotas"></label>
                                </div>
                                <div class="col-6 ">
                                    <label id="lbltotalcom">Total Venta: $ </label>
                                    <label id="lbltotalc"></label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6 ">
                                    <label id="lblabonado"></label>
                                </div>
                                <div class="col-6 ">
                                    <label id="lbldeudor"></label>
                                </div>
                            </div><hr>
                            <div class="form-group row">
                                <div class="col-sm-2">                                
                                    <label for="fechabono" class="control-label" id="lblfechabono">Fecha</label>
                                </div>
                                <div class="col-sm-8">
                                    <input title="Fecha de abono" type="date" class="form-control" id="fechabono" name="fechabono" step="any" required="true" placeholder="Fecha de abono" value="<?php echo date('Y-m-d'); ?>">
                                </div>

                                <!-- <div class="col-sm-2">                                
                                    <label for="fechabono" class="control-label" id="lblfechabono">Cobrador</label>
                                </div>
                                <div class="col-sm-4">
                                    <select title="Vendedor" name="emple" id="emple" class="form-control">
                                        <option value="">Seleccionar Vendedor</option>
                                        <?php// foreach ($datos['vendedores'] as $emple) { ?>
                                            <option value="<?php echo $emple->code; ?>">
                                                <?php// echo $emple->nombre.' '.$emple->apellido; ?>
                                            </option>
                                        <?php// } ?>
                                    </select>
                                </div> -->
                            </div>
                            <div class="form-group row">
                                <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <input type="hidden" id="idcom">
                                <div class="col-sm-2">                                
                                    <label for="abono" class="control-label" id="lblabono">Abono</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="number" class="form-control" id="abono" name="abono" step="any" required="true" placeholder="Cantidad">
                                </div>
                            </div>
                            <hr>
                            <h4>Historial de Abonos</h4>
                            <table class="table table-bordered table-striped table-sm" id="t">
                                    <thead>
                                        <tr>
                                            <th>Monto</th>
                                            <th>Fecha</th>
                                        </tr>
                                    </thead>
                                    <tbody id="table-abono">

                                    </tbody>
                                </table>
                            </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="btnabonar">Realizar Abono</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/ventascredito.js"></script>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/abonoventacredito.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>