<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Control de Inventario</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Control de Inventario</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo de Producto</th>
                                        <th>Marca</th>
                                        <th>Descripcion del producto</th>
                                        <th>Categoria</th>
                                        <th>Lote</th>
                                        <th>Existencia</th>
                                        <th>Costo</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['Inventario'] as $inventario) { ?>
                                        <tr>
                                            <td><?php echo $inventario->codigo_producto; ?> </td>
                                            <td><?php echo $inventario->marca; ?></td>
                                            <td><?php echo $inventario->descripcion_producto; ?> </td>
                                            <td><?php echo $inventario->categoria; ?></td>
                                            <td><?php echo $inventario->lote_producto; ?></td>
                                            <td><?php echo $inventario->existencia_productos; ?></td>
                                            <td><?php echo $inventario->costo_productos; ?></td>
                                            <td><?php echo $inventario->total_producto; ?></td>
                                           
            
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>