<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark">Panel de Compras</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                            <li class="breadcrumb-item active">Panel de Compras</li>
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card" style="color: #000; background: #F7FAFC;">
                        <div class="card-header">
                            <div class="row mb-12">
                                <div class="col-sm-10">
                                <h3 class="card-title float-sm-left">Vista de Factura de Compra No. <b><?php echo $datos['nfac']; ?></b></h3>
                                </div><!-- /.col -->
                                <div class="col-sm-2">
                                        <?php 
                                            if($datos['tipoc']===conta){
                                                $paneltipo = '/compras/';
                                            }else{
                                                $paneltipo = '/compras/credito';
                                            }
                                        ?>
                                        <a href="<?php echo RUTA_URL . $paneltipo; ?>" class="nav-link">
                                            <i class="nav-icon fas fa-sign-out-alt"></i> Volver
                                        </a>
                                </div><!-- /.col -->
                            </div><!-- /.row -->
                        </div>
                        <div class="card-header row">
                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>No. Factura: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label><?php echo $datos['nfac']; ?></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Prefijo: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label><label><?php echo $datos['pref']; ?></label></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Fecha: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label><?php echo $datos['fecha']; ?></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-4">
                                        <label>Proveedor: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label><?php echo $datos['prov']; ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Estado de Compra: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <?php
                                            if ($datos['estado'] == 1) {
                                                echo '<span class="span label-warning"><b>EN PROCESO</b></span>';
                                            }else if ($datos['estado'] == 2){
                                                echo '<span class="span label-warning"><b>ADEUDO</b></span>';
                                            }else if ($datos['estado'] == 3){
                                                echo '<span class="span label-success"><b>CANCELADA</b></span>';
                                            }
                                        ?>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Total de IVA: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label>$ <?php echo $datos['totiva']; ?></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Total Sin IVA: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label>$ <?php echo $datos['totsiniva']; ?></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Total de Compra: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label>$ <?php echo $datos['totalc']; ?></label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-4">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Tipo de Compra: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label><?php echo $datos['tipoc']; ?></label>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-6">
                                        <label>Sucursal: </label>                                    
                                    </div>
                                    <div class="col-sm-6">
                                        <label><?php echo $datos['sucursal']; ?></label>
                                    </div>
                                </div>
                                <?php if($datos['tipoc']==credi){?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Saldo Deudor: </label>                                    
                                        </div>
                                        <div class="col-sm-6">
                                            <label>$ <?php echo $datos['deuda']; ?></label>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-6">
                                            <label>Saldo Abonado: </label>                                    
                                        </div>
                                        <div class="col-sm-6">
                                            <label>$ <?php echo $datos['abono']; ?></label>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>                            
                        </div>

                        <div class="card-header" style="color: #000; background: #FFF;">
                            <div class="col-sm-6"><br>
                                <h3 class="card-title float-sm-left"><b>Detalle de Compra </b></h3>  
                            </div>

                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Codigo</th>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <th>Total</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['detalle'] as $detalle) { ?>
                                        <tr>
                                            <td><?php echo $detalle->codpd; ?> </td>
                                            <td><?php echo $detalle->producto; ?> </td>
                                            <td><?php echo $detalle->cantidad; ?> </td>
                                            <td><?php echo $detalle->precio; ?> </td>
                                            <td><?php echo $detalle->total; ?> </td>
                                            <td>Sin Acciones</td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <!-- <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script> -->
    </body>
</html>