<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php //echo RUTA_URL;?>/css/select2.css"> -->
        <!-- <script src="<?php //echo RUTA_URL;?>/js/select2.js"></script> -->
        <!-- <script>
            $(document).ready(function() {
                 $("#provee").select2(); 
                });
        </script> -->
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Factura de Compras</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Factura de Compras</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">

                            <form id="compras-form"> 
                                <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="numf" name="numf" placeholder="# Factura" required="true">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" class="form-control" id="pref" name="pref" placeholder="Prefijo">
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="date" class="form-control" id="fecha" name="fecha" placeholder="Fecha" value="<?php echo date("Y-m-d"); ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <!-- <input type="text" name="prove" list="prove" placeholder="Seleccionar Proveedor" class="form-control"/> -->
                                        <!-- <datalist id="prove"> -->
                                                <select name="provee" id="provee" class="form-control">
                                                    <option value="">Seleccionar Proveedor</option>
                                                    <?php foreach ($datos['proveedores'] as $prov) { ?>
                                                        <option value="<?php echo $prov->code; ?>">
                                                            <?php echo $prov->nombre; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                        <!-- </datalist> -->
                                    </div>
                                    <div class="col-sm-3">
                                        <select name="tipoco" id="tipoco" class="form-control">
                                            <?php foreach ($datos['tipocomp'] as $tpc) { ?>
                                                <option value="<?php echo $tpc->id_tipo_compra; ?>">
                                                    <?php echo $tpc->descripcion; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <input readonly required type="text" class="form-control" id="estad" name="estad" placeholder="Estado Compra" value="EN PROCESO">
                                    </div>

                                    <div class="col-sm-3">
                                        <button type="button" class="btn btn-success" id="btndetalle">
                                            <i class="nav-icon fas fa-forward">
                                                <b>Siguiente</b>
                                            </i>
                                        </button>
                                    </div>
                                </div>
                            <!-- </form>    -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" id="detalle">
                            <!-- <form id="form-detalle"> -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                        <select name="prod" id="prod" class="form-control">
                                            <option value="">Seleccionar Producto</option>
                                            <?php foreach ($datos['productos'] as $producto) { ?>
                                                <option value="<?php echo $producto->code; ?>">
                                                    <?php echo $producto->producto; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="cantidad" placeholder="Cantidad" required>
                                </div>

                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="costunit" placeholder="Precio Unitario" required>
                                </div>

                                <div class="col-sm-3">
                                        <button type="button" class="btn btn-success swalDefaultError" id="mas">
                                            <i class="nav-icon fas fa-plus">
                                                <b></b>
                                            </i>
                                        </button>
                                    </div>
                            </div>
                            <table class="table table-bordered table-striped table-sm" id="t">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <!-- <th>IVA</th> -->
                                        <th>Total</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="table-detalle">

                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="text-align: right;" colspan="4"><b>TOTAL</b></td>
                                        <td id="total"></td>
                                        <td></td>
                                    </tr>
                                <tfoot>
                            </table>
                           
                            <div class="col-sm-7">
                                <button type="submit" class="btn btn-success" >
                                    <i class="nav-icon fas fa-cart-plus">
                                        <b>Registrar Compra</b>
                                    </i>
                                </button>
                                <a href="<?php echo RUTA_URL; ?>/compras/" class="btn btn-danger" >
                                    <i class="nav-icon fas fa-window-close">
                                        <b>Cancelar Compra</b>
                                    </i>
                                </a>
                            </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/compras.js"></script>
        <script src="<?php echo RUTA_URL;?>/js/jquery.validate.js"></script>

        <!-- <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" -->
  <!-- crossorigin="anonymous"></script> -->
    </body>
</html>