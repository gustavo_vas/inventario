<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
        <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
        <!-- <link rel="stylesheet" type="text/css" href="<?php //echo RUTA_URL;?>/css/select2.css"> -->
        <!-- <script src="<?php //echo RUTA_URL;?>/js/select2.js"></script> -->
        <!-- <script>
            $(document).ready(function() {
                 $("#provee").select2(); 
                });
        </script> -->
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Editar Factura de Compras</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Editar Factura de Compras</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">

                            <form id="editcompras-form"> 
                                <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                                <div class="form-group row">
                                    <input type="hidden" id="idcomp" name="idcomp" value="<?php echo $datos['id']; ?>">
                                    <div class="col-sm-3">
                                        <input type="text" disabled="true" class="form-control" placeholder="# Factura" required="true" value="<?php echo $datos['nfac']; ?>">
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="text" disabled="true" class="form-control" placeholder="Prefijo" value="<?php echo $datos['pref']; ?>">
                                    </div>

                                    <div class="col-sm-3">
                                        <input type="date" disabled="true" class="form-control" placeholder="Fecha" value="<?php echo $datos['fecha']; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <div class="col-sm-3">
                                        <!-- <input type="text" name="prove" list="prove" placeholder="Seleccionar Proveedor" class="form-control"/> -->
                                        <!-- <datalist id="prove"> -->
                                                <select disabled="true" class="form-control">
                                                    <option value="<?php echo $datos['idprov']; ?>"><?php echo $datos['prov']; ?></option>
                                                </select>
                                        <!-- </datalist> -->
                                    </div>
                                    <div class="col-sm-3">
                                        <select disabled="true" class="form-control" id="tp">
                                            <?php foreach ($datos['tipocomp'] as $tpc) { ?>
                                                <option value="<?php echo $tpc->id_tipo_compra; ?>">
                                                    <?php echo $tpc->descripcion; ?>
                                                </option>
                                                <?php if($tpc->descripcion == credi){$tipo='credito';}else{$tipo='index';} ?>
                                            <?php } ?>
                                        </select>
                                    </div>

                                    <div class="col-sm-3">
                                        <input readonly required type="text" class="form-control" id="estad" name="estad" placeholder="Estado Compra" value="EN PROCESO">
                                    </div>
                                </div>
                            <!-- </form>    -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body" id="editdetalle">
                            <!-- <form id="form-detalle"> -->
                            <div class="form-group row">
                                <div class="col-sm-3">
                                        <select name="eprod" id="eprod" class="form-control">
                                            <option value="">Seleccionar Producto</option>
                                            <?php foreach ($datos['productos'] as $producto) { ?>
                                                <option value="<?php echo $producto->code; ?>">
                                                    <?php echo $producto->producto; ?>
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="ecantidad" placeholder="Cantidad" required>
                                </div>

                                <div class="col-sm-3">
                                    <input type="number" class="form-control" id="ecostunit" placeholder="Precio Unitario" required>
                                </div>

                                <div class="col-sm-3">
                                        <button type="button" class="btn btn-success swalDefaultError" id="emas">
                                            <i class="nav-icon fas fa-plus">
                                                <b></b>
                                            </i>
                                        </button>
                                    </div>
                            </div>
                            <table class="table table-bordered table-striped table-sm" id="t">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Producto</th>
                                        <th>Cantidad</th>
                                        <th>Precio Unitario</th>
                                        <!-- <th>IVA</th> -->
                                        <th>Total</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody id="table-detalle">
                                    <?php $total = 0;
                                     foreach ($datos['detalle'] as $detalle) {
                                        $total += $detalle->total; ?>
                                        <tr ides="<?php echo $detalle->iddetalle; ?>">
                                            <td><?php echo $detalle->codpd; ?> </td>
                                            <td><?php echo $detalle->producto; ?> </td>
                                            <td><?php echo $detalle->cantidad; ?> </td>
                                            <td><?php echo $detalle->precio; ?> </td>
                                            <td><?php echo $detalle->total; ?> </td>
                                            <td><button type="button" class="btn btn-danger deldet"><i class="nav-icon fas fa-trash-alt"></i></button></td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td style="text-align: right;" colspan="4"><b>TOTAL</b></td>
                                        <td id="etotal"><?php echo $total; ?></td>
                                        <td></td>
                                    </tr>
                                <tfoot>
                            </table>
                           
                            <div class="col-sm-7">
                                <a href="<?php echo RUTA_URL; ?>/compras/<?php echo $tipo; ?>" class="btn btn-success" >
                                    <i class="nav-icon fas fa-cart-plus">
                                        <b>Actualizar Compra</b>
                                    </i>
                                </a>
                                <a href="<?php echo RUTA_URL; ?>/compras/<?php echo $tipo; ?>" class="btn btn-danger" >
                                    <i class="nav-icon fas fa-window-close">
                                        <b>Cancelar Compra</b>
                                    </i>
                                </a>
                            </div>
                            </form>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/comprascredito.js"></script>
        <script src="<?php echo RUTA_URL;?>/js/jquery.validate.js"></script>
    </body>
</html>