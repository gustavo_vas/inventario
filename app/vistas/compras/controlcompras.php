<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Panel de Compras</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Panel de Compras</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Historial de Compras al <?php echo $datos['tipo']; ?></h3>  
                            </div>
                            <div class="col-sm-12">
                                <a href="<?php echo RUTA_URL; ?>/compras/agregar" class="float-sm-right btn btn-success">
                                    <i class="nav-icon fas fa-cart-plus"> 
                                        <b>Realizar Compra</b>
                                    </i>
                                </a>
                            </div><!-- /.col -->                         
                        </div><div class="card-header">
                        <form action="<?php echo RUTA_URL; ?>/compras/" method="post">
                            <div class="row col-sm-12">
                               
                                    <div>
                                        <label for="filtro">Inicio: </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input value="<?php echo $datos['inicio']; ?>" title="Seleccione fecha de inicio" type="date" id="inicio" name="inicio" class="form-control" required="true">
                                        <!-- <select title="Filtrar por" id="filtro" class="form-control">
                                            <option>Seleccione Filtro</option>
                                            <option value="1" class="form-control">No. Factura</option>
                                            <option value="2" class="form-control">Rango de Fecha</option>
                                            <option value="3" class="form-control">Estado</option>
                                            <option value="4" class="form-control">Sucursal</option>
                                        </select> -->
                                    </div>

                                    <div>
                                        <label for="filtro">Fin: </label>
                                    </div>
                                    <div class="col-sm-3">
                                        <input value="<?php echo $datos['fin']; ?>" title="Seleccione fecha de fin" type="date" id="fin" name="fin" class="form-control" required="true">
                                        <!-- <select title="Filtrar por" id="filtro" class="form-control">
                                            <option>Seleccione Filtro</option>
                                            <option value="1" class="form-control">No. Factura</option>
                                            <option value="2" class="form-control">Rango de Fecha</option>
                                            <option value="3" class="form-control">Estado</option>
                                            <option value="4" class="form-control">Sucursal</option>
                                        </select> -->
                                    </div>
                                    <!-- <div id="controlesfiltro">
                                        
                                    </div>
                                    <div id="controlesfiltro2">
                                        
                                    </div> -->

                                    <!-- <div id="controlesfiltro3">
                                        <select title="Seleccionar Sucursal" id="Suc" class="form-control">
                                            <?php// foreach ($datos['sucursal'] as $sucur) { ?>
                                                <option value="<?php// echo $sucur->id_sucursal; ?>"><?php //echo $sucur->nombre; ?></option>
                                        <?php //} ?>
                                        </select>
                                    </div> -->
                                    <div>
                                        <button class="form-control btn btn-info" id="mostrarfilt">
                                            <i class="nav-icon fas fa-search"> 
                                                <b>Mostrar</b>
                                            </i>
                                        </button>
                                    </div> 
                                                          
                            </div>  
                            </form>                          
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th># Factura</th>
                                        <!-- <th>Usuario</th> -->
                                        <th>Proveedor</th>
                                        <th>Tipo</th>
                                        <th>Sucursal</th>
                                        <th>Total</th>
                                        <th>Estado</th>
                                        <th>Fecha</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['compras'] as $compra) {
                                            if ($compra->estado == 3) {
                                                $estado = '<span class="span label-success"><b>CANCELADA</b></span>';
                                            }else{
                                                $estado = '<span class="span label-warning"><b>EN PROCESO</b></span>';
                                            }
                                        ?>
                                        <tr inde="<?php echo $compra->idcompra; ?>">
                                            <td><?php echo $compra->nfact; ?> </td>
                                            <td><?php echo $compra->proveedor; ?> </td>
                                            <td><?php echo $compra->tipoc; ?> </td>
                                            <td><?php echo $compra->sucursal; ?> </td>
                                            <td><b>$ <?php echo $compra->totalc; ?> </b></td>
                                            <td><?php echo $estado; ?> </td>
                                            <td><b><?php echo $compra->fecha; ?> </b></td>
                                            <td>
                                                <?php if ($compra->estado == 3) {?>
                                                    <a href="<?php echo RUTA_URL; ?>/compras/verdetalle/<?php echo $compra->idcompra; ?>" title="Ver detalle" class="btn btn-primary"><i class='nav-icon fas fa-file-alt'></i></a>
                                                <?php }else{?>
                                                    <a href="<?php echo RUTA_URL; ?>/compras/verdetalle/<?php echo $compra->idcompra; ?>" title="Ver detalle" class="btn btn-success"><i class='nav-icon fas fa-file-alt'></i></a>
                                                    <button title="Editar Compra" class="btn btn-primary abc"><i class='nav-icon fas fa-edit'></i></button>
                                                    <a href="<?php echo RUTA_URL; ?>/compras/editardetallecontado/<?php echo $compra->idcompra; ?>" title="editar detalle" class="btn btn-primary"><i class='nav-icon fas fa-pen-square'></i></a>
                                                    <a href="<?php echo RUTA_URL; ?>/compras/eliminarcompra/<?php echo $compra->idcompra; ?>" title="Eliminar Compra" class="btn btn-danger"><i class='nav-icon fas fa-trash-alt'></i></a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>    
        
        <!-- modal para editar maestro  -->
        <div class="modal fade" id="editmaster">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="encab">Factura de Compra</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form id="compras-editar">
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">
                            <div class="form-group row">
                                <input type="hidden" id="idcomedit">
                                <div class="col-sm-3">                                
                                    <label for="nfact" class="control-label" id="lblnfact">No. Factura</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="nfact" name="nfact" required="true" placeholder="No. Factura">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="prefijo" class="control-label" id="lblpref">Prefijo</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control" id="prefijo" name="prefijo" placeholder="Prefijo">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="fecha" class="control-label" id="lblfecha">Fecha</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="date" class="form-control" id="fecha" name="fecha" required="true" placeholder="Fecha">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="prov" class="control-label" id="lblprov">Proveedor</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="provee" id="provee" class="form-control">
                                        <option value=""></option>
                                        <?php foreach ($datos['proveedores'] as $prov) { ?>
                                            <option value="<?php echo $prov->code; ?>">
                                                <?php echo $prov->nombre; ?>
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="tipo" class="control-label" id="lbltipo">Tipo</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="tipocom" id="tipocom" class="form-control">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-3">                                
                                    <label for="estad" class="control-label" id="lblestad">Estado</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control" id="estad" name="estad" required="true">
                                        <option value=""></option>
                                        <option value="1">EN PROCESO</option>
                                        <option value="3">CANCELADA</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            </div>
                        <div class="modal-footer justify-content-between">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary" id="btnactcompra">Actualizar Compra</button>
                        </div>
                    </form>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>

        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/compras.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>