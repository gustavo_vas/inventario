<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <link rel="stylesheet" href="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.css">
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Control de Clientes</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Control de Clientes</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <div class="col-sm-6">
                                <h3 class="card-title float-sm-left">Datos de los Clientes</h3>  
                            </div>
                            <div class="col-sm-12">
                                <a href="<?php echo RUTA_URL; ?>/Clientes/agregar" class="float-sm-right btn btn-success">
                                    <i class="nav-icon fas fa-user-plus"> 
                                        <b>Agregar Clientes</b>
                                    </i>
                                </a>
                            </div><!-- /.col -->                         
                        </div>
                        
                        <!-- /.card-header -->
                        <div class="card-body">
                            <table id="example1" class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Apellido</th>
                                        <th>DUI</th>
                                        <!-- <th>NIT</th> -->
                                        <th>Teléfono</th>
                                        <th>Celular</th>
                                        <th>Dirección</th>
                                        <th>Sucursal</th>
                                        <!-- <th>Departamento</th> -->
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($datos['Clientes'] as $clientes) { ?>
                                        <tr inde="<?php echo $clientes->code; ?>">
                                            <td><?php echo $clientes->nombre; ?> </td>
                                            <td><?php echo $clientes->apellido; ?></td>
                                            <td><?php echo $clientes->dui; ?> </td>
                                            <!-- <td><?php //echo $clientes->nit; ?></td> -->
                                            <td><?php echo $clientes->tel; ?></td>
                                            <td><?php echo $clientes->cel; ?></td>
                                            <td><?php echo $clientes->direccion; ?></td>
                                            <td><?php echo $clientes->sucursal; ?></td>
                                            <!-- <td><?php //echo $clientes->departamento; ?></td> -->
                                            <td>
                                                <a href="<?php echo RUTA_URL; ?>/Clientes/editar/<?php echo $clientes->code; ?>" title="Editar Cliente" class="btn btn-primary"><i class='nav-icon fas fa-edit'></i></a> 
                                                <button title="Eliminar Cliente" class="btn btn-danger delclie"><i class='nav-icon fas fa-trash-alt'></i></button>
                                                <!-- <a href="<?php //echo RUTA_URL; ?>/empleados/desactivar/<?php //echo $empleado->code; ?>" title="Desactivar Empleado" class="btn btn-danger"><i class='nav-icon fas fa-share-square'></i></a> -->
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>                            
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>
            </div>
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div> 

        <!-- modal para eliminar cliente  -->
        <div class="modal fade" id="elimclie">
            <div class="modal-dialog modal-md">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 id="encab">Formulario de confirmación</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <input type="hidden" id="e">
                        <div class="modal-body justify-center">
                            <h3>¿Desea eliminar este registro?</h3>                           
                        </div>
                        <div class="modal-footer justify-content-between">
                            <button class="btn btn-danger" data-dismiss="modal">CERRAR</button>
                            <button class="btn btn-primary" id="delcliente">Aceptar</button>
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>   

        <!-- modal para editar maestro  -->
        <div class="modal fade" id="okis">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 id="encab">Registro Eliminado</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                        <input type="hidden" id="ruta" value="<?php echo RUTA_URL;?>" readonly>
                        <div class="modal-body">  
                            <button class="btn btn-success form-control" id="ok">Aceptar</button>                        
                        </div>
                        <div class="modal-footer justify-content-between">
                        </div>
                </div>
            <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?>
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/delete.js"></script>
        <script src="<?php echo RUTA_URL;?>/plugins/toastr/toastr.min.js"></script>
        <script>
        $(function () {
            $("#example1").DataTable();
            $('#example2').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": false,
            "ordering": true,
            "info": true,
            "autoWidth": false,
            });
        });
        </script>
    </body>
</html>