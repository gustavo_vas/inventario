<?php
    $this->protegerPagina();
?>
<!DOCTYPE html>
<html>
    <head>
        <?php require_once RUTA_APP."/vistas/include/header.php"; ?>
        <!-- <link rel="stylesheet" type ="text/css" href= " /inventarionohelia/public/css/select2.css"> -->
        <script src = " /inventarionohelia/public/js/jquery.min.js"></script>
        <!-- <script src= " /inventarionohelia/public/js/select2.js"></script> -->
        <script type="text/javascript" src="<?php echo RUTA_URL;?>/js/validcampos.js"></script> 
    </head>

    <body class="hold-transition sidebar-mini layout-fixed">        
        <?php require_once RUTA_APP."/vistas/include/navadmin.php"; ?>
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Nuevo Cliente</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="<?php echo RUTA_URL;?>/admins1/inicio">Inicio</a></li>
                        <li class="breadcrumb-item active">Nuevo cliente</li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <!-- <div class="row"> -->
                <!-- <div class="col-12"> -->
                    <!-- <div class="card"> -->                        
                        <!-- /.card-header -->
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="card card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title">Datos del cliente</h3>
                                    </div>
                                        <form role="form" action="<?php echo RUTA_URL; ?>/Clientes/agregar" method="POST">
                                            <div class="card-body">

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="nombre" class="control-label">Nombres</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input onkeypress="return soloLetras(event)" type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombres" required="true">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="apellido" class="control-label">Tel. Fijo</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input onkeypress="return solonumeros(event)" title="0000-0000" type="tel" class="form-control" id="tel" name="tel" placeholder="0000-0000" required="true">
                                                    </div> 
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="apellido" class="control-label">Apellidos</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input onkeypress="return soloLetras(event)" type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellidos" required="true">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="isss" class="control-label">Tel. Móvil</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input onkeypress="return solonumeros(event)" title="0000-0000" type="tel" class="form-control" id="cel" name="cel" placeholder="0000-0000">
                                                    </div> 
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="direccion" class="control-label">Dirección</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input type="text" class="form-control" id="direccion" name="direccion" placeholder="Dirección">
                                                    </div>                                                    

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="sucursal" class="control-label">Depto.</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="departamento" name="departamento">
                                                            <?php foreach ($datos['departamento'] as $departamentos) { ?>
                                                                <option value="<?php echo $departamentos->id_departamento; ?>">
                                                                    <?php echo $departamentos->nombre; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div>                                                                                       
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="nombre" class="control-label">DUI</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input onkeypress="return solonumeros(event)" title="FORMATO: 00000000-0" type="text" class="form-control" id="dui" name="dui" placeholder="00000000-0" required="true">
                                                    </div>

                                                    <div class="col-sm-1"></div>

                                                    <div class="col-sm-1">
                                                        <label for="sucursal" class="control-label">Sucursal</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <select class="form-control" id="sucursal" name="sucursal">
                                                            <?php foreach ($datos['sucursal'] as $sucursales) { ?>
                                                                <option value="<?php echo $sucursales->id_sucursal; ?>">
                                                                    <?php echo $sucursales->nombre; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                    </div> 
                                                </div>

                                                <div class="form-group row">
                                                    <div class="col-sm-1">
                                                        <label for="afp" class="control-label">NIT</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <input onkeypress="return solonumeros(event)" title="FORMATO: 0000-000000-000-0" type="text" class="form-control" id="nit" name="nit" placeholder="0000-000000-000-0">
                                                    </div>                                             
                                                </div>  
                                            </div>                                              
                                        <!-- /.card-body -->
                                            <div class="card-footer">
                                                <button type="submit" class="btn btn-success">
                                                    <i class="nav-icon fas fa-save">
                                                        <b>Guardar Datos</b>
                                                    </i>
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- </div> -->
                    <!-- /.card -->
                <!-- </div> -->
            <!-- </div> -->
        </section>
        <!-- /.content -->
        </div>
        </div>
        <!-- ./wrapper -->
        </div>
        <?php require_once RUTA_APP."/vistas/include/footer.php"; ?> 
        <!-- <script type="text/javascript" src="<?php //echo RUTA_URL;?>/js/validcampos.js"></script>        -->
    </body>
</html>
<script type="text/javascript">
    $(document).ready(function(){
    $('#departamento').select2();
});
</script>  