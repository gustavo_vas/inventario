<?php

    class Empleado{
        private $db;//manejador de la bd
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function obtenerEmpleados(){
            $consulta = 'SELECT (em.id_empleado)as code,(em.nombre)as nombre,(em.apellido)as apellido,(em.direccion)as direccion,
                (em.dui)as dui,(em.nit)as nit,(em.afp)as afp,(em.isss)as isss,(em.sueldo)as sueldo,
                (ca.cargo)as cargo,(em.estado)as estado,(suc.nombre)as sucursal from empleados em 
                join cargoempleado ca on em.id_cargo = ca.id_cargo 
                join sucursales suc on em.id_sucursal = suc.id_sucursal 
                order by em.nombre asc';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function agregarEmpleado($datos){
            $consulta = 'INSERT INTO empleados (nombre,apellido,direccion,dui,nit,afp,isss,sueldo,id_cargo,estado,id_sucursal) 
                    values(:nombre,:apellido,:direccion,:dui,:nit,:afp,:isss,:sueldo,:cargo,:estado,:sucursal)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombres']);
            $this->db->bind(':apellido',$datos['apellidos']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':nit',$datos['nit']);
            $this->db->bind(':afp',$datos['afp']);
            $this->db->bind(':isss',$datos['isss']);
            $this->db->bind(':sueldo',$datos['sueldo']);
            $this->db->bind(':cargo',$datos['cargo']);
            $this->db->bind(':estado',$datos['estado']);
            $this->db->bind(':sucursal',$datos['sucursal']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function obtenerEmpleadoId($id){
            $consulta = 'SELECT (em.id_empleado)as code,(em.nombre)as nombre,(em.apellido)as apellido,(em.direccion)as direccion,
                (em.dui)as dui,(em.nit)as nit,(em.afp)as afp,(em.isss)as isss,(em.sueldo)as sueldo,(em.id_cargo)as idcargo, 
                (ca.cargo)as cargo,(em.estado)as estado,(suc.nombre)as sucursal,(em.id_sucursal)as idsucursal from empleados em 
                join cargoempleado ca on em.id_cargo = ca.id_cargo 
                join sucursales suc on em.id_sucursal = suc.id_sucursal 
                where em.id_empleado = :id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);

            $fila = $this->db->registro();
            return $fila;
        }

        public function actualizarEmpleado($datos){
            $consulta = 'UPDATE empleados set nombre = :nombre, apellido = :apellido, direccion = :direccion, dui = :dui, 
                    nit = :nit, afp = :afp, isss = :isss, sueldo = :sueldo, id_cargo = :cargo, estado = :estado, id_sucursal = :sucursal 
                    where id_empleado = :id';

            $this->db->query($consulta);
            //vincular los valores
            $this->db->bind(':nombre',$datos['nombres']);
            $this->db->bind(':apellido',$datos['apellidos']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':nit',$datos['nit']);
            $this->db->bind(':afp',$datos['afp']);
            $this->db->bind(':isss',$datos['isss']);
            $this->db->bind(':sueldo',$datos['sueldo']);
            $this->db->bind(':cargo',$datos['cargo']);
            $this->db->bind(':estado',$datos['estado']);
            $this->db->bind(':sucursal',$datos['sucursal']);
            $this->db->bind(':id',$datos['codigo']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }