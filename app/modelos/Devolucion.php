<?php 
    class Devolucion{
        private $db;
        private $consulta;
       

         public function __construct(){
            $this->db = new Base;
         }
      
        //metodo para consultar las ventas realizadas a un usuario
        public function obtenerVentasDUI(){
            $consulta = 'SELECT (ve.id_venta) as IdVenta, (ve.num_fact) as Nfactura,(ve.fecha_venta) as f_venta, (cl.nombres)as n_clientes, 
            (cl.apellidos)as n_apellidos,(ve.total_venta)as totalvent,(cl.dui)as dui,(tv.descripcion)as tipoventa,
            (ve.estado_venta)as estado 
            from ventas ve
            join clientes cl on ve.id_cliente = cl.id_cliente
            join tipo_venta tv on ve.id_tipo_venta = tv.id_tipo_venta 
            where ve.estado_venta in (2,3)';

            $this->db->query($consulta);
           
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function obtenerDetalleId($id){
            $consulta = 'SELECT (p.descripcion) as descripcion_producto, (dv.cantidad) as cantidad_producto, 
            (dv.costo_unitario) as costo_producto, (dv.total_detalle) as total,(dv.id_detalle_ventas)as iddetvent 
            from detalle_ventas dv 
            join productos p on dv.id_producto = p.id_producto where id_venta =  :id';

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para obtener el detalle de linea de venta
        public function getlineadetventa($id){
            $consulta = 'SELECT * FROM detalle_ventas WHERE id_detalle_ventas = :id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);

            $resultado = $this->db->registro();
            return $resultado;
        }

        //metodo para obtener una venta
        public function getventa($idv){
            $consulta = 'SELECT total_venta FROM ventas where id_venta=:idv';
            $this->db->query($consulta);
            $this->db->bind(':idv',$idv);

            $resultadov = $this->db->registro();
            return $resultadov;
        }

        //metodo para devolvcer un articulo
        public function devolucion($datos){
            $dev = 'CALL devolucion(:newcant, :iva, :totdet, :cantdev, :iddetvent, :totventa, :totiva, :totsiniva,
                            :pfecha, :idsuc, :iduser, :idventa, :cantdev2, :idprod, :costunit, :totdetv)';
            $this->db->query($dev);

            $this->db->bind(':newcant',$datos['newcant']);
            $this->db->bind(':iva',$datos['iva']);
            $this->db->bind(':totdet',$datos['total']);
            $this->db->bind(':cantdev',$datos['cantdv']);
            $this->db->bind(':iddetvent',$datos['id']);
            $this->db->bind(':totventa',$datos['totalv']);
            $this->db->bind(':totiva',$datos['ivanewvent']);
            $this->db->bind(':totsiniva',$datos['newsiniva']);
            $this->db->bind(':pfecha',$datos['fecha']);
            $this->db->bind(':idsuc',$datos['idsuc']);
            $this->db->bind(':iduser',$datos['userid']);
            $this->db->bind(':idventa',$datos['idv']);
            $this->db->bind(':cantdev2',$datos['cantdv2']);
            $this->db->bind(':idprod',$datos['idprod']);
            $this->db->bind(':costunit',$datos['pu']);
            $this->db->bind(':totdetv',$datos['totdev']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
            
        }

    
    }