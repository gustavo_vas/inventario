<?php 

    class Cliente {
        private $db;//manejador de la bd
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function obtenerClientes(){
            $consulta = "SELECT (cl.nombres) as nombre,(cl.apellidos) as apellido,(cl.id_cliente) as code, (cl.dui) as dui, (cl.nit) as nit, (cl.tel) as tel, 
            (cl.cel) as cel,(cl.direccion) as direccion,(dep.nombre) as departamento, (suc.nombre)as sucursal 
            from clientes cl join departamentos dep on dep.id_departamento = cl.id_departamento 
            join sucursales suc on cl.id_sucursal = suc.id_sucursal
            where cl.eliminado = 'N'";

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function agregarCliente($datos){
            $consulta = 'INSERT INTO clientes (nombres,apellidos,dui,nit,tel,cel,direccion,id_departamento,id_sucursal) 
                    values(:nombre,:apellido,:dui,:nit,:tel,:cel,:direccion,:departamento,:sucursal)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':apellido',$datos['apellido']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':nit',$datos['nit']);
            $this->db->bind(':tel',$datos['tel']);
            $this->db->bind(':cel',$datos['cel']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':departamento',$datos['departamento']);
            $this->db->bind(':sucursal',$datos['sucursal']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function obtenerClienteID($id){
            $consulta = 'SELECT (cl.nombres) as nombre,(cl.apellidos) as apellido,(cl.id_cliente) as code, (cl.dui) as dui, (cl.nit) as nit, (cl.tel) as tel, 
            (cl.cel) as cel,(cl.direccion) as direccion,(dep.id_departamento) as id_dep,(dep.nombre) as departamento, 
            (suc.id_sucursal)as id_suc, (suc.nombre)as sucursal from clientes cl 
            join departamentos dep on dep.id_departamento = cl.id_departamento 
            join sucursales suc on cl.id_sucursal = suc.id_sucursal 
            where cl.id_cliente = :id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);

            $fila = $this->db->registro();
            return $fila;
        }

        public function actualizarCliente($datos){
            $consulta = 'UPDATE clientes set   nombres = :nombre,  apellidos = :apellido, dui = :dui, nit = :nit, tel = :tel, 
                    cel = :cel, direccion = :direccion, id_departamento = :departamento, id_sucursal = :sucursal 
                    where id_cliente = :id';

            $this->db->query($consulta);
            //vincular los valores
            $this->db->bind(':nombre',$datos['nombre']);
            $this->db->bind(':apellido',$datos['apellido']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':nit',$datos['nit']);
            $this->db->bind(':tel',$datos['tel']);
            $this->db->bind(':cel',$datos['cel']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':departamento',$datos['departamento']);
            $this->db->bind(':sucursal',$datos['sucursal']);
            $this->db->bind(':id',$datos['codigo']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function eliminarcli($id,$est){
            $consulta = 'UPDATE clientes set eliminado = :elimina 
                    where id_cliente = :id';

            $this->db->query($consulta);
            //vincular los valores
            $this->db->bind(':elimina',$est);
            $this->db->bind(':id',$id);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }