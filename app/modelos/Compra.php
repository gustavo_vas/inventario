<?php 
    class Compra{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        public function getTipoCompra($tipo){
            $consulta = 'SELECT * from tipo_compra 
            where descripcion = :descrip';
            $this->db->query($consulta);

            $this->db->bind(':descrip',$tipo);
            $resultado = $this->db->registros();

            return $resultado;
        }

        public function obtenerSumaAbono($id){
            $consulta = 'SELECT sum(monto)as monto from abonos where id_compra=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        public function obtenerabonosxcompra($id){
            $consulta = 'SELECT * from abonos where id_compra=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function obtenerCompras($datos){
            $consulta = "SELECT (cp.id_compra)as idcompra,(cp.n_factura)as nfact,(cp.prefijo)as prefijo,(DATE_FORMAT(cp.fecha,'%d/%m/%Y'))as fecha, 
            (pv.nombre)as proveedor,(cp.total_compra)as totalc,(cp.total_sin_iva)as totsiniva,(cp.total_iva)as totiva, 
            (cp.estado_compra)as estado,(tc.descripcion)as tipoc,(suc.nombre) as sucursal, (ifnull(SUM(abo.monto),0))AS abono 
            from compras cp 
            join proveedores pv on cp.id_proveedor = pv.id_proveedor  
            join tipo_compra tc on cp.id_tipo_compra = tc.id_tipo_compra 
            join sucursales suc on cp.id_sucursal = suc.id_sucursal 
            LEFT JOIN abonos abo ON cp.id_compra = abo.id_compra
            -- right join abonos ab on cp.id_compra = ab.id_compra 
            where tc.descripcion = :tipo and cp.fecha between :finit and :ffin
            group by cp.id_compra
            ORDER BY cp.fecha asc";

            $this->db->query($consulta);
            $this->db->bind(':tipo', $datos['tipo']);
            $this->db->bind(':finit', $datos['dateinit']);
            $this->db->bind(':ffin', $datos['datefin']);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function updateEstado($datos){
            $consulta = 'UPDATE compras set estado_compra=:estado where id_compra=:id';
            $this->db->query($consulta);

            $this->db->bind(':estado',$datos['estad']);
            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //metodo para insertar el abono de una compra a la bd
        public function abonarcompra($datos){
            $consulta = 'INSERT INTO abonos (id_compra, monto, fecha) values(:idcompra, :monto, :fecha)';
            $this->db->query($consulta);

            $this->db->bind(':idcompra',$datos['idcompra']);
            $this->db->bind(':monto',$datos['monto']);
            $this->db->bind(':fecha',$datos['fecha']);

            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        public function agregarCompra($datos){
            $consulta = 'INSERT INTO compras (n_factura, prefijo,fecha, id_proveedor, total_compra, total_sin_iva, total_iva, estado_compra, id_tipo_compra, id_sucursal) 
                    values(:nfac, :pref, :fecha, :idprov, :totc, :totsiniva, :totiva, :estadoc, :idtipocom, :idsuc)';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':nfac',$datos['numf']);
            $this->db->bind(':pref',$datos['pref']);
            $this->db->bind(':fecha',$datos['fecha']);
            $this->db->bind(':idprov',$datos['provee']);
            $this->db->bind(':totc',$datos['totalc']);
            $this->db->bind(':totiva',$datos['totaliva']);
            $this->db->bind(':totsiniva',$datos['totasiniva']);
            $this->db->bind(':estadoc',$datos['estadoc']);
            $this->db->bind(':idtipocom',$datos['tipoco']);
            $this->db->bind(':idsuc',$datos['suc']);

            //ejecutar
            if ($this->db->execute()) {
                $this->db->query('SELECT LAST_INSERT_ID() AS id FROM compras');
                $resultado = $this->db->registro();
                $idcompra = $resultado->id;
                return $idcompra;
            }else{
                return false;
            }
        }

        public function agregarDetalleCompra($datos){
            $consulta = 'INSERT INTO detalle_compras (id_producto, cantidad, costo_unitario, iva, total_detalle, id_compra) 
            values(:idprod, :cantidad, :costounit, :iva, :totaldetalle, :idcompra)';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':idprod',$datos['idprod']);
            $this->db->bind(':cantidad',$datos['cantidad']);
            $this->db->bind(':costounit',$datos['costounit']);
            $this->db->bind(':iva',$datos['iva']);
            $this->db->bind(':totaldetalle',$datos['totaldetlle']);
            $this->db->bind(':idcompra',$datos['idcompra']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function obtenerDetalleId($id){
            $consulta = 'SELECT (dc.id_detalle_compras)as iddetalle,(pd.codigo)as codpd,(pd.descripcion)as producto,(dc.cantidad)as cantidad,(dc.costo_unitario)as precio,(dc.total_detalle)as total 
            from detalle_compras dc
            join productos pd on dc.id_producto = pd.id_producto 
            where id_compra = :id';
            $this->db->query($consulta);

            $this->db->bind(':id', $id);

            $resultado = $this->db->registros();
            return $resultado;
        }

        public function obtenerCompraId($id){
            $consulta = 'SELECT (cp.id_compra)as idcompra,(cp.n_factura)as nfact,(cp.prefijo)as prefijo,(cp.fecha)as fecha, 
            (pv.nombre)as proveedor,(cp.id_proveedor)as idproveedor,(cp.total_compra)as totalc,(cp.total_sin_iva)as totsiniva,(cp.total_iva)as totiva, 
            (cp.estado_compra)as estado,(tc.descripcion)as tipoc,(cp.id_tipo_compra)as idtipoc,(suc.nombre) as sucursal 
            from compras cp 
            join proveedores pv on cp.id_proveedor = pv.id_proveedor  
            join tipo_compra tc on cp.id_tipo_compra = tc.id_tipo_compra 
            join sucursales suc on cp.id_sucursal = suc.id_sucursal 
            where cp.id_compra = :id';

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        public function eliminarCompraid($id){
            $consulta = 'DELETE FROM detalle_compras where id_compra=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            if ($this->db->execute()) {
                $consulta2 = 'DELETE FROM compras where id_compra=:id';
                $this->db->query($consulta2);
                $this->db->bind(':id',$id);

                if ($this->db->execute()) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        public function editarmestrocompra($datos){
            $consulta = 'UPDATE compras set n_factura=:nfact, prefijo=:prefijo, fecha=:fecha, id_proveedor=:idprov, 
                        estado_compra=:estado where id_compra=:id';
            $this->db->query($consulta);

            $this->db->bind(':nfact',$datos['nfact']);
            $this->db->bind(':prefijo',$datos['prefijo']);
            $this->db->bind(':fecha',$datos['fecha']);
            $this->db->bind(':idprov',$datos['provee']);
            $this->db->bind(':estado',$datos['estad']);
            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function actuaCompra($idc){
            $consulta = 'SELECT sum(total_detalle)as totdet from detalle_compras where id_compra=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$idc);
            $resultado = $this->db->registro();
            $total = $resultado->totdet;
            $totiva = ($total * 0.13);
            $totsiva = ($total - $totiva);
            if ($resultado) {
                $consulta2 = 'UPDATE compras set total_compra=:totdet, total_sin_iva=:totsiva, total_iva=:totiva
                where id_compra=:id';
                $this->db->query($consulta2);

                $this->db->bind(':totdet',$total);
                $this->db->bind(':totsiva',$totsiva);
                $this->db->bind(':totiva',$totiva);
                $this->db->bind(':id',$idc);

                if ($this->db->execute()) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        }

        public function eliminardetalleCompraid($id){
            $consulta = 'DELETE FROM detalle_compras where id_detalle_compras=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }