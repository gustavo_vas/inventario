<?php
    class Finanza
    {
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function gettipocompra(){
            $consulta = 'SELECT * FROM tipo_compra';
            $this->db->query($consulta);

            $resultado = $this->db->registros();
            return $resultado;
        }

        public function getcompras($datos){
            $consulta = 'SELECT (cp.id_compra)as idcompra,(cp.n_factura)as nfact,(cp.prefijo)as prefijo,(cp.fecha)as fecha, 
            (pv.nombre)as proveedor,(cp.total_compra)as totalc,(cp.total_sin_iva)as totsiniva,(cp.total_iva)as totiva, 
            (cp.estado_compra)as estado,(tc.descripcion)as tipoc,(suc.nombre) as sucursal 
            from compras cp 
            join proveedores pv on cp.id_proveedor = pv.id_proveedor  
            join tipo_compra tc on cp.id_tipo_compra = tc.id_tipo_compra 
            join sucursales suc on cp.id_sucursal = suc.id_sucursal 
            -- right join abonos ab on cp.id_compra = ab.id_compra 
            where cp.id_tipo_compra = :tipo and cp.fecha between :finit and :ffin
            order by cp.fecha asc';

            $this->db->query($consulta);
            $this->db->bind(':tipo', $datos['tipocompra']);
            $this->db->bind(':finit', $datos['fechainit']);
            $this->db->bind(':ffin', $datos['fechafin']);
            $resultado = $this->db->registros();
            return $resultado;
        }
        
    }
    