<?php

    class Admin{
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function obtenerCargos(){
            $consulta = 'SELECT * from cargoempleado';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function obtenerSucursales(){
            $consulta = 'SELECT * from sucursales';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function obtenerTipoP(){
            $consulta = 'SELECT * from tipo_proveedor';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function obtenerDepartamentos(){
            $consulta = 'SELECT * from departamentos';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //agregar cargo
        public function addcargo($cargo){
            $consulta = 'INSERT INTO cargoempleado (cargo) 
                    values(:cargo)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cargo',$cargo);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar cargo
        public function edicargo($cargo,$id){
            $consulta = 'UPDATE cargoempleado set cargo=:cargo where id_cargo=:id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cargo',$cargo);
            $this->db->bind(':id',$id);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //agregar sucursal
        public function addsucur($datos){
            $consulta = 'INSERT INTO sucursales (nombre,direccion,tel) 
                    values(:nom,:dir,:tel)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nom',$datos['suc']);
            $this->db->bind(':dir',$datos['dire']);
            $this->db->bind(':tel',$datos['tel']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //editar sucursal
        public function edisucursal($datos){
            $consulta = 'UPDATE sucursales set nombre=:nom, direccion=:dire, tel=:tele where id_sucursal=:id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nom',$datos['sucu']);
            $this->db->bind(':dire',$datos['dire']);
            $this->db->bind(':tele',$datos['tele']);
            $this->db->bind(':id',$datos['idsucu']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }