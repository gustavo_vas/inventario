<?php
    class Venta{
        private $db;

        public function __construct(){
            $this->db = new Base;
        }

        //obtiene el tipo de venta
        public function getTipoventa($tipo){
            $consulta = 'SELECT * from tipo_venta 
            where descripcion = :descrip';
            $this->db->query($consulta);

            $this->db->bind(':descrip',$tipo);
            $resultado = $this->db->registros();

            return $resultado;
        }

        public function agregarVentas($datos){
            $consulta = 'INSERT INTO ventas (num_fact, fecha_venta, total_venta, total_iva, total_sin_iva, 
                        estado_venta, id_tipo_venta, id_sucursal, id_cliente, id_empleado, comision_emp_venta) 
                        values(:nfac, :fecha, :totalv, :totaliva, :totsiniva, :estadov, :idtipovent, :idsuc, 
                        :idcliente, :idemple, :comision)';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':nfac',$datos['numf']);
            $this->db->bind(':fecha',$datos['fecha']);
            $this->db->bind(':totalv',$datos['totalv']);
            $this->db->bind(':totaliva',$datos['totaliva']);
            $this->db->bind(':totsiniva',$datos['totasiniva']);
            $this->db->bind(':estadov',$datos['estadov']);
            $this->db->bind(':idtipovent',$datos['tipoventa']);
            $this->db->bind(':idsuc',$datos['suc']);
            $this->db->bind(':idcliente',$datos['cliente']);
            $this->db->bind(':idemple',$datos['vendedor']);
            $this->db->bind(':comision',$datos['comision']);

            //ejecutar
            if ($this->db->execute()) {
                $this->db->query('SELECT LAST_INSERT_ID() AS id FROM ventas');
                $resultado = $this->db->registro();
                $idventa = $resultado->id;
                return $idventa;
            }else{
                return false;
            }
        }

        // agrega detalle de venta
        public function agregarDetalleVenta($datos){
            $consulta = 'INSERT INTO detalle_ventas (id_producto, cantidad, costo_unitario, iva, total_detalle, id_venta)
            values(:idprod, :cantidad, :costounit, :iva, :totaldetalle, :idventa)';
            $this->db->query($consulta);

                //vincular los valores
            $this->db->bind(':idprod',$datos['idprod']);
            $this->db->bind(':cantidad',$datos['cantidad']);
            $this->db->bind(':costounit',$datos['costounit']);
            $this->db->bind(':iva',$datos['iva']);
            $this->db->bind(':totaldetalle',$datos['totaldetlle']);
            $this->db->bind(':idventa',$datos['idventa']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //obtiene la lista de ventas, solo el maestro
        public function obtenerVentas($datos){
            $consulta = "SELECT (vt.id_venta)as idventa,(vt.num_fact)as nfact,(DATE_FORMAT(vt.fecha_venta,'%d/%m/%Y'))as fecha,(vt.total_venta)as totv, 
            (vt.total_iva)as totiva,(vt.total_sin_iva)as totsiniva,(vt.estado_venta)as estadov,(tpv.descripcion)as tipov, 
            (cl.nombres)as nomclie,(cl.apellidos)as apellclie,(suc.nombre) as sucursal ,(emp.nombre)as nomemp,
				(emp.apellido)as apellemp,(vt.comision_emp_venta)as comision,
				(ifnull(SUM(abo.monto),0))AS abono
            from ventas vt 
            join clientes cl on vt.id_cliente = cl.id_cliente  
            join tipo_venta tpv on vt.id_tipo_venta = tpv.id_tipo_venta 
            join sucursales suc on vt.id_sucursal = suc.id_sucursal 
            join empleados emp on vt.id_empleado = emp.id_empleado 
            LEFT JOIN abonos_ventas abo ON vt.id_venta = abo.id_venta
            where tpv.descripcion = :tipo and vt.fecha_venta between :finit and :ffin
            group by vt.id_venta
            ORDER BY vt.fecha_venta asc";

            $this->db->query($consulta);
            $this->db->bind(':tipo', $datos['tipo']);
            $this->db->bind(':finit', $datos['dateinit']);
            $this->db->bind(':ffin', $datos['datefin']);
            $resultado = $this->db->registros();
            return $resultado;
        }

        public function updateEstado($datos){
            $consulta = 'UPDATE ventas set estado_venta=:estado where id_venta=:id';
            $this->db->query($consulta);

            $this->db->bind(':estado',$datos['estad']);
            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //metodo para obtener una venta, maestro detalle
        public function obtenerVentaId($id){
            $consulta = 'SELECT (vt.id_venta)as idventa,(vt.num_fact)as nfact,(vt.fecha_venta)as fecha,(vt.total_venta)as totv, 
            (vt.total_iva)as totiva,(vt.total_sin_iva)as totsiniva,(vt.estado_venta)as estadov,(tpv.descripcion)as tipov, 
            (cl.nombres)as nomclie,(cl.apellidos)as apellclie,(suc.nombre) as sucursal ,(emp.nombre)as nomemp,(emp.apellido)as apellemp,(vt.comision_emp_venta)as comision,
            (cl.dui)as dui,(cl.direccion)as direccion,(cl.nit)as nit,(vt.id_empleado)as idemple,(vt.id_cliente)as idclie,
            (vt.id_tipo_venta)as idtpv
            from ventas vt 
            join clientes cl on vt.id_cliente = cl.id_cliente  
            join tipo_venta tpv on vt.id_tipo_venta = tpv.id_tipo_venta 
            join sucursales suc on vt.id_sucursal = suc.id_sucursal 
            join empleados emp on vt.id_empleado = emp.id_empleado 
            where vt.id_venta = :id
            order by vt.id_venta desc';

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //metodo para obtener detalle de venta
        public function obtenerDetalleId($id){
            $consulta = 'SELECT (dv.id_detalle_ventas)as iddetalle,(pd.codigo)as codpd,(pd.descripcion)as producto,
            (dv.cantidad)as cantidad,(dv.costo_unitario)as precio,(dv.total_detalle)as total 
            from detalle_ventas dv
            join productos pd on dv.id_producto = pd.id_producto 
            where dv.id_venta = :id';
            $this->db->query($consulta);

            $this->db->bind(':id', $id);

            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para editar el maestro de venta
        public function editarmestroventa($datos){
            $consulta = 'UPDATE ventas set num_fact=:nfact, id_empleado=:vende, fecha_venta=:fecha, id_cliente=:idclie, 
                        estado_venta=:estado where id_venta=:id';
            $this->db->query($consulta);

            $this->db->bind(':nfact',$datos['nfact']);
            $this->db->bind(':vende',$datos['vende']);
            $this->db->bind(':fecha',$datos['fecha']);
            $this->db->bind(':idclie',$datos['clie']);
            $this->db->bind(':estado',$datos['estad']);
            $this->db->bind(':id',$datos['id']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        // metodo para actualizar venta 
        public function actuaVenta($idc,$comision){
            $consulta = 'SELECT sum(total_detalle)as totdet from detalle_ventas where id_venta=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$idc);
            $resultado = $this->db->registro();
            $total = $resultado->totdet;
            $totiva = ($total * 0.13);
            $totsiva = ($total - $totiva);
            if ($resultado) {
                $consulta2 = 'UPDATE ventas set total_venta=:totdet, total_sin_iva=:totsiva, total_iva=:totiva, 
                comision_emp_venta=:comi
                where id_venta=:id';
                $this->db->query($consulta2);

                $this->db->bind(':totdet',$total);
                $this->db->bind(':totsiva',$totsiva);
                $this->db->bind(':totiva',$totiva);
                $this->db->bind(':id',$idc);
                $this->db->bind(':comi',$comision);

                if ($this->db->execute()) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        }

        //metodo para eliminar linea detalle de detalle ventas
        public function eliminardetalleVentaid($id){
            $consulta = 'DELETE FROM detalle_ventas where id_detalle_ventas=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        // metodo para actualizar venta despues de eliminar linea detalle
        public function actuaVentadel($idc){
            $consulta = 'SELECT sum(total_detalle)as totdet from detalle_ventas where id_venta=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$idc);
            $resultado = $this->db->registro();
            $total = $resultado->totdet;
            $totiva = ($total * 0.13);
            $totsiva = ($total - $totiva);
            if ($resultado) {
                $consulta2 = 'UPDATE ventas set total_venta=:totdet, total_sin_iva=:totsiva, total_iva=:totiva
                where id_venta=:id';
                $this->db->query($consulta2);

                $this->db->bind(':totdet',$total);
                $this->db->bind(':totsiva',$totsiva);
                $this->db->bind(':totiva',$totiva);
                $this->db->bind(':id',$idc);

                if ($this->db->execute()) {
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
            
        }

        //metodo para eliminar una venta totalmente
        public function eliminarVentaid($id){
            $consulta = 'DELETE FROM detalle_ventas where id_venta=:idventa';
            $this->db->query($consulta);
            $this->db->bind(':idventa',$id);

            if($this->db->execute()){
                $consulta2 = 'DELETE FROM ventas where id_venta=:idventa';
                $this->db->query($consulta2);
                $this->db->bind(':idventa',$id);
                if($this->db->execute()){
                    return true;
                }else{
                    return false;
                }
            }else{
                return false;
            }
        }

        //metodo para obtener los abonos por compra
        public function obtenerabonosxventa($id){
            $consulta = 'SELECT * from abonos_ventas where id_venta=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para obtener la suma de abonos de ventas a clientes
        public function obtenerSumaAbono($id){
            $consulta = 'SELECT sum(monto)as monto from abonos_ventas where id_venta=:id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

        //metodo para insertar el abono de una venta a la bd
        public function abonarventa($datos){
            $consulta = 'INSERT INTO abonos_ventas (id_venta, monto, fecha) values(:idventa, :monto, :fecha)';
            $this->db->query($consulta);

            $this->db->bind(':idventa',$datos['idventa']);
            $this->db->bind(':monto',$datos['monto']);
            $this->db->bind(':fecha',$datos['fecha']);

            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        
        //metodo para obtener una venta, maestro detalle contrato
        public function obtenerVentaContratoId($id){
            $consulta = 'SELECT (vt.id_venta)as idvent,(vt.fecha_venta)as fecha,(vt.total_venta)as totv, 
            (cl.nombres)as nomclie,(cl.apellidos)as apellclie,(suc.nombre) as sucursal , (cl.dui)as dui,
            (cl.direccion)as direccion,(cl.nit)as nit,(pr.descripcion) as descpro, (ma.descripcion) as marcapro, 
            (pr.codigo) as codigopro, (vt.id_tipo_venta)as idtpv from ventas vt 
            join clientes cl on vt.id_cliente = cl.id_cliente 
            join tipo_venta tpv on vt.id_tipo_venta = tpv.id_tipo_venta 
            join sucursales suc on vt.id_sucursal = suc.id_sucursal 
            join empleados emp on vt.id_empleado = emp.id_empleado 
            join detalle_ventas dt on vt.id_venta = dt.id_venta 
            join productos pr on dt.id_producto = pr.id_producto 
            join marcas ma on pr.id_marca = ma.id_marca 
            where vt.id_tipo_venta = 2 and 
            vt.id_venta = :id
            order by vt.id_venta desc';

            $this->db->query($consulta);
            $this->db->bind(':id',$id);
            $resultado = $this->db->registro();
            return $resultado;
        }

            // agrega contrato
            public function agregarContrato($datos){
                $consulta = 'INSERT INTO contratos (id_venta, fecha_contrato) 
                VALUES (:idventa, :fechac)';
                $this->db->query($consulta);
    
                    //vincular los valores
                $this->db->bind('idventa',$datos['idventa']);
                $this->db->bind('fechac',$datos['fechac']);
              
                if ($this->db->execute()) {
                    return true;
                }else{
                    return false;
                }
            }

    }