<?php

    class Usuario{
        private $db;
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }
        public function __destruct(){
            $this->db = null;
        }


        public function obtenerUsuario($usuario,$pass){
            $consulta = 'SELECT (us.id_usuario)as codusuario,(us.usuario)as usuario,(em.nombre)as nombre,
            (em.apellido)as apellido,(em.estado)as estado,(ca.cargo)as cargo, (us.id_empleado) as idemp,
                    (em.id_sucursal)as idsuc FROM usuarios us 
                    join empleados em on us.id_empleado=em.id_empleado 
                    join cargoempleado ca on em.id_cargo=ca.id_cargo
                    where usuario=:usuario and user_pass=:pass';
            $this->db->query($consulta);
            $this->db->bind(':usuario',$usuario);
            $this->db->bind(':pass',$pass);
            $resultado = $this->db->registro();
            return $resultado;
        }

        public function getusers(){
            $consulta = 'SELECT (us.id_usuario)as codusuario,(us.usuario)as usuario,(em.nombre)as nombre,
            (em.apellido)as apellido,(em.estado)as estado,(ca.cargo)as cargo, (us.id_empleado) as idemp,
                    (em.id_sucursal)as idsuc, (suc.nombre)as sucursal FROM usuarios us 
                    join empleados em on us.id_empleado=em.id_empleado 
                    join cargoempleado ca on em.id_cargo=ca.id_cargo 
                    join sucursales suc on em.id_sucursal=suc.id_sucursal
                    order by em.nombre asc';
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }
    }