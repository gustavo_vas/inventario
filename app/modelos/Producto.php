<?php

    class Producto{
        private $db; //manejador de la base de datos
        private $consulta;

        public function __construct(){
            //instancia de conexion a la bd
            $this->db = new Base;
        }

        //metodo para obtener productos
        public function obtenerProductos(){
            $consulta = 'SELECT (pt.id_producto)as code,(pt.codigo)as codigo,(pt.id_marca)as codmarca,(mc.descripcion)as marca, 
            (pt.id_categoria)as codcategoria,(cat.descripcion)as categoria,(pt.descripcion)as producto
            from productos pt 
            join categorias cat on pt.id_categoria = cat.id_categoria 
            join marcas mc on pt.id_marca = mc.id_marca 
            order by pt.descripcion asc';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para obtener un producto especifico
        public function obtenerProductoId($id){
            $consulta = 'SELECT (pt.id_producto)as code,(pt.codigo)as codigo,(pt.id_marca)as idmarca,(mc.descripcion)as marca, 
                    (pt.id_categoria)as idcate,(cat.descripcion)as cate,(pt.descripcion)as producto 
                    from productos pt 
                    join marcas mc on pt.id_marca = mc.id_marca 
                    join categorias cat on pt.id_categoria = cat.id_categoria 
                    where pt.id_producto = :id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);

            $fila = $this->db->registro();
            return $fila;
        }
        
        //metodo para obtener categorias
        public function obtenerCategorias(){
            $consulta = 'SELECT * from categorias';
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para obtener marcas
        public function obtenerMarcas(){
            $consulta = 'SELECT * from marcas';
            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        //metodo para obtener una categoria especifica
        public function obtenerCategoriaId($id){
            $consulta = 'SELECT * from categorias where id_categoria = :id';
            $this->db->query($consulta);

            //vincular
            $this->db->bind(':id',$id);
            $fila = $this->db->registro();
            return $fila;
        }

        //metodo para obtener una categoria especifica
        public function obtenerMarcaId($id){
            $consulta = 'SELECT * from marcas where id_marca = :id';
            $this->db->query($consulta);

            //vincular
            $this->db->bind(':id',$id);
            $fila = $this->db->registro();
            return $fila;
        }

        //metodo para agregar un nuevo producto
        public function agregarProducto($datos){
            $consulta = 'INSERT INTO productos (codigo,id_marca,id_categoria,descripcion) 
                    values(:cod,:marca,:cate,:descrip)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cod',$datos['codp']);
            $this->db->bind(':marca',$datos['marca']);
            $this->db->bind(':cate',$datos['categoria']);
            $this->db->bind(':descrip',$datos['descri']);
            // $this->db->bind(':pv',$datos['precventa']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //metodo para actualizar un nuevo producto
        public function actualizarProducto($datos){
            $consulta = 'UPDATE productos set codigo = :cod,id_marca = :marca,id_categoria = :cate,
                    descripcion = :descrip
                    where id_producto = :id';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':cod',$datos['codp']);
            $this->db->bind(':marca',$datos['marca']);
            $this->db->bind(':cate',$datos['categoria']);
            $this->db->bind(':descrip',$datos['descri']);
            // $this->db->bind(':pv',$datos['precventa']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        //metodo para insertar una nueva categoria
        public function agregarCategoria($datos){
            $consulta = 'INSERT INTO categorias(descripcion) values(:descrip)';
            $this->db->query($consulta);

            //vincular parametros
            $this->db->bind(':descrip',$datos['descripcion']);

            //ejecutar
            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }

        //metodo para agregar una nueva marca
        public function agregarMarca($datos){
            $consulta = 'INSERT INTO marcas(descripcion) values(:descrip)';
            $this->db->query($consulta);

            //vincular parametros
            $this->db->bind(':descrip',$datos['descripcion']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function editarCategoria($datos){
            $consulta = 'UPDATE categorias set descripcion = :descr where id_categoria = :id';
            $this->db->query($consulta);

            //vincular parametros
            $this->db->bind(':descr',$datos['descripcion']);
            $this->db->bind(':id',$datos['code']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function editarMarca($datos){
            $consulta = 'UPDATE marcas set descripcion = :descrip where id_marca = :id';
            $this->db->query($consulta);

            $this->db->bind(':descrip', $datos['descripcion']);
            $this->db->bind(':id', $datos['code']);

            if($this->db->execute()){
                return true;
            }else{
                return false;
            }
        }
    }