<?php 

    class Inventario {
        private $db;//manejador de la bd
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function obtenerInventario(){
            $consulta = 'SELECT (it.id_producto) as codigo_producto, (ma.descripcion) as marca,(po.descripcion)as descripcion_producto, 
            (ca.descripcion) as categoria, (it.lote) as lote_producto, (it.existencias) as existencia_productos, 
            (it.costo) as costo_productos, (it.total) as total_producto,CONCAT(it.lote," - ",po.descripcion)as loteproducto from inventario it 
            JOIN productos po on po.id_producto = it.id_producto
            join marcas ma on po.id_marca = ma.id_marca
            join categorias ca on po.id_categoria = ca.id_categoria
            ORDER BY po.codigo ASC';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;
        }

        
    }