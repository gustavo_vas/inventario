<?php

    class Proveedor{
        private $db;//manejador de la bd
        private $consulta;

        public function __construct(){
            $this->db = new Base;
        }

        public function obtenerProveedores(){
            $consulta = 'SELECT (pv.id_proveedor)as code,(pv.nombre)as nombre,(pv.direccion)as direccion,(pv.tel)as tel, 
                    (pv.nrc)as nrc,(pv.dui)as dui,(pv.nit)as nit,(pv.actividad_economica)as actividad,(tp.descripcion)as descripcion 
                    from proveedores pv 
                    join tipo_proveedor tp on pv.id_tipo_proveedor = tp.id_tipo_proveedor 
                    order by pv.nombre';

            $this->db->query($consulta);
            $resultado = $this->db->registros();
            return $resultado;

            
        }

        public function agregarProveedor($datos){
            $consulta = 'INSERT INTO proveedores (nombre,direccion,tel,nrc,dui,nit,actividad_economica,id_tipo_proveedor) 
                    values(:nombre,:direccion,:tel,:nrc,:dui,:nit,:actividad,:tipo)';
            $this->db->query($consulta);

            //vincular los valores
            $this->db->bind(':nombre',$datos['nombres']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':tel',$datos['tel']);
            $this->db->bind(':nrc',$datos['nrc']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':nit',$datos['nit']);
            $this->db->bind(':actividad',$datos['actividad']);
            $this->db->bind(':tipo',$datos['tp']);

            //ejecutar
            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }

        public function obtenerProveedorId($id){
            $consulta = 'SELECT (pv.id_proveedor)as code,(pv.nombre)as nombre,(pv.direccion)as direccion,(pv.tel)as tel, 
                    (pv.nrc)as nrc,(pv.dui)as dui,(pv.nit)as nit,(pv.actividad_economica)as actividad,(tp.descripcion)as descripcion, 
                    (pv.id_tipo_proveedor)as tipo from proveedores pv 
                    join tipo_proveedor tp on pv.id_tipo_proveedor = tp.id_tipo_proveedor 
                    where pv.id_proveedor = :id';
            $this->db->query($consulta);
            $this->db->bind(':id',$id);

            $fila = $this->db->registro();
            return $fila;
        }

        public function actualizarProveedor($datos){
            $consulta = 'UPDATE proveedores set nombre = :nombre, direccion = :direccion, dui = :dui, 
                    nit = :nit, tel = :tel, nrc = :nrc, actividad_economica = :actividad, id_tipo_proveedor = :tipo 
                    where id_proveedor = :id';

            $this->db->query($consulta);
            //vincular los valores
            $this->db->bind(':nombre',$datos['nombres']);
            $this->db->bind(':direccion',$datos['direccion']);
            $this->db->bind(':dui',$datos['dui']);
            $this->db->bind(':nit',$datos['nit']);
            $this->db->bind(':tel',$datos['tel']);
            $this->db->bind(':nrc',$datos['nrc']);
            $this->db->bind(':actividad',$datos['actividad']);
            $this->db->bind(':tipo',$datos['tp']);
            $this->db->bind(':id',$datos['codigo']);

            if ($this->db->execute()) {
                return true;
            }else{
                return false;
            }
        }
    }