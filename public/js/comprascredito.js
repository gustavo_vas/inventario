$(document).ready(function(){
    $('#prod').attr('disabled',true);
    $('#cantidad').attr('disabled',true);
    $('#costunit').attr('disabled',true);
    $('#mas').attr('disabled',true);
    $('#tipoco').attr('disabled',true);
    // validar();
    var datos2 = new Array();
    $('#detalle').hide();
    //funcion js para mandar datos al controlador para agregar nueva compra 
    $('#compras-form').submit(function(e){
        let ruta_url = $('#ruta').val();
        var detalles = JSON.stringify((datos2));
        const postData = {
            numf: $('#numf').val(),
            pref: $('#pref').val(),
            fecha: $('#fecha').val(),
            provee: $('#provee').val(),
            tipoco: $('#tipoco').val(),
            detalles: detalles
        };

        $.post(ruta_url+'/compras/agregarCredito',postData,function(response){
            // console.log(response);
            datos2.splice(0,datos2.length);
            datos2 = [];
            document.getElementById("total").innerHTML = 0;
            $('#compras-form').trigger('reset');
            $("#table-detalle").empty();
            $(location).attr('href',ruta_url+'/compras/credito');
        });
        e.preventDefault();
    });   
    
    // $('#form-detalle').submit(function(e){
    $(document).on('click','#mas',function(){
        if ($('#cantidad').val() == 0 || $('#costunit').val() == 0 || $('#prod').val() == 0) {
            $('#cantidad').css("border", "solid 2px #BFBEBE");
            $('#costunit').css("border", "solid 2px #BFBEBE"); 
            $('#prod').css("border", "solid 2px #BFBEBE");

            if($('#cantidad').val() == 0){
                $('#cantidad').css("border", "solid 2px #FA5858");
            }

            if($('#costunit').val() == 0){
                $('#costunit').css("border", "solid 2px #FA5858");
            }

            if($('#prod').val() == 0){
                $('#prod').css("border", "solid 2px #FA5858");
            }
        }else{
            $('#cantidad').css("border", "solid 2px #BFBEBE");
            $('#costunit').css("border", "solid 2px #BFBEBE"); 
            $('#prod').css("border", "solid 2px #BFBEBE");
                let total = 0;
            // let ruta_url = $('#ruta').val();
            // let postData = {
                let prod= $('#prod').val();
                let prod1= $('#prod option:selected').html();
                let cantidad= $('#cantidad').val();
                let costunit= $('#costunit').val();

                let totaldetalle = (cantidad * costunit);
                let iva = (totaldetalle * 0.13);
            // };
            var datos = [];
            let template = '';
            
            datos = {
                producto: prod,
                producto1: prod1,
                cant: cantidad,
                costou: costunit,
                total: totaldetalle,
                iva: iva
            };
            datos2.push(datos);
            $("#table-detalle").empty();
            // $("#table-detalle tbody tr").remove();

            for(var i = 0; i < datos2.length; i++) {
                total += datos2[i].total;
                template += `<tr inde='${i}'>
                <td>${datos2[i].producto}</td>
                <td>${datos2[i].producto1}</td>
                <td>${datos2[i].cant}</td>
                <td>${datos2[i].costou}</td>
                <td>${datos2[i].total}</td>
                <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                </tr>`;
            }
            // console.log(datos2);
            $("#table-detalle").prepend(template);
            document.getElementById("total").innerHTML = total;
        }
    });

    //metodo jquery para eliminar del array y mostrar en tabla
    $(document).on('click','.d', function(){
        if (confirm('¿desea eliminar este registro?')) {
            let template = '';
            let total = 0;
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('inde');
                    datos2.splice(id,1);
                    $("#table-detalle").empty();

                    for(var i = 0; i < datos2.length; i++) {
                        total += datos2[i].total;
                        template += `<tr inde='${i}'>
                        <td>${datos2[i].producto}</td>
                        <td>${datos2[i].producto1}</td>
                        <td>${datos2[i].cant}</td>
                        <td>${datos2[i].costou}</td>
                        <td>${datos2[i].total}</td>
                        <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                        </tr>`;
                        }

                        $("#table-detalle").prepend(template);
                        document.getElementById("total").innerHTML = total;
        }
    });
    
    $(document).on('click','#btndetalle',function(){
        validar();
        if($("#compras-form").valid()){
            $('#numf').attr('disabled',true);
            $('#pref').attr('disabled',true);
            $('#fecha').attr('disabled',true);
            $('#provee').attr('disabled',true);
            $('#tipoco').attr('disabled',true);

            $('#prod').attr('disabled',false);
            $('#cantidad').attr('disabled',false);
            $('#costunit').attr('disabled',false);
            $('#mas').attr('disabled',false);

            $('#detalle').show();
        }
        // $('#numf').css("border", "solid 2px #FA5858");
    });

    //metodo para validar datos del maestro
    function validar(){
        $("#compras-form").validate();

        $("#numf").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Ingrese No. de Factura</b></small>"
            }
        });

        $("#fecha").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Ingrese una Fecha</b></small>"
            }
        });

        $("#provee").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Seleccionar Proveedor</b></small>"
            }
        });

        $("#tipoco").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Seleccionar Tipo Compra</b></small>"
            }
        });
    }

    $(document).on('click','#emas',function(){
        if ($('#ecantidad').val() == 0 || $('#ecostunit').val() == 0 || $('#eprod').val() == 0) {
            $('#ecantidad').css("border", "solid 2px #BFBEBE");
            $('#ecostunit').css("border", "solid 2px #BFBEBE"); 
            $('#eprod').css("border", "solid 2px #BFBEBE");

            if($('#ecantidad').val() == 0){
                $('#ecantidad').css("border", "solid 2px #FA5858");
            }

            if($('#ecostunit').val() == 0){
                $('#ecostunit').css("border", "solid 2px #FA5858");
            }

            if($('#eprod').val() == 0){
                $('#eprod').css("border", "solid 2px #FA5858");
            }
        }else{
            $('#ecantidad').css("border", "solid 2px #BFBEBE");
            $('#ecostunit').css("border", "solid 2px #BFBEBE"); 
            $('#eprod').css("border", "solid 2px #BFBEBE");
            let ruta_url = $('#ruta').val();
            let prod= $('#eprod option:selected').val();
            let prod1= $('#eprod option:selected').html();
            let cantidad= $('#ecantidad').val();
            let costunit= $('#ecostunit').val();
            let idcomp= $('#idcomp').val();
            let tp= $('#tp').val();
            let t = '';
            if (tp == 1) {
                t = 'editardetallecontado';
            }else{
                t = 'editardetalle';
            }

            const postData = {
                prod: prod,
                prod1: prod1,
                cantidad: cantidad,
                costunit: costunit,
                idcomp: idcomp
            };

            $.post(ruta_url+'/compras/'+t+'/'+idcomp,postData,function(){
                $(location).attr('href',ruta_url+'/compras/'+t+'/'+idcomp);
            });       
        }
    });

    $(document).on('click','.deldet', function(){
        if (confirm('¿Desea eliminar este registro?')) {
            let idcomp= $('#idcomp').val();
            let ruta_url = $('#ruta').val();
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('ides');

            let tp= $('#tp').val();
            let t = '';
            if (tp == 1) {
                t = 'editardetallecontado';
            }else{
                t = 'editardetalle';
            }

            const postData = {
                id: id,
                idcomp: idcomp
            };

            
            $.post(ruta_url+'/compras/eliminardetallecompracredito/',postData,function(){
                $(location).attr('href',ruta_url+'/compras/'+t+'/'+idcomp);
            });
        }
    });
});