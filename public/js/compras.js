$(document).ready(function(){
    $('#prod').attr('disabled',true);
    $('#cantidad').attr('disabled',true);
    $('#costunit').attr('disabled',true);
    $('#mas').attr('disabled',true);
    $('#tipoco').attr('disabled',true);
    $('#tipocom').attr('disabled',true);
    // validar();
    var datos2 = new Array();
    $('#detalle').hide();
    //funcion js para mandar datos al controlador para agregar nueva compra 
    $('#compras-form').submit(function(e){
        let ruta_url = $('#ruta').val();
        var detalles = JSON.stringify((datos2));
        const postData = {
            numf: $('#numf').val(),
            pref: $('#pref').val(),
            fecha: $('#fecha').val(),
            provee: $('#provee').val(),
            tipoco: $('#tipoco').val(),
            detalles: detalles
        };

        $.post(ruta_url+'/compras/agregar',postData,function(response){
            // console.log(response);
            datos2.splice(0,datos2.length);
            datos2 = [];
            document.getElementById("total").innerHTML = 0;
            $('#compras-form').trigger('reset');
            $("#table-detalle").empty();
            $(location).attr('href',ruta_url+'/compras/');
        });
        e.preventDefault();
    });   
    
    // $('#form-detalle').submit(function(e){
    $(document).on('click','#mas',function(){
        if ($('#cantidad').val() == 0 || $('#costunit').val() == 0 || $('#prod').val() == 0) {
            $('#cantidad').css("border", "solid 2px #BFBEBE");
            $('#costunit').css("border", "solid 2px #BFBEBE"); 
            $('#prod').css("border", "solid 2px #BFBEBE");

            if($('#cantidad').val() == 0){
                $('#cantidad').css("border", "solid 2px #FA5858");
            }

            if($('#costunit').val() == 0){
                $('#costunit').css("border", "solid 2px #FA5858");
            }

            if($('#prod').val() == 0){
                $('#prod').css("border", "solid 2px #FA5858");
            }
        }else{
            $('#cantidad').css("border", "solid 2px #BFBEBE");
            $('#costunit').css("border", "solid 2px #BFBEBE"); 
            $('#prod').css("border", "solid 2px #BFBEBE");
                let total = 0;
            // let ruta_url = $('#ruta').val();
            // let postData = {
                let prod= $('#prod').val();
                let prod1= $('#prod option:selected').html();
                let cantidad= $('#cantidad').val();
                let costunit= $('#costunit').val();

                let totaldetalle = (cantidad * costunit);
                let iva = (totaldetalle * 0.13);
            // };
            var datos = [];
            let template = '';
            
            datos = {
                producto: prod,
                producto1: prod1,
                cant: cantidad,
                costou: costunit,
                total: totaldetalle,
                iva: iva
            };
            datos2.push(datos);
            $("#table-detalle").empty();
            // $("#table-detalle tbody tr").remove();

            for(var i = 0; i < datos2.length; i++) {
                total += datos2[i].total;
                template += `<tr inde='${i}'>
                <td>${datos2[i].producto}</td>
                <td>${datos2[i].producto1}</td>
                <td>${datos2[i].cant}</td>
                <td>${datos2[i].costou}</td>
                <td>${datos2[i].total}</td>
                <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                </tr>`;
            }
            // console.log(datos2);
            $("#table-detalle").prepend(template);
            document.getElementById("total").innerHTML = total;
        }
    });

    //metodo jquery para eliminar del array y mostrar en tabla
    $(document).on('click','.d', function(){
        if (confirm('¿desea eliminar este registro?')) {
            let template = '';
            let total = 0;
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('inde');
                    datos2.splice(id,1);
                    $("#table-detalle").empty();

                    for(var i = 0; i < datos2.length; i++) {
                        total += datos2[i].total;
                        template += `<tr inde='${i}'>
                        <td>${datos2[i].producto}</td>
                        <td>${datos2[i].producto1}</td>
                        <td>${datos2[i].cant}</td>
                        <td>${datos2[i].costou}</td>
                        <td>${datos2[i].total}</td>
                        <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                        </tr>`;
                        }

                        $("#table-detalle").prepend(template);
                        document.getElementById("total").innerHTML = total;
        }
    });
    
    $(document).on('click','#btndetalle',function(){
        validar();
        if($("#compras-form").valid()){
            $('#numf').attr('disabled',true);
            $('#pref').attr('disabled',true);
            $('#fecha').attr('disabled',true);
            $('#provee').attr('disabled',true);
            $('#tipoco').attr('disabled',true);

            $('#prod').attr('disabled',false);
            $('#cantidad').attr('disabled',false);
            $('#costunit').attr('disabled',false);
            $('#mas').attr('disabled',false);

            $('#detalle').show();
        }
        // $('#numf').css("border", "solid 2px #FA5858");
    });

    //metodo para validar datos del maestro
    function validar(){
        $("#compras-form").validate();

        $("#numf").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Ingrese No. de Factura</b></small>"
            }
        });

        $("#fecha").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Ingrese una Fecha</b></small>"
            }
        });

        $("#provee").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Seleccionar Proveedor</b></small>"
            }
        });

        $("#tipoco").rules("add", {
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Seleccionar Tipo Compra</b></small>"
            }
        });
    }

    $(document).on('click','.abc',function(){
        let ruta_url = $('#ruta').val();
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('inde');
        document.getElementById('idcomedit').value = id;

        $.post(ruta_url+'/compras/obtenercomprasmaestro',{id},function(response){
            datos = JSON.parse(response);
            if (datos.estado==1) {
                estado = 'EN PROCESO';
            }else if(datos.estado==2){
                estado = 'ADEUDO';
            }else if(datos.estado==3){
                estado = 'CANCELADA';
            }
            // console.log(ruta_url);
            document.getElementById('nfact').value = datos.nfact;
            document.getElementById('prefijo').value = datos.prefijo;
            document.getElementById('fecha').value = datos.fecha;
            $('#tipocom option:selected').val(datos.idtipoc);
            $('#tipocom option:selected').html(datos.tipoc);

            $('#provee option:selected').val(datos.idproveedor);
            $('#provee option:selected').html(datos.proveedor);

            $('#estad option:selected').val(datos.estado);
            $('#estad option:selected').html(estado);
            $('#editmaster').modal('show');
        });
        
    });

    //metodo para editar una compra
    $('#compras-editar').submit(function(e){
        let ruta_url = $('#ruta').val();
        let id = $('#idcomedit').val();
        let nfact = $('#nfact').val();
        let prefijo = $('#prefijo').val();
        let fecha = $('#fecha').val();
        let provee = $('#provee option:selected').val();
        let estad = $('#estad option:selected').val();
        const postData = {
            id: id,
            nfact: nfact,
            prefijo: prefijo,
            fecha: fecha,
            provee: provee,
            estad: estad
        };
        $.post(ruta_url+'/compras/editarmaestrocompra',postData,function(response){
            if (response == 1) {
                // toastr.success('Actualizacion Realizada Exitosamente');
                $('#editmaster').modal('hide');
                $(location).attr('href',ruta_url+'/compras');
            }else{
                // toastr.error('Ocurrio un error al actualizar los datos.');
                e.preventDefault();
            }
            
        });
        
    });  

    // $(document).on('click','#mostrarfilt',function(){
    //     let fechainit = $('#inicio').val();
    //     let fechafin = $('#fin').val();
    //     let ruta_url = $('#ruta').val();
    //     let postData = {
    //         fechainit: fechainit,
    //         fechafin: fechafin
    //     };
    //     $.post(ruta_url+'/compras/index',postData,function(){});
    // });
    
    // $("#mostrar").attr('disabled', true);
    // $("#Suc").hide();
    // $('#filtro').change(function(){
    //     var nfact = '<input type="text" placeholder="Ingrese No. Factura" class="form-control">';
    //     var date1 = '<input id="finicio" type="date" class="form-control" title="Fecha de inicio">';
    //     var date2 = '<input id="ffin" type="date" class="form-control" title="Fecha Fin">';
    //     var estado = `<select id="estado" class="form-control">
    //                     <option value="1" class="form-control">EN PROCESO</option>
    //                     <option value="2" class="form-control">ADEUDO</option>
    //                     <option value="3" class="form-control">CANCELADA</option>
    //                 </select>`;
        
    //     $('#controlesfiltro').empty();
    //     $('#controlesfiltro2').empty();
    //     $("#Suc").hide();

    //     if ($('#filtro').val()==1) {       
    //         $('#controlesfiltro').html(nfact);
    //         $("#mostrar").addClass("Activo").prop('disabled', false);
    //     }else if ($('#filtro').val()==2){
    //         $('#controlesfiltro').html(date1);
    //         $('#controlesfiltro2').html(date2);
    //         $("#mostrar").addClass("Activo").prop('disabled', false);
    //     }else if ($('#filtro').val()==3){
    //         $('#controlesfiltro').html(estado);
    //         $("#mostrar").addClass("Activo").prop('disabled', false);
    //     }else if ($('#filtro').val()==4){
    //         $("#Suc").show();
    //         $("#mostrar").addClass("Activo").prop('disabled', false);
    //     }else{
    //         $("#mostrar").addClass("Activo").prop('disabled', true);
    //     }
        
    // });
});