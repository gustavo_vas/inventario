$(document).ready(function(){
    $('#idcom').attr('disabled',true);
    $('#idcomedit').attr('disabled',true);
    $('#tipocom').attr('disabled',true);
    $('#ruta').attr('disabled',true);
    $(document).on('click','.ab',function(){
        $("#table-abono").empty();
        document.getElementById("lblcuotas").innerHTML = 'Cuota No. '+1;
        document.getElementById("lblabonado").innerHTML = 'Saldo Abonado: $ 0';
        document.getElementById("lbldeudor").innerHTML = 'Saldo Actual: $ '+parseFloat(document.getElementById("lbltotalc").innerHTML).toFixed(2);

        let template = '';
        let ruta_url = $('#ruta').val();
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('inde'); 
        var totcompra = parseFloat(document.getElementById("lbltotalc").innerHTML);
        var total = 0;
        var totaldeuda = 0;
        var cuota = 0;
        document.getElementById('idcom').value = id;           
        $.post(ruta_url+'/ventas/obtenerabonosxventa',{id},function(response){
            datos = JSON.parse(response);
            for(var i = 0; i < datos.length; i++) {
                total += parseFloat(datos[i].monto);
                template += `<tr>
                <td>${datos[i].monto}</td>
                <td>${datos[i].fecha}</td>
                </tr>`;
                cuota = i+2;
            }
            totaldeuda = parseFloat(totcompra - total);
            document.getElementById("lblcuotas").innerHTML = 'Cuota No. '+cuota;
            document.getElementById("lblabonado").innerHTML = 'Saldo Abonado: $ '+total.toFixed(2);
            document.getElementById("lbldeudor").innerHTML = 'Saldo Actual: $ '+totaldeuda.toFixed(2);
            $("#table-abono").prepend(template);
        });
        $('#abon').modal('show');
    });

    $('#compras-abono').submit(function(e){
        let ruta_url = $('#ruta').val();
        let id = $('#idcom').val();
        let fecha = $('#fechabono').val();
        const postData = {
            id: id,
            abono: $('#abono').val(),
            fecha: fecha
        };
        $.post(ruta_url+'/ventas/agregarabonosxventa',postData,function(response){
            if (response == 0) {
                toastr.warning('No se realizo la transaccion. La cantidad abonada no puede ser superior al saldo deudor');
            }else if(response == 1){
                toastr.error('La cantidad ingresada no puede ser inferior o igual a cero.');
            }else if(response == 2){
                toastr.info('Factura Cancelada. Saldo Deudor en Cero');
            }else{
                toastr.success('Transaccion Realizada Exitosamente');
                $('#abon').modal('hide');
                location.reload();
            }
            
        });
        e.preventDefault();
    });  
    
    //metodo para cargarlos datos de la compra a editar
    $(document).on('click','.abc',function(){
        let ruta_url = $('#ruta').val();
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('inde');
        document.getElementById('idcomedit').value = id;

        $.post(ruta_url+'/compras/obtenercomprasmaestro',{id},function(response){
            datos = JSON.parse(response);
            document.getElementById('nfact').value = datos.nfact;
            document.getElementById('prefijo').value = datos.prefijo;
            document.getElementById('fecha').value = datos.fecha;
            $('#tipocom option:selected').val(datos.idtipoc);
            $('#tipocom option:selected').html(datos.tipoc);

            $('#provee option:selected').val(datos.idproveedor);
            $('#provee option:selected').html(datos.proveedor);
            $('#editmaster').modal('show');
        });
        
    });

    //metodo para editar una compra
    $('#compras-editar').submit(function(e){
        let ruta_url = $('#ruta').val();
        let id = $('#idcomedit').val();
        let nfact = $('#nfact').val();
        let prefijo = $('#prefijo').val();
        let fecha = $('#fecha').val();
        let provee = $('#provee option:selected').val();
        let estad = $('#estad option:selected').val();
        const postData = {
            id: id,
            nfact: nfact,
            prefijo: prefijo,
            fecha: fecha,
            provee: provee,
            estad: estad
        };
        $.post(ruta_url+'/compras/editarmaestrocompra',postData,function(response){
            if (response == 1) {
                toastr.success('Actualizacion Realizada Exitosamente');
                $('#editmaster').modal('hide');
            }else{
                toastr.error('Ocurrio un error al actualizar los datos.');
                e.preventDefault();
            }
            
        });
        
    });  
});