$(document).ready(function(){
    $('#e').attr('disabled',true);
    //metodo para eliminar un cliente
    $(document).on('click','.delclie',function(){
        let fil = $(this)[0].parentElement.parentElement;
       let idcli = $(fil).attr('inde');
       $('#e').val(idcli);
        $('#elimclie').modal('show');
    });

    $(document).on('click','#delcliente',function(){
        let url = $('#ruta').val();
        let idc = $('#e').val();
        postData = {
            idc:idc
        };
        
        $.post(url+'/clientes/eliminar',postData,function(response){
            if (response == '1') {                
                    toastr.success('Registro eliminado correctamente.');
                    $('#e').val('');
                    $('#elimclie').modal('hide');
                    $('#okis').modal('show');
            }else{
                    toastr.error('Error al eliminar registro, recargue la pagina e intente de nuevo.');
            }
        });
    });

    $(document).on('click','#ok',function(){
        $('#okis').modal('hide');
        location.reload();
    });
});