$(document).ready(function(){
    //bloquear controles del detalle de venta
    $('#prod').attr('disabled',true);
    $('#cantidad').attr('disabled',true);
    $('#costunit').attr('disabled',true);
    $('#mas').attr('disabled',true);
    $('#tipoven').attr('disabled',true);
    $('#comision').attr('disabled',true);
    $('#comision').hide();
    $('#lblcomi').hide();

    //ocultar detalle y arreglo para guardar todo el detalle
    var datos2 = new Array();
    $('#detalle').hide();

    //activar controles del detalle y desactivar controles del maestro
    $(document).on('click','#btndetalle',function(){
        //funcion para validar formulario
        validar();
        //controles del maestro
        if($("#ventas-form").valid()){
            $('#numf').attr('disabled',true);
            $('#emple').attr('disabled',true);
            $('#fecha').attr('disabled',true);
            $('#clie').attr('disabled',true);

            //controles del detalle
            $('#prod').attr('disabled',false);
            $('#cantidad').attr('disabled',false);
            $('#costunit').attr('disabled',false);
            $('#mas').attr('disabled',false);
            $('#comision').val(0);
            $('#comision').attr('disabled',false);

            //mostrar detalle
            $('#detalle').show();
            $('#comision').show();
            $('#lblcomi').show();
        }
    });

    //validar y agregar detalle a la factura de venta
    $(document).on('click','#mas',function(){
        if ($('#cantidad').val() == 0 || $('#costunit').val() == 0 || $('#prod').val() == 0) {
            $('#cantidad').css("border", "solid 2px #BFBEBE");
            $('#costunit').css("border", "solid 2px #BFBEBE"); 
            $('#prod').css("border", "solid 2px #BFBEBE");

            if($('#cantidad').val() == 0){
                $('#cantidad').css("border", "solid 2px #FA5858");
            }

            if($('#costunit').val() == 0){
                $('#costunit').css("border", "solid 2px #FA5858");
            }

            if($('#prod').val() == 0){
                $('#prod').css("border", "solid 2px #FA5858");
            }
        }else{
            $('#cantidad').css("border", "solid 2px #BFBEBE");
            $('#costunit').css("border", "solid 2px #BFBEBE"); 
            $('#prod').css("border", "solid 2px #BFBEBE");
            // $('#comision').show();
            // $('#lblcomi').show();

            //capturando datos de entrada del detalle
            let prod = $('#prod').val();
            let proddescr = $('#prod option:selected').html();
            let cantidad = $('#cantidad').val();
            let costunit = $('#costunit').val();
            let total = 0;

            //calculando el total de linea de detalle
            let totaldetalle = (cantidad * costunit);
            //calculando iva para cada linea de detalle
            let iva = (totaldetalle * 0.13);

            //arreglo para almacenar cada objeto de linea detalle
            var datos = [];

            //almacenando objeto de linea detalle en arreglo
            datos = {
                codeproducto: prod,
                producto: proddescr,
                cant: cantidad,
                costounitario: costunit,
                total: totaldetalle,
                iva: iva
            };
            
            //agregando linea de detalle a arreglo asociativo
            datos2.push(datos);
            //limpiando el cuerpo de la tavbla
            $("#table-detalle").empty();
            // console.log(datos);

            //variable para construir el cuerpo de la tabla
            var template = '';
            //descargando arreglo en la tabla
            for(var i = 0; i < datos2.length; i++) {
                total += datos2[i].total;
                template += `<tr inde='${i}'>
                <td>${datos2[i].codeproducto}</td>
                <td>${datos2[i].producto}</td>
                <td>${datos2[i].cant}</td>
                <td>${datos2[i].costounitario}</td>
                <td>${datos2[i].total}</td>
                <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                </tr>`;
            }
            // console.log(datos2);
            //mostrando tabla
            $("#table-detalle").prepend(template);
            //mostrando total en el pie de la tabla
            document.getElementById("total").innerHTML = total;
        }
    });

    //eliminar linea detalle
    $(document).on('click','.d',function(){
        //confirmar eliminacion de registro
        if (confirm('¿desea eliminar este registro?')) {
            let template = '';
            let total = 0;
            // obtener la fila en la tabla
            let element = $(this)[0].parentElement.parentElement;
            // obtener la posicion del elemento linea detalle en la fila
            let id = $(element).attr('inde');
            //eliminar elemento del arreglo especificando la posicion y la cqantidad de elementos a eliminar 
            //a partir de la posicion
            datos2.splice(id,1);
            //limpiar cuerpo de la tabla para reconstruir
            $('#table-detalle').empty();

            // reconstruir nuevamente el cuerpo de la tabla con los nuevos objetos
            for(var i = 0; i < datos2.length; i++) {
                total += datos2[i].total;
                template += `<tr inde='${i}'>
                <td>${datos2[i].codeproducto}</td>
                <td>${datos2[i].producto}</td>
                <td>${datos2[i].cant}</td>
                <td>${datos2[i].costounitario}</td>
                <td>${datos2[i].total}</td>
                <td><button type="button" class="btn btn-danger d"><i class="nav-icon fas fa-trash-alt"></i></button></td>'+
                </tr>`;
            }

            //mostrando tabla
            $("#table-detalle").prepend(template);
            //mostrando total en el pie de la tabla
            document.getElementById("total").innerHTML = total;
        }
    });

    //metodo para registrar una venta al controlador
    $('#ventas-form').submit(function(e){
        let ruta_url = $('#ruta').val();
        var detalle = JSON.stringify(datos2);

        //recojer datos del formulario
        const postData = {
            numfact : $('#numf').val(),
            Vendedor : $('#emple').val(),
            fechaventa : $('#fecha').val(),
            Cliente : $('#clie').val(),
            tipoventa : $('#tipoven').val(),
            estadoventa : $('#estad').val(),
            comision : $('#comision').val(),
            detalle : detalle
        };

        //funcion ajax para mandar datos al controlador
        $.post(ruta_url+'/ventas/agregarcredito',postData,function(response){
            datos2.splice(0,datos2.length);
            datos2 = [];
            document.getElementById('total').innerHTML = '00.00';
            $('#ventas-form').trigger('reset');
            $('#table-detalle').empty();
            // if(response=='1'){
            //     $(location).attr('href',ruta_url+'/ventas');
            // }else{
            //     $(location).attr('href',ruta_url+'/ventas/credito');
            // }
            $(location).attr('href',ruta_url+'/ventas/credito');
            // e.preventDefault();
            console.log(response);
        });
        e.preventDefault();
    });

    //metodo para editar el maestro detalle de factura de venta
    $(document).on('click','.abc',function(){
        let ruta_url = $('#ruta').val();
        let element = $(this)[0].parentElement.parentElement;
        let id = $(element).attr('inde');
        document.getElementById('idcomedit').value = id;

        $.post(ruta_url+'/ventas/obtenerVentasmaestro',{id},function(response){
            datos = JSON.parse(response);
            if (datos.estadov==1) {
                estado = 'EN PROCESO';
            }else if(datos.estadov==2){
                estado = 'ADEUDO';
            }else if(datos.estadov==3){
                estado = 'CANCELADA';
            }
            // console.log(ruta_url);
            document.getElementById('nfact').value = datos.nfact;
            // document.getElementById('prefijo').value = datos.prefijo;
            document.getElementById('fecha').value = datos.fecha;
            $('#tipoven option:selected').val(datos.idtpv);
            $('#tipoven option:selected').html(datos.tipov);

            $('#vende option:selected').val(datos.idemple);
            $('#vende option:selected').html(datos.nomemp+' '+datos.apellemp);

            $('#clie option:selected').val(datos.idclie);
            $('#clie option:selected').html(datos.nomclie+' '+datos.apellclie);

            $('#estad option:selected').val(datos.estadov);
            $('#estad option:selected').html(estado);
            $('#editmaster').modal('show');
        });
        
    });

    //metodo para editar una venta
    $('#ventas-editar').submit(function(e){
        let ruta_url = $('#ruta').val();
        let id = $('#idcomedit').val();
        let nfact = $('#nfact').val();
        // let prefijo = $('#prefijo').val();
        let fecha = $('#fecha').val();
        let vende = $('#vende option:selected').val();
        let clie = $('#clie option:selected').val();
        let estad = $('#estad option:selected').val();
        const postData = {
            id: id,
            nfact: nfact,
            vende: vende,
            fecha: fecha,
            clie: clie,
            estad: estad
        };
        $.post(ruta_url+'/ventas/editarmaestroventas',postData,function(response){
            if (response == '1') {
                // toastr.success('Actualizacion Realizada Exitosamente');
                $('#editmaster').modal('hide');
                $(location).attr('href',ruta_url+'/ventas/credito');
            }else{
                // toastr.error('Ocurrio un error al actualizar los datos.');
                e.preventDefault();
            }
            
        });
        // e.preventDefault();
    });

    //metodo para validar maestro
    function validar(){
        $('#ventas-form').validate();

        $('#numf').rules('add',{
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Ingresar No. de Factura</b></small>"
            }
        });

        $('#emple').rules('add',{
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Seleccionar Vendedor</b></small>"
            }
        });

        $('#fecha').rules('add',{
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Ingresar Fecha</b></small>"
            }
        });

        $('#clie').rules('add',{
            required: true,
            messages: {
                required: "<small style='color: red;'><b>Seleccionar Cliente</b></small>"
            }
        });
    }

    //metodo para editar un detalle
    $(document).on('click','#emas',function(){
        if ($('#ecantidad').val() == 0 || $('#ecostunit').val() == 0 || $('#eprod').val() == 0) {
            $('#ecantidad').css("border", "solid 2px #BFBEBE");
            $('#ecostunit').css("border", "solid 2px #BFBEBE"); 
            $('#eprod').css("border", "solid 2px #BFBEBE");

            if($('#ecantidad').val() == 0){
                $('#ecantidad').css("border", "solid 2px #FA5858");
            }

            if($('#ecostunit').val() == 0){
                $('#ecostunit').css("border", "solid 2px #FA5858");
            }

            if($('#eprod').val() == 0){
                $('#eprod').css("border", "solid 2px #FA5858");
            }
        }else{
            $('#ecantidad').css("border", "solid 2px #BFBEBE");
            $('#ecostunit').css("border", "solid 2px #BFBEBE"); 
            $('#eprod').css("border", "solid 2px #BFBEBE");
            let ruta_url = $('#ruta').val();
            let prod= $('#eprod option:selected').val();
            let prod1= $('#eprod option:selected').html();
            let cantidad= $('#ecantidad').val();
            let costunit= $('#ecostunit').val();
            let idvent= $('#idvent').val();
            let tipoven= $('#tipoven').val();
            let comision= $('#editcomision').val();
            let t = '';
            // if (tipoven == 1) {
                t = 'editardetallecredito';
            // }else{
                // t = 'editardetalle';
            // }

            const postData = {
                prod: prod,
                prod1: prod1,
                cantidad: cantidad,
                costunit: costunit,
                idvent: idvent,
                comision: comision
            };

            $.post(ruta_url+'/ventas/'+t+'/'+idvent,postData,function(){
                $(location).attr('href',ruta_url+'/ventas/'+t+'/'+idvent);
            });       
        }
    });

    //metodo para eliminar una linea de detalle del formulario editar detalle de venta
    $(document).on('click','.deldet', function(){
        if (confirm('¿desea eliminar este registro?')) {
            let idvent= $('#idvent').val();
            let ruta_url = $('#ruta').val();
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('ides');

            let tipoven= $('#tipoven').val();
            let t = '';
            // if (tipoven == 1) {
                t = 'editardetallecredito';
            // }else{
                // t = 'editardetalle';
            // }

            const postData = {
                id: id,
                idvent: idvent
            };

            
            $.post(ruta_url+'/ventas/eliminardetalleLineaventa/',postData,function(){
                $(location).attr('href',ruta_url+'/ventas/'+t+'/'+idvent);
            });
        }
    });

        //metodo para insertar datos a la tabla contratos
        $(document).on('click','.contrato',function(){
            let ruta_url = $('#ruta').val();
            let element = $(this)[0].parentElement.parentElement;
            let id = $(element).attr('inde');
            document.getElementById('idcomedit').value = id;
            const postData = {
               id :id
            };
            $.post(ruta_url+'/ventas/agregarcontrato',postData,function(response){
                console.log(response);
                // datos = JSON.parse(response);
                // // console.log(ruta_url);
                // document.getElementById('nfact').value = datos.nfact;
                // // document.getElementById('prefijo').value = datos.prefijo;
                // document.getElementById('fecha').value = datos.fecha;
                // $('#tipoven option:selected').val(datos.idtpv);
                // $('#tipoven option:selected').html(datos.tipov);
    
                // $('#vende option:selected').val(datos.idemple);
                // $('#vende option:selected').html(datos.nomemp+' '+datos.apellemp);
    
                // $('#clie option:selected').val(datos.idclie);
                // $('#clie option:selected').html(datos.nomclie+' '+datos.apellclie);
                // $('#editmaster').modal('show');
            });
            
        });
});