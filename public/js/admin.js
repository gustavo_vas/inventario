$(document).ready(function(){
    $('#ruta').attr('disabled',true);
    $('#idcargo').attr('disabled',true);
    
    $(document).on('click','#cargo',function(){
        $('#addcargo').modal('show');
    });

    //agregar cargo
    $(document).on('click','#btnadcargo',function(){
        let scargo = $('#cargoemp').val().toUpperCase();
        let url = $('#ruta').val();
        let postData = {
            scargo: scargo
        };
        $.post(url+'/admins1/addcargo',postData,function(response){
            if (response == 1) {
                location.reload();
            }else{
                alert('Fallo al insertar cargo');
            }
        });
        
    });

    //editar cargo
    $(document).on('click','.edca',function(){
        let elemment = $(this)[0].parentElement.parentElement;
        let idcargo = $(elemment).attr('inde');

        let carg = $(this).parents('tr').find('td').eq(0).text();
        $('#cargoempedit').val(carg);
        $('#idcargo').val(idcargo);
        $('#editcargo').modal('show');
    });

    $(document).on('click','#btneditcargo',function(){
        let scargo = $('#cargoempedit').val().toUpperCase();
        let idcarg = ($('#idcargo').val());
        let url = $('#ruta').val();
        let postData = {
            scargo: scargo,
            idcarg: idcarg
        };
        $.post(url+'/admins1/editcargo',postData,function(response){
            if (response == 1) {
                location.reload();
            }else{
                alert('Fallo al actualizar datos.')
            }
        });
        
    });

    //agregar sucursal
    $(document).on('click','#btnaddsuc',function(){
        $('#addsucsl').modal('show');
    });
    
    $(document).on('click','#btnadsucursal',function(){
        let suc = $('#suc').val().toUpperCase();
        let dire = $('#dire').val().toUpperCase();
        let tel = $('#tel').val().toUpperCase();
        let url = $('#ruta').val();
        let postData = {
            suc: suc,
            dire: dire,
            tel: tel
        };
        $.post(url+'/admins1/addsucursal',postData,function(response){
            if (response == 1) {
                location.reload();
            }else{
                alert('Fallo al insertar sucursal.');
            }
        });
        
    });

    //editar sucursal
    $(document).on('click','.edsuc',function(){
        let elemment = $(this)[0].parentElement.parentElement;
        let idsuc = $(elemment).attr('inde');

        let suc = $(this).parents('tr').find('td').eq(0).text();
        let dir = $(this).parents('tr').find('td').eq(1).text();
        let tel = $(this).parents('tr').find('td').eq(2).text();
        $('#sucedit').val(suc);
        $('#direedit').val(dir);
        $('#teledit').val(tel);
        $('#codi').val(idsuc);
        $('#editsucsl').modal('show');
    });

    $(document).on('click','#btneditsucursal',function(){
        let sucu = $('#sucedit').val().toUpperCase();
        let dire = $('#direedit').val().toUpperCase();
        let tele = $('#teledit').val().toUpperCase();
        let idsucu = ($('#codi').val());
        let url = $('#ruta').val();
        let postData = {
            sucu: sucu,
            dire: dire,
            tele: tele,
            idsucu: idsucu
        };
        $.post(url+'/admins1/editsucursal',postData,function(response){
            if (response == 1) {
                location.reload();
            }else{
                alert('Fallo al actualizar datos.')
            }
        });
        
    });
});