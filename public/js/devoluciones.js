$(document).ready(function(){
  var datos = new Array();
  $('#prd').attr('disabled',true);
  $('#cant').attr('disabled',true);
  $('#det').attr('disabled',true);
  $('#cant').val(0);
  $('#cantdv').val(0);
  $('#btndev').attr('disabled',true);
  $('#idv').attr('disabled',true);
    $(document).on('click','.abc',function(){
         let ruta_url = $('#ruta').val();
         let template = '';
         
          let element = $(this)[0].parentElement.parentElement;
          let id = parseInt($(element).attr('inde'));
          // document.getElementById('idcomedit').value = id;

          $.post(ruta_url+'/devoluciones/obtenerdevoluciones',{id},function(response){
              datos = JSON.parse(response);
             $("#table-detalle1").empty();
             var totalv = 0;
              for(i in datos) {
                totalv += parseFloat(datos[i].total);
                template += `<tr indedet='${datos[i].iddetvent}'>
                <td>${datos[i].descripcion_producto}</td>
                <td>${datos[i].cantidad_producto}</td>
                <td>${datos[i].costo_producto}</td>
                <td>${datos[i].total}</td>
                <td><a href="#" title="Devolver artículo" class="btn btn-warning devolver"><i class="nav-icon fas fa-check-square"></i></a></td>
                </tr>`;      
                // console.log(datos[i].iddetvent);     
              }
              $("#table-detalle1").prepend(template);
              document.getElementById("etotal").innerHTML = totalv.toFixed(2);    
          });
    
        $('#adddevoluciones').modal('show');
    }); 
    
    $(document).on('click','.devolver',function(){
        let url = $('#ruta').val();
        let element = $(this)[0].parentElement.parentElement;
        var canti = parseInt($(this).parents("tr").find("td").eq(1).text());
        let prod = $(this).parents('tr').find('td').eq(0).text();
        let id = parseInt($(element).attr('indedet'));
        $('#cant').val(canti);
        $('#prd').val(prod);
        $('#det').val(id);
        $('#btndev').attr('disabled',false);
    });

    $(document).on('click','#btndev',function(){
      if (confirm('¿Desea realizar la devolución del artículo?')) {
        let url = $('#ruta').val();
        let id = parseInt($('#det').val());
        var cantdv = parseInt($('#cantdv').val());
        let cant = parseInt($('#cant').val());
        let fecha = $('#fecha').val();
        let idv = $('#idv').val();
        const postData = {
          id:id,
          cant:cant,
          cantdv:cantdv,
          fecha:fecha,
          idv:idv
        };

        if (cantdv<=cant) {
          if (cantdv>0) {
            $.post(url+'/devoluciones/devolverarticulo',postData,function(response){
              switch (response) {
                case '4':
                  toastr.success('Devolución Realizada Exitosamente');
                  $('#cantdv').val(0);
                  $('#cant').val(0);
                  $('#prd').val('');
                  // e.preventDefault();
                  $('#adddevoluciones').modal('hide');
                  $('#okis').modal('show');
                break;

                case '3':
                  toastr.warning('Ocurrio un problema al realizar el proceso de devolucion');
                break;

                case '2':
                  toastr.error('La cantidad a devolver no puede ser menor o igual a cero');
                break;

                case '1':
                    toastr.error('La cantidad a devolver no puede ser mayor a la cantidad de venta');
                break;
                case '0':
                  toastr.warning('Problemas al intentar consultar los datos de linea detalle de venta');
                break;
              
                default:
                  toastr.error('Excepción sin controlar, comuniquese con el administrador de sistema.');
                break;
              }
              
            });
          }else{
            toastr.error('La cantidad a devolver no puede ser menor o igual a cero')
          }          
        }else{
          toastr.error('La cantidad a devolver no puede ser mayor a la cantidad de venta');
        }
        
        // alert(cant);
      }
    });
});

function val(idventa,nfact,nombre){
  document.getElementById('s').innerHTML = nfact;
  document.getElementById('sd').innerHTML = nombre;
  document.getElementById('idv').value = idventa;
}