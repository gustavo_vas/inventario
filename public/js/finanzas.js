$(document).ready(function(){
    $(document).on('click','#btnbuscar',function(){
        $("#finanzas1").empty();
        let template = '';
        var url = $('#ruta1').val();
        var fechainit = $('#fechainit').val();
        var fechafin = $('#fechafin').val();
        var tipocompra = $('#tipoco').val();
        var estad = $('#estad').val();
        var posData = {
            fechainit: fechainit,
            fechafin: fechafin,
            tipocompra: tipocompra
        };
        $.post(url+'/Finanzas/compras',posData,function(response){
             datos = JSON.parse(response);
            // console.log(datos);
            // alert(response);
            var total = 0;
            var iva = 0;
            var subtotal = 0;
            
            // <td>${datos[i].proveedor}</td>
            // <td>${datos[i].tipoc}</td>
            for(var i = 0; i < datos.length; i++) {
                if (datos[i].estado == estad) {
                    total += parseFloat(datos[i].totalc);
                    iva += parseFloat(datos[i].totiva);
                    subtotal += parseFloat(datos[i].totsiniva);
                    if(datos[i].estado ==1){
                        estado = '<span class="span label-warning"><b>EN PROCESO</b></span>';
                    }else if(datos[i].estado ==2){
                        estado = '<span class="span label-warning"><b>ADEUDO</b></span>';
                    }else if(datos[i].estado ==3){
                        estado = '<span class="span label-success"><b>CANCELADA</b></span>';
                    }
                    // total += parseFloat(datos[i].monto);
                    template += `<tr>
                        <td>${datos[i].nfact}</td>
                        <td>${estado}</td>
                        <td>${datos[i].sucursal}</td>
                        <td>${datos[i].fecha}</td>
                        <td>$ ${datos[i].totalc}</td>
                        <td>$ ${datos[i].totiva}</td>
                        <td>$ ${datos[i].totsiniva}</td>
                    </tr>`;
                }
                
            }
            $("#finanzas1").prepend(template);
            // $('#total').val(total);
            document.getElementById("total").innerHTML = '<b>$ '+total.toFixed(2)+'</b>';
            document.getElementById("iva").innerHTML = '<b>$ '+iva.toFixed(2)+'</b>';
            document.getElementById("subtotal").innerHTML = '<b>$ '+subtotal.toFixed(2)+'</b>';

            document.getElementById("subtot").innerHTML = '<b>$ '+total.toFixed(2)+'</b>';
            document.getElementById("iva2").innerHTML = '<b>$ '+iva.toFixed(2)+'</b>';
            document.getElementById("tot2").innerHTML = '<b>$ '+subtotal.toFixed(2)+'</b>';
        });
    });









    jQuery("#buscador").keyup(function(){
        if( jQuery(this).val() != ""){
            jQuery("#example1 tbody>tr").hide();
            jQuery("#example1 td:contiene-palabra('" + jQuery(this).val() + "')").parent("tr").show();
        }
        else{
            jQuery("#example1 tbody>tr").show();
        }
    });
     
    jQuery.extend(jQuery.expr[":"], 
    {
        "contiene-palabra": function(elem, i, match, array) {
            return (elem.textContent || elem.innerText || jQuery(elem).text() || "").toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
        }
    });
});